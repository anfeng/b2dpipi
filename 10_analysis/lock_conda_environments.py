import argparse
import os
import subprocess

parser = argparse.ArgumentParser(description="Lock conda environments.")
parser.add_argument("-d", "--directory", dest="dir", type=str, nargs="*")
parser.add_argument("-f", "--file", dest="file", type=str, nargs="*")
parser.add_argument("--apptainer", dest="apptainer", type=str)

args, unknown = parser.parse_known_args()

filepaths_to_be_pinned = []

if args.dir:
    for dirname in args.dir:
        with os.scandir(dirname) as it:
            for entry in it:
                if (
                    (not entry.name.endswith(".pin.yml"))
                    and (entry.name.endswith(".yml") or (entry.name.endswith(".yaml")))
                    and entry.is_file()
                ):
                    filepaths_to_be_pinned.append(entry.path)

if args.file:
    filepaths_to_be_pinned += args.file

if args.apptainer:
    cmd_apptainer = f"apptainer exec --cleanenv {args.apptainer}"
else:
    cmd_apptainer = ""

for filepath in filepaths_to_be_pinned:
    pin_file_name = os.path.splitext(filepath)[0] + ".pin.yml"
    if os.path.exists(pin_file_name):
        print(f"Pin file {pin_file_name} exists. Skipping.")
    else:
        print(f"Pin file {pin_file_name} does not exist. Pinning.")
        cmd = f"{cmd_apptainer} conda-lock -f {filepath} -p linux-64 --lockfile {pin_file_name}"
        print(cmd)
        subprocess.run(cmd, shell=True)
