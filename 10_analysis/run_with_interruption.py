import argparse
import subprocess

parser = argparse.ArgumentParser(description="Run command with periodic interruption.")
parser.add_argument("--interruption-timeout", dest="timeout", type=int, default=3600)
parser.add_argument(
    "--interruption-timeout-kill", dest="timeout_kill", type=int, default=30
)

args, unknown = parser.parse_known_args()

timeout = args.timeout
timeout_kill = args.timeout_kill
command = unknown

while True:
    print("running...")
    p = subprocess.Popen(command)
    try:
        p.wait(timeout)
        break
    except subprocess.TimeoutExpired or KeyboardInterrupt:
        print("interrupting...")
        p.send_signal(2)
        while True:
            try:
                p.wait(timeout_kill)
                break
            except subprocess.TimeoutExpired:
                print("interrupting again...")
                p.send_signal(2)
