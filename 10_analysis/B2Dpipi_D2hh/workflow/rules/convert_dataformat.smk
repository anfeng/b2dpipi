map_channel_to_tree_name = {
    "Kpi": "B2Dhh_Kpi/B2DhhReco",
    "KK": "B2Dhh_KK/B2DhhReco",
    "pipi": "B2Dhh_pipi/B2DhhReco",
}


def get_input_filepaths(wildcards):
    if wildcards["datatype"] == "data":
        return f"data/input/Bd2Dpipi_hh/data/Run{wildcards['run']}/B2Dhh-CollisionCombined-MagCombined-Stripping24r2-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withSelection.root"
    elif wildcards["datatype"] == "mc":
        return f"data/input/Bd2Dpipi_hh/mc/Run{wildcards['run']}/Bd2pipiD0-D02{wildcards['channel']}-MC-YearsCombined-MagCombined-Stripping24r2-Sim09k-withMCtruth-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withSelection.root"
    elif wildcards["datatype"] == "toy":
        return f"data/input/Bd2Dpipi_hh/toy/TEST-Dpipi_CPEven_B2Dhh_{wildcards['channel']}_Uniform_DTRoff_expts0-Run{wildcards['run']}_weight.root"
    else:
        raise Exception(f"unknown datatype {wildcards['datatype']}")


rule split_rootfiles:
    input:
        get_input_filepaths,
    output:
        f"data/derived/datasets/partitioned/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.root",
    log:
        f"logs/split_rootfiles/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
        notebook=f"logs/notebooks/split_rootfiles/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/split_rootfiles/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name=lambda wildcards: (
            map_channel_to_tree_name[wildcards["channel"]]
            if wildcards["datatype"] != "toy"
            else "fitTree"
        ),
        output_tree_name="DecayTree",
        ipartition=lambda wildcards: int(wildcards["ipartition"]),
        npartitions=npartitions,
    threads: 1  # This is required by RDataFrame.Range()
    resources:
        mem_mib=2000,
    conda:
        "../envs/split_rootfiles.yml"
    notebook:
        "../notebooks/split_rootfiles.py.ipynb"


rule convert_dataformat:
    input:
        f"data/derived/datasets/partitioned/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/to_parquet/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
    log:
        f"logs/convert_dataformat/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
        notebook=f"logs/notebooks/convert_dataformat/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/convert_dataformat/{paramspace_combined.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="DecayTree",
    threads: 1  # Setting it > 1 may cause incorrect results
    resources:
        mem_mib=2000,
    conda:
        "../envs/convert_dataformat.yml"
    notebook:
        "../notebooks/convert_dataformat.py.ipynb"
