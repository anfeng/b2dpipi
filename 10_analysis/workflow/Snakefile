from snakemake.utils import min_version, Paramspace
import pandas as pd

# -------- set minimum snakemake version --------
min_version("7")


# -------- set configfile --------
configfile: "config/config.yml"


# -------- set global container image --------
container: "docker://laf070810/conda-lock@sha256:4c4871c5589dfa8e898b6dc0224feb1f8e223a80fed22879b76a935fa3ab02b3"


# -------- declare paramspaces --------
dataframe = pd.DataFrame(columns=["eventtype", "KStype", "year", "polarity"])
paramspace = Paramspace(dataframe)


wildcard_constraints:
    eventtype="90000000|11166117",
    KStype="LL|DD|NA",  # NA is Not Applicable
    year="2011|2012|2015|2016|2017|2018",
    polarity="magdown|magup",


# Wildcard "mva" corresponds to every single MVA.
# Wildcard "mvatype" corresponds to types of MVA cut.

dataframe_mva = pd.DataFrame(columns=["mva", "KStype", "run"])
paramspace_mva = Paramspace(dataframe_mva)


wildcard_constraints:
    mva="independent_KS|independent_D|independent_B|cutcascaded_KS|cutcascaded_D|cutcascaded_B|varcascaded_KS|varcascaded_D|varcascaded_B|combined",
    run="1|2",


dataframe_fom = pd.DataFrame(columns=["mvatype", "KStype", "run"])
paramspace_fom = Paramspace(dataframe_fom)


wildcard_constraints:
    mvatype="independent|varcascaded|combined|cutcascaded_KS|cutcascaded_D|cutcascaded_B",


dataframe_mvaresponse = pd.DataFrame(
    columns=["mva", "eventtype", "KStype", "year", "polarity"]
)
paramspace_mvaresponse = Paramspace(dataframe_mvaresponse)


dataframe_aftermva = pd.DataFrame(
    columns=["mvatype", "eventtype", "KStype", "year", "polarity"]
)
paramspace_aftermva = Paramspace(dataframe_aftermva)

dataframe_afterselection = pd.DataFrame(
    columns=["datatype", "channel", "KStype", "run"]
)
paramspace_afterselection = Paramspace(dataframe_afterselection)


wildcard_constraints:
    datatype="data|mc|toy",
    channel="KSpipi|Kpi|KK|pipi",


dataframe_massfit = pd.DataFrame(columns=["KStype", "run"])
paramspace_massfit = Paramspace(dataframe_massfit)


dataframe_ftcalifit = pd.DataFrame(columns=["taggertype", "run"])
paramspace_ftcalifit = Paramspace(dataframe_ftcalifit)


wildcard_constraints:
    taggertype="OS|SS",


dataframe_fitbetagentoy = pd.DataFrame(
    columns=["model_name", "M", "N", "param_source", "event_num", "run_index"]
)
paramspace_fitbetagentoy = Paramspace(dataframe_fitbetagentoy)


wildcard_constraints:
    model_name=r"model_beta|model_hh|model_combined",
    M=r"\d+",
    N=r"\d+",
    param_source=r"bondar_params|our_params",
    event_num=r"\d+",
    run_index=r"\d+",


dataframe_fitbeta = pd.DataFrame(
    columns=[
        "datatype",
        "method",
        "model_name",
        "M",
        "N",
        "param_source",
        "c_s_status",
        "C_S_status",
        "beta_status",
    ]
)
paramspace_fitbeta = Paramspace(dataframe_fitbeta)


wildcard_constraints:
    method=r"roofit|scipy",
    c_s_status=r"(c\d+)*(s\d+)*(cs)*(fixed|float)",
    C_S_status=r"(C\d+)*(S\d+)*(CS)*(fixed|float)",
    beta_status=r"betafixed|betafloat",


# -------- declare paramspaces for rectangular cuts --------
# originally designed for incremental grid searching for the best rectangular cut, but abandoned afterwards
rectangular_cuts = {
    "dz_significance_DB": {"current": ["0.5"], "previous": ["0.5"]},
    "KSD_FDCHI2_ORIVX": {
        "current": ["0"],
        "previous": ["0"],
    },
    "D_FDCHI2_ORIVX": {"current": ["0"], "previous": ["0"]},
}
dataframe_rectcut = pd.DataFrame(columns=rectangular_cuts.keys())
paramspace_rectcut = Paramspace(dataframe_rectcut)


# -------- declare paramspaces for partitions --------
dataframe_partition = pd.DataFrame(columns=["ipartition"])
paramspace_partition = Paramspace(dataframe_partition)
npartitions = 100
npartitions_aftermva = 5
npartitions_combined = 50
npartitions_aftermassfit = 5


wildcard_constraints:
    ipartition="|".join([str(i) for i in range(npartitions)]),


# -------- load rules: external rules --------


module B2Dpipi_D2hh_workflow:
    snakefile:
        "../B2Dpipi_D2hh/workflow/Snakefile"
    prefix:
        "B2Dpipi_D2hh"
    config:
        config


use rule * from B2Dpipi_D2hh_workflow as B2Dpipi_D2hh_*


# -------- load rules: common functions --------
include: "rules/common.smk"


# -------- load rules: event selection --------


# include: "rules/fetch_apdata.smk"
include: "rules/match_mctruth.smk"
include: "rules/pidcorr.smk"
include: "rules/define_columns.smk"
include: "rules/remove_arraycolumns.smk"
include: "rules/convert_dataformat.smk"
include: "rules/remove_kpiswapmultiplecandidates.smk"
include: "rules/cut_preselection.smk"
include: "rules/cut_DTFconverged.smk"
include: "rules/cut_trigger.smk"
include: "rules/cut_veto.smk"
include: "rules/cut_rectangular.smk"
include: "rules/add_index.smk"
include: "rules/massfit_forfom_mc.smk"
include: "rules/massfit_forfom_mc_plot.smk"
include: "rules/massfit_forfom_data.smk"
include: "rules/massfit_forfom_data_plot.smk"
include: "rules/mva.smk"
include: "rules/fom.smk"
include: "rules/cut_mva.smk"
include: "rules/remove_multiplecandidates.smk"
include: "rules/cut_KSmass.smk"
include: "rules/cut_Dmass.smk"
include: "rules/combine_datasets.smk"


# -------- load rules: mass fit --------


include: "rules/massfit_fortoy_mc.smk"
include: "rules/massfit_fortoy_mc_plot.smk"
include: "rules/massfit_fortoy_data.smk"
include: "rules/massfit_fortoy_data_plot.smk"
include: "rules/massfit_forcharmlessbkg_data.smk"
include: "rules/massfit_forcharmlessbkg_data_plot.smk"
include: "rules/massfit_mc.smk"
include: "rules/massfit_mc_plot.smk"
include: "rules/massfit_data.smk"
include: "rules/massfit_data_plot.smk"
include: "rules/massfit_data_sweight.smk"


# -------- load rules: analysis fit --------


include: "rules/ftcomb.smk"  # flavor tagging combination
include: "rules/dtrfit.smk"  # decay time resolution fit
include: "rules/dtrfit_plot.smk"
# include: "rules/dtafit.smk"  # decay time acceptance fit
# include: "rules/dtafit_plot.smk"
include: "rules/ftcalifit.smk"  # flavor tagging calibration fit
include: "rules/ftcalifit_plot.smk"
include: "rules/calibrate_flavor_tagging.smk"  # apply flavor tagging calibration results
include: "rules/bin_dalitz_plots.smk"  # Dalitz plot binning
include: "rules/fit_beta_generate_toy.smk"
include: "rules/fit_beta_generate_toy_plot.smk"
include: "rules/fit_beta.smk"  # beta angle fit
include: "rules/fit_beta_plot.smk"


# -------- load rules: checks and plots --------


include: "rules/checks.smk"
include: "rules/massfit_forfomcheck_data.smk"
include: "rules/massfit_forfomcheck_data_plot.smk"
include: "rules/plot.smk"


# -------- intermediate and final target rules --------
rule pidcorr_target:
    input:
        expand(
            f"data/derived/datasets/withpidcorr/{paramspace.wildcard_pattern}.root",
            eventtype=[11166117],
            KStype=["LL", "DD"],
            year=[2011, 2012, 2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
        ),


rule add_index_target:
    input:
        expand(
            f"data/derived/datasets/withindex/{paramspace.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            eventtype=[11166117, 90000000],
            KStype=["LL", "DD"],
            year=[2011, 2012, 2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            ipartition=range(npartitions),
            **{key: val["current"] for key, val in rectangular_cuts.items()},
        ),


rule full_selection_target:
    input:
        expand(
            f"data/derived/datasets/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            mvatype=["independent", "varcascaded", "combined", "cutcascaded_B"],
            eventtype=[11166117, 90000000],
            KStype=["LL", "DD"],
            year=[2011, 2012, 2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            ipartition=range(npartitions_aftermva),
            dz_significance_DB=["0.5"],
            KSD_FDCHI2_ORIVX=["0"],
            D_FDCHI2_ORIVX=["0"],
        ),


rule massfit_fortoy_target:
    input:
        expand(
            f"figures/massfit_fortoy_mc/hist_B_M/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pdf",
            mvatype=["independent", "varcascaded", "combined", "cutcascaded_B"],
            KStype=["LL", "DD"],
            run=[1, 2],
            **{key: val["current"] for key, val in rectangular_cuts.items()},
        ),
        expand(
            f"figures/massfit_fortoy_data/hist_B_M/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pdf",
            mvatype=["independent", "varcascaded", "combined", "cutcascaded_B"],
            KStype=["LL", "DD"],
            run=[1, 2],
            dz_significance_DB=["0.5"],
            KSD_FDCHI2_ORIVX=["0"],
            D_FDCHI2_ORIVX=["0"],
        ),


rule massfit_forcharmlessbkg_target:
    input:
        expand(
            f"figures/massfit_forcharmlessbkg_data/hist_B_M/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pdf",
            mvatype=["independent", "varcascaded", "combined", "cutcascaded_B"],
            KStype=["LL", "DD"],
            run=[1, 2],
            dz_significance_DB=["0.5"],
            KSD_FDCHI2_ORIVX=["0"],
            D_FDCHI2_ORIVX=["0"],
        ),


rule all:
    input:
        rules.fom_plot.output,
        rules.full_selection_target.input,
        rules.massfit_fortoy_target.input,
        rules.massfit_forcharmlessbkg_target.input,
    default_target: True
