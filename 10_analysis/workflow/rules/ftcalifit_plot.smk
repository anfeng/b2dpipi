rule ftcalifit_plot:
    input:
        model_filepath="workflow/scripts/ftcalifit_model.py",
        fitresult_filepath=rules.ftcalifit.output.fitresult_filepath,
        binning_filepath=rules.ftcalifit.output.binning_filepath,
    output:
        **{
            f"hist_dt_bin{i}": f"figures/ftcalifit/hist_dt/bin{i}/{paramspace_ftcalifit.wildcard_pattern}.pdf"
            for i in range(8)
        },
        **{
            f"hist_dt_separated_bin{i}": f"figures/ftcalifit/hist_dt_separated/bin{i}/{paramspace_ftcalifit.wildcard_pattern}.pdf"
            for i in range(8)
        },
        **{
            f"hist_dt_asym_bin{i}": f"figures/ftcalifit/hist_dt_asym/bin{i}/{paramspace_ftcalifit.wildcard_pattern}.pdf"
            for i in range(8)
        },
        hist_dt=f"figures/ftcalifit/hist_dt/{paramspace_ftcalifit.wildcard_pattern}.pdf",
        hist_dt_separated=f"figures/ftcalifit/hist_dt_separated/{paramspace_ftcalifit.wildcard_pattern}.pdf",
        hist_dt_asym=f"figures/ftcalifit/hist_dt_asym/{paramspace_ftcalifit.wildcard_pattern}.pdf",
        fig_eta_B0=f"figures/ftcalifit/fig_eta_B0/{paramspace_ftcalifit.wildcard_pattern}.pdf",
        fig_eta_B0bar=f"figures/ftcalifit/fig_eta_B0bar/{paramspace_ftcalifit.wildcard_pattern}.pdf",
    log:
        f"logs/ftcalifit_plot/{paramspace_ftcalifit.wildcard_pattern}.log",
        notebook=f"logs/notebooks/ftcalifit_plot/{paramspace_ftcalifit.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/ftcalifit_plot/{paramspace_ftcalifit.wildcard_pattern}.benchmark.txt"
    conda:
        "../envs/ftcalifit.yml"
    notebook:
        "../notebooks/ftcalifit_plot.py.ipynb"
