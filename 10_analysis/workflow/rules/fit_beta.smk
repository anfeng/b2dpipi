NUM_FITS = 10


rule fit_beta:
    input:
        model_filepath="workflow/scripts/fit_beta_model.py",
        b2dpipi_parameters_filepath="data/input/b2dpipi_parameters/M~{M}/N~{N}/param_source~{param_source}.yml",
        # KSpipi_KSLL_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSLL_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=2)
            for filepath in rules.bin_dalitz_plots.output
        ],
        # KSpipi_KSDD_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSDD_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=2)
            for filepath in rules.bin_dalitz_plots.output
        ],
        ftcalifit_fitresult_OS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="OS", run=2
        ),
        ftcalifit_fitresult_SS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="SS", run=2
        ),
        # dtrfit_fitresult_KSLL_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="LL", run=1
        # ),
        dtrfit_fitresult_KSLL_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="LL", run=2
        ),
        # dtrfit_fitresult_KSDD_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="DD", run=1
        # ),
        dtrfit_fitresult_KSDD_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="DD", run=2
        ),
    output:
        fitresult_filepath=f"data/output/fit_beta/fitresult/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.root",
    log:
        console=f"logs/fit_beta/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.log",
        notebook=f"logs/notebooks/fit_beta/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.benchmark.txt"
    wildcard_constraints:
        method="roofit",
    threads: config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta.py.ipynb"


rule fit_beta_accumulate_result:
    input:
        fitresult_filepaths=[
            rules.fit_beta.output.fitresult_filepath.replace("{run_index}", str(i))
            for i in range(NUM_FITS)
        ],
    output:
        fitresult_filepath=f"data/output/fit_beta/fitresult/{paramspace_fitbeta.wildcard_pattern}.root",
    log:
        f"logs/fit_beta_accumulate_result/{paramspace_fitbeta.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fit_beta_accumulate_result/{paramspace_fitbeta.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta_accumulate_result/{paramspace_fitbeta.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        method="roofit",
    threads: 2 * config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_accumulate_result.py.ipynb"


rule fit_beta_scipy:
    input:
        b2dpipi_parameters_filepath="data/input/b2dpipi_parameters/M~{M}/N~{N}/param_source~{param_source}.yml",
        model_filepath="workflow/scripts/fit_beta_model_scipy.py",
        pdf_expression_filepaths=[
            f"data/input/symbolic_expressions/pdf{i+1}.txt" for i in range(8)
        ],
        norm_expression_filepaths=[
            f"data/input/symbolic_expressions/normalization_factor_type3_{i+1}.txt"
            for i in range(8)
        ],
        # KSpipi_KSLL_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSLL_run2_filepaths=lambda wildcards: (
            (
                [
                    filepath.format(
                        datatype=wildcards["datatype"],
                        channel="KSpipi",
                        KStype="LL",
                        run=2,
                    )
                    for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
                ]
                if wildcards["datatype"] != "toy"
                else rules.fit_beta_generate_toy.output["toy_filepath"].format(
                    model_name="model_beta",
                    M=wildcards["M"],
                    N=wildcards["N"],
                    param_source="our_params",
                    event_num=90000,
                    run_index=0,
                )
            )
            if wildcards["model_name"] in ["model_beta", "model_combined"]
            else []
        ),
        # KSpipi_KSDD_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSDD_run2_filepaths=lambda wildcards: (
            (
                [
                    filepath.format(
                        datatype=wildcards["datatype"],
                        channel="KSpipi",
                        KStype="DD",
                        run=2,
                    )
                    for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
                ]
                if wildcards["datatype"] != "toy"
                else rules.fit_beta_generate_toy.output["toy_filepath"].format(
                    model_name="model_beta",
                    M=wildcards["M"],
                    N=wildcards["N"],
                    param_source="our_params",
                    event_num=90000,
                    run_index=0,
                )
            )
            if wildcards["model_name"] in ["model_beta", "model_combined"]
            else []
        ),
        KK_run2_filepaths=lambda wildcards: (
            (
                [
                    filepath.format(
                        datatype=wildcards["datatype"],
                        channel="KK",
                        KStype="NA",
                        run=2,
                    )
                    for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
                ]
            )
            if wildcards["model_name"] in ["model_hh", "model_combined"]
            else []
        ),
        pipi_run2_filepaths=lambda wildcards: (
            (
                [
                    filepath.format(
                        datatype=wildcards["datatype"],
                        channel="pipi",
                        KStype="NA",
                        run=2,
                    )
                    for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
                ]
                if wildcards["datatype"] != "toy"
                else []
            )
            if wildcards["model_name"] in ["model_hh", "model_combined"]
            else []
        ),
        Kpi_run2_filepaths=lambda wildcards: (
            (
                [
                    filepath.format(
                        datatype=wildcards["datatype"],
                        channel="Kpi",
                        KStype="NA",
                        run=2,
                    )
                    for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
                ]
                if wildcards["datatype"] != "toy"
                else []
            )
            if wildcards["model_name"] in ["model_hh", "model_combined"]
            else []
        ),
        # dtrfit_fitresult_KSLL_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="LL", run=1
        # ),
        dtrfit_fitresult_KSLL_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="LL", run=2
        ),
        # dtrfit_fitresult_KSDD_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="DD", run=1
        # ),
        dtrfit_fitresult_KSDD_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="DD", run=2
        ),
    output:
        fitresult_filepath=f"data/output/fit_beta/fitresult/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.pkl",
    log:
        console=f"logs/fit_beta/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.log",
        notebook=f"logs/notebooks/fit_beta/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta/{paramspace_fitbeta.wildcard_pattern}/{{run_index}}.benchmark.txt"
    wildcard_constraints:
        method="scipy",
    threads: config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_scipy.py.ipynb"


rule fit_beta_accumulate_result_scipy:
    input:
        fitresult_filepaths=[
            rules.fit_beta_scipy.output.fitresult_filepath.replace(
                "{run_index}", str(i)
            )
            for i in range(NUM_FITS)
        ],
    output:
        fitresult_filepath=f"data/output/fit_beta/fitresult/{paramspace_fitbeta.wildcard_pattern}.pkl",
    log:
        f"logs/fit_beta_accumulate_result/{paramspace_fitbeta.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fit_beta_accumulate_result/{paramspace_fitbeta.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta_accumulate_result/{paramspace_fitbeta.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        method="scipy",
    threads: config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_accumulate_result_scipy.py.ipynb"
