rule remove_multiplecandidates:
    input:
        expand_partitions(
            f"data/derived/datasets/cut_mva/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/remove_multiplecandidates/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
    log:
        f"logs/remove_multiplecandidates/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/remove_multiplecandidates/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/remove_multiplecandidates/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        mva_response_name=lambda wildcards: (
            "MVA_response" if wildcards.mvatype == "combined" else "MVA_B_response"
        ),
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/remove_multiplecandidates.py.ipynb"
