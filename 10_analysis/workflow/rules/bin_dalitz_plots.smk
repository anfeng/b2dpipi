rule bin_dalitz_plots:
    input:
        data_filepaths=lambda wildcards: (
            rules.calibrate_flavor_tagging.output
            if wildcards["channel"] == "KSpipi"
            else rules.calibrate_flavor_tagging_hh.output
        ),
        b2dpipi_binning_filepath="data/input/binning_schemes/b2dpipi_equalphase.root",
        d2kspipi_binning_filepath="data/input/binning_schemes/KsPiPi_optimal.root",
    output:
        data_filepaths=expand_partitions(
            f"data/derived/datasets/bin_dalitz_plots/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
        hist_binning=f"data/output/hist_binning/{paramspace_afterselection.wildcard_pattern}.root",
    log:
        f"logs/bin_dalitz_plots/{paramspace_afterselection.wildcard_pattern}.log",
        notebook=f"logs/notebooks/bin_dalitz_plots/{paramspace_afterselection.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/bin_dalitz_plots/{paramspace_afterselection.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/bin_dalitz_plots.yml"
    notebook:
        "../notebooks/bin_dalitz_plots.py.ipynb"
