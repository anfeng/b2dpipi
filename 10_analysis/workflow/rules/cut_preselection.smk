rule cut_preselection:
    input:
        expand_partitions(
            f"data/derived/datasets/remove_kpiswapmultiplecandidates/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/cut_preselection/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    log:
        f"logs/cut_preselection/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_preselection/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_preselection/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        criteria=lambda wildcards: (
            "KSD_FDCHI2_ORIVX > 5"
            if wildcards.KStype == "LL"
            else "KSD_FDCHI2_ORIVX > 25"
        ),
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/filter.py.ipynb"
