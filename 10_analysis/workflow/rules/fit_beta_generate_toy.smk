rule fit_beta_generate_toy:
    input:
        b2dpipi_parameters_filepath="data/input/b2dpipi_parameters/M~{M}/N~{N}/param_source~{param_source}.yml",
        model_filepath="workflow/scripts/fit_beta_model_scipy.py",
        pdf_expression_filepaths=[
            f"data/input/symbolic_expressions/pdf{i+1}.txt" for i in range(8)
        ],
        norm_expression_filepaths=[
            f"data/input/symbolic_expressions/normalization_factor_type3_{i+1}.txt"
            for i in range(8)
        ],
        ftcalifit_fitresult_OS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="OS", run=2
        ),
        ftcalifit_fitresult_SS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="SS", run=2
        ),
        # dtrfit_fitresult_KSLL_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="LL", run=1
        # ),
        dtrfit_fitresult_KSLL_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="LL", run=2
        ),
        # dtrfit_fitresult_KSDD_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="DD", run=1
        # ),
        dtrfit_fitresult_KSDD_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="DD", run=2
        ),
        KSpipi_KSLL_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
        KSpipi_KSDD_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
    output:
        toy_filepath=f"data/output/fit_beta_generate_toy/toy/{paramspace_fitbetagentoy.wildcard_pattern}.parquet",
    log:
        console=f"logs/fit_beta_generate_toy/{paramspace_fitbetagentoy.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fit_beta_generate_toy/{paramspace_fitbetagentoy.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta_generate_toy/{paramspace_fitbetagentoy.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_generate_toy.py.ipynb"
