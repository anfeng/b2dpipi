rule fit_beta_generate_toy_plot:
    input:
        b2dpipi_parameters_filepath="data/input/b2dpipi_parameters/M~{M}/N~{N}/param_source~{param_source}.yml",
        model_filepath="workflow/scripts/fit_beta_model_scipy.py",
        pdf_expression_filepaths=[
            f"data/input/symbolic_expressions/pdf{i+1}.txt" for i in range(8)
        ],
        norm_expression_filepaths=[
            f"data/input/symbolic_expressions/normalization_factor_type3_{i+1}.txt"
            for i in range(8)
        ],
        ftcalifit_fitresult_OS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="OS", run=2
        ),
        ftcalifit_fitresult_SS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="SS", run=2
        ),
        # dtrfit_fitresult_KSLL_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="LL", run=1
        # ),
        dtrfit_fitresult_KSLL_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="LL", run=2
        ),
        # dtrfit_fitresult_KSDD_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
        #     datatype="mc", channel="KSpipi", KStype="DD", run=1
        # ),
        dtrfit_fitresult_KSDD_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="DD", run=2
        ),
        data_Kpi_filepaths=lambda wildcards: [
            rules.ftcomb_convert_to_parquet_file.output[0].format(
                datatype="data",
                channel="Kpi",
                KStype="NA",
                run=2,
                ipartition=ipartition,
            )
            for ipartition in range(B2Dpipi_D2hh_workflow.npartitions)
        ],
        KSpipi_KSLL_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
        KSpipi_KSDD_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
        toy_filepath=rules.fit_beta_generate_toy.output["toy_filepath"],
        hist_binning=rules.bin_dalitz_plots.output["hist_binning"].format(
            datatype="data", channel="KSpipi", KStype="LL", run="2"
        ),
    output:
        hist_dt_separated=f"figures/fit_beta_generate_toy/hist_dt_separated/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dt_asym=f"figures/fit_beta_generate_toy/hist_dt_asym/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dt_asym_transformed=f"figures/fit_beta_generate_toy/hist_dt_asym_transformed/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_binning_D=f"figures/fit_beta_generate_toy/hist_binning_D/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_binning_B=f"figures/fit_beta_generate_toy/hist_binning_B/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_toy_binned_D=f"figures/fit_beta_generate_toy/hist_dalitz_toy_binned_D/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_toy_binned_B=f"figures/fit_beta_generate_toy/hist_dalitz_toy_binned_B/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_toy_binned_B_B0=f"figures/fit_beta_generate_toy/hist_dalitz_toy_binned_B_B0/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_toy_binned_B_B0bar=f"figures/fit_beta_generate_toy/hist_dalitz_toy_binned_B_B0bar/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_binned_D=f"figures/fit_beta_generate_toy/hist_dalitz_data_binned_D/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_binned_B=f"figures/fit_beta_generate_toy/hist_dalitz_data_binned_B/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_binned_B_B0=f"figures/fit_beta_generate_toy/hist_dalitz_data_binned_B_B0/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_binned_B_B0bar=f"figures/fit_beta_generate_toy/hist_dalitz_data_binned_B_B0bar/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_signalchannel_unbinned_D=f"figures/fit_beta_generate_toy/hist_dalitz_data_signalchannel_unbinned_D/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_signalchannel_unbinned_B=f"figures/fit_beta_generate_toy/hist_dalitz_data_signalchannel_unbinned_B/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_signalchannel_unbinned_B_B0=f"figures/fit_beta_generate_toy/hist_dalitz_data_signalchannel_unbinned_B_B0/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_signalchannel_unbinned_B_B0bar=f"figures/fit_beta_generate_toy/hist_dalitz_data_signalchannel_unbinned_B_B0bar/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_controlchannel_unbinned_B=f"figures/fit_beta_generate_toy/hist_dalitz_data_controlchannel_unbinned_B/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_controlchannel_unbinned_B_B0=f"figures/fit_beta_generate_toy/hist_dalitz_data_controlchannel_unbinned_B_B0/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
        hist_dalitz_data_controlchannel_unbinned_B_B0bar=f"figures/fit_beta_generate_toy/hist_dalitz_data_controlchannel_unbinned_B_B0bar/{paramspace_fitbetagentoy.wildcard_pattern}.pdf",
    log:
        console=f"logs/fit_beta_generate_toy_plot/{paramspace_fitbetagentoy.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fit_beta_generate_toy_plot/{paramspace_fitbetagentoy.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta_generate_toy_plot/{paramspace_fitbetagentoy.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_generate_toy_plot.py.ipynb"
