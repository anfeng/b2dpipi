rule fit_beta_plot:
    input:
        model_filepath="workflow/scripts/fit_beta_model.py",
        b2dpipi_parameters_filepath="data/input/b2dpipi_parameters/M~{M}/N~{N}/param_source~{param_source}.yml",
        fitresult_filepath=rules.fit_beta_accumulate_result.output.fitresult_filepath,
        # binning_filepath=rules.ftcalifit.output.binning_filepath,
        ftcalifit_fitresult_OS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="OS", run=2
        ),
        ftcalifit_fitresult_SS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="SS", run=2
        ),
        dtrfit_fitresult_KSLL_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="LL", run=1
        ),
        dtrfit_fitresult_KSLL_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="LL", run=2
        ),
        dtrfit_fitresult_KSDD_run1_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="DD", run=1
        ),
        dtrfit_fitresult_KSDD_run2_filepath=rules.dtrfit.output.fitresult_filepath.format(
            datatype="mc", channel="KSpipi", KStype="DD", run=2
        ),
    output:
        hist_dt=f"figures/fit_beta/hist_dt/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_separated=f"figures/fit_beta/hist_dt_separated/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_asym=f"figures/fit_beta/hist_dt_asym/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_Dbincombined=directory(
            f"figures/fit_beta/hist_dt_Dbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_separated_Dbincombined=directory(
            f"figures/fit_beta/hist_dt_separated_Dbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_asym_Dbincombined=directory(
            f"figures/fit_beta/hist_dt_asym_Dbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_Bbincombined=directory(
            f"figures/fit_beta/hist_dt_Bbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_separated_Bbincombined=directory(
            f"figures/fit_beta/hist_dt_separated_Bbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_asym_Bbincombined=directory(
            f"figures/fit_beta/hist_dt_asym_Bbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        # hist_dt_singlebin=directory("figures/fit_beta/hist_dt_singlebin"),
        # hist_dt_separated_singlebin=directory(
        #     "figures/fit_beta/hist_dt_separated_singlebin"
        # ),
        # hist_dt_asym_singlebin=directory("figures/fit_beta/hist_dt_asym_singlebin"),
        fig_cs=f"figures/fit_beta/fig_cs/{paramspace_fitbeta.wildcard_pattern}.pdf",
        fig_nll_beta=f"figures/fit_beta/fig_nll_beta/{paramspace_fitbeta.wildcard_pattern}.pdf",
        fig_nll_cs=directory(
            f"figures/fit_beta/fig_nll_cs/{paramspace_fitbeta.wildcard_pattern}"
        ),
    log:
        f"logs/fit_beta_plot/{paramspace_fitbeta.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fit_beta_plot/{paramspace_fitbeta.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta_plot/{paramspace_fitbeta.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        method="roofit",
    threads: config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_plot.py.ipynb"


rule fit_beta_plot_calculate_pdf_values_scipy:
    input:
        b2dpipi_parameters_filepath="data/input/b2dpipi_parameters/M~{M}/N~{N}/param_source~{param_source}.yml",
        model_filepath="workflow/scripts/fit_beta_model_scipy.py",
        pdf_expression_filepaths=[
            f"data/input/symbolic_expressions/pdf{i+1}.txt" for i in range(8)
        ],
        norm_expression_filepaths=[
            f"data/input/symbolic_expressions/normalization_factor_type3_{i+1}.txt"
            for i in range(8)
        ],
        fitresult_filepath=rules.fit_beta_accumulate_result_scipy.output.fitresult_filepath,
        # KSpipi_KSLL_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSLL_run2_filepaths=rules.fit_beta_scipy.input[
            "KSpipi_KSLL_run2_filepaths"
        ],
        # KSpipi_KSDD_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSDD_run2_filepaths=rules.fit_beta_scipy.input[
            "KSpipi_KSDD_run2_filepaths"
        ],
        KK_run2_filepaths=rules.fit_beta_scipy.input["KK_run2_filepaths"],
        pipi_run2_filepaths=rules.fit_beta_scipy.input["pipi_run2_filepaths"],
        Kpi_run2_filepaths=rules.fit_beta_scipy.input["Kpi_run2_filepaths"],
    output:
        pdf_values=f"data/output/fit_beta/pdf_values/{paramspace_fitbeta.wildcard_pattern}.npz",
    log:
        f"logs/fit_beta_plot_calculate_pdf_values/{paramspace_fitbeta.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fit_beta_plot_calculate_pdf_values/{paramspace_fitbeta.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta_plot_calculate_pdf_values/{paramspace_fitbeta.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        method="scipy",
    threads: 2 * config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_plot_calculate_pdf_values_scipy.py.ipynb"


rule fit_beta_plot_scipy:
    input:
        b2dpipi_parameters_filepath="data/input/b2dpipi_parameters/M~{M}/N~{N}/param_source~{param_source}.yml",
        model_filepath="workflow/scripts/fit_beta_model_scipy.py",
        pdf_expression_filepaths=[
            f"data/input/symbolic_expressions/pdf{i+1}.txt" for i in range(8)
        ],
        norm_expression_filepaths=[
            f"data/input/symbolic_expressions/normalization_factor_type3_{i+1}.txt"
            for i in range(8)
        ],
        fitresult_filepath=rules.fit_beta_accumulate_result_scipy.output[
            "fitresult_filepath"
        ],
        KSpipi_KSLL_run2_filepaths=rules.fit_beta_scipy.input[
            "KSpipi_KSLL_run2_filepaths"
        ],
        KSpipi_KSDD_run2_filepaths=rules.fit_beta_scipy.input[
            "KSpipi_KSDD_run2_filepaths"
        ],
        KK_run2_filepaths=rules.fit_beta_scipy.input["KK_run2_filepaths"],
        pipi_run2_filepaths=rules.fit_beta_scipy.input["pipi_run2_filepaths"],
        Kpi_run2_filepaths=rules.fit_beta_scipy.input["Kpi_run2_filepaths"],
        pdf_values=rules.fit_beta_plot_calculate_pdf_values_scipy.output["pdf_values"],
    output:
        hist_dt=f"figures/fit_beta/hist_dt/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_separated=f"figures/fit_beta/hist_dt_separated/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_asym=f"figures/fit_beta/hist_dt_asym/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_Dbincombined=directory(
            f"figures/fit_beta/hist_dt_Dbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_separated_Dbincombined=directory(
            f"figures/fit_beta/hist_dt_separated_Dbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_asym_Dbincombined=directory(
            f"figures/fit_beta/hist_dt_asym_Dbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_Bbincombined=directory(
            f"figures/fit_beta/hist_dt_Bbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_separated_Bbincombined=directory(
            f"figures/fit_beta/hist_dt_separated_Bbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_asym_Bbincombined=directory(
            f"figures/fit_beta/hist_dt_asym_Bbincombined/{paramspace_fitbeta.wildcard_pattern}"
        ),
        hist_dt_asym_transformed=f"figures/fit_beta/hist_dt_asym_transformed/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_asym_transformed_paramsfrommodel=f"figures/fit_beta/hist_dt_asym_transformed_paramsfrommodel/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_asym_weighted=f"figures/fit_beta/hist_dt_asym_weighted/{paramspace_fitbeta.wildcard_pattern}.pdf",
        hist_dt_asym_weighted_paramsfrommodel=f"figures/fit_beta/hist_dt_asym_weighted_paramsfrommodel/{paramspace_fitbeta.wildcard_pattern}.pdf",
        # hist_dt_singlebin=directory("figures/fit_beta/hist_dt_singlebin"),
        # hist_dt_separated_singlebin=directory(
        #     "figures/fit_beta/hist_dt_separated_singlebin"
        # ),
        # hist_dt_asym_singlebin=directory("figures/fit_beta/hist_dt_asym_singlebin"),
        fig_cs=f"figures/fit_beta/fig_cs/{paramspace_fitbeta.wildcard_pattern}.pdf",
        # fig_nll_beta=f"figures/fit_beta/fig_nll_beta/{paramspace_fitbeta.wildcard_pattern}.pdf",
        # fig_nll_cs=directory(
        #     f"figures/fit_beta/fig_nll_cs/{paramspace_fitbeta.wildcard_pattern}"
        # ),
    log:
        f"logs/fit_beta_plot/{paramspace_fitbeta.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fit_beta_plot/{paramspace_fitbeta.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fit_beta_plot/{paramspace_fitbeta.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        method="scipy",
    threads: 2 * config["threads"]
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/fit_beta_plot_scipy.py.ipynb"
