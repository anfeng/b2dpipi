pidcorr_config = lambda wildcards: {
    "__input_tree_name__": "'DecayTree'",
    "__dataset__": f"'{wildcards.polarity[:3].capitalize()}{wildcards.polarity[3:].capitalize()}_{wildcards.year}'",
    "__simversion__": "'sim09'" if wildcards.year in ["2011", "2012"] else "'run2'",
    "__ptvar__": "'PT'",
    "__pvar__": "'P'",
    "__etavar__": "None",
    "__ntrvar__": "'nTracks'",
    "__tracks__": {
        particle_name: {
            "ProbNNp": (
                "pi_V3ProbNNp"
                if wildcards.year in ["2011", "2012"]
                else "pi_MC15TuneV1_ProbNNp_Brunel"
            ),
            "ProbNNk": (
                "pi_V3ProbNNK"
                if wildcards.year in ["2011", "2012"]
                else "pi_MC15TuneV1_ProbNNK_Brunel"
            ),
            "ProbNNpi": (
                "pi_V3ProbNNpi"
                if wildcards.year in ["2011", "2012"]
                else "pi_MC15TuneV1_ProbNNpi_Brunel"
            ),
        }
        for particle_name in ["KKst", "piKst", "pipD", "pimD", "pipKS", "pimKS"]
    },
}

branches = [
    [f"{particle_name}_{probnntype}_corr", f"{particle_name}_{probnntype}"]
    for particle_name in ["KKst", "piKst", "pipD", "pimD", "pipKS", "pimKS"]
    for probnntype in ["ProbNNp", "ProbNNk", "ProbNNpi"]
]


rule downgrade_root:
    input:
        f"data/derived/datasets/mctruthmatched/{paramspace.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/mctruthmatched-oldroot/{paramspace.wildcard_pattern}.root",
    benchmark:
        f"benchmarks/downgrade_root/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="DecayTree",
        output_tree_name="DecayTree",
        criteria="true",
    wildcard_constraints:
        eventtype="11166117",
    threads: config["threads"]
    resources:
        mem_mib=2000,
    conda:
        "../envs/root6.24.yml"
    notebook:
        "../notebooks/filter_root.py.ipynb"


rule pidcorr_mc:
    input:
        f"data/derived/datasets/mctruthmatched-oldroot/{paramspace.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/withpidcorr/{paramspace.wildcard_pattern}.root",
    log:
        f"logs/pidcorr_mc/{paramspace.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/pidcorr_mc/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        pidcorr_config=pidcorr_config,
    wildcard_constraints:
        eventtype="11166117",
    run:
        import tempfile
        import subprocess
        import re

        with open("workflow/scripts/pidcorr.py") as f:
            pidcorr_codes = f.read()

        with tempfile.NamedTemporaryFile(delete=False) as fp:
            fp.write(
                bytearray(
                    re.sub(
                        f"({'|'.join(params.pidcorr_config.keys())})",
                        lambda m: f"{params.pidcorr_config[m.group(1)]}",
                        pidcorr_codes,
                    )
                    .replace("__input_filepath__", f"'{input[0]}'")
                    .replace("__output_filepath__", f"'{output[0]}'"),
                    encoding="utf-8",
                )
            )
            fp.close()
            subprocess.run(
                f"source /cvmfs/lhcb.cern.ch/lib/LbEnv && (cat ~/cert_pass.txt | lhcb-proxy-init) && lb-run Urania/latest python {fp.name} --usefuncs | tee {log[0]}",
                shell=True,
            )


# for data, directly copy original PID variables with no transformation
rule pidcorr_data:
    input:
        f"data/derived/datasets/mctruthmatched/{paramspace.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/withpidcorr/{paramspace.wildcard_pattern}.root",
    log:
        f"logs/withpidcorr/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/withpidcorr/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/pidcorr_data/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="DecayTree",
        output_tree_name="DecayTree",
        branches=branches,
    wildcard_constraints:
        eventtype="90000000",
    threads: config["threads"]
    resources:
        mem_mib=40000,
    conda:
        "../envs/define_columns_root.yml"
    notebook:
        "../notebooks/define_columns_root.py.ipynb"
