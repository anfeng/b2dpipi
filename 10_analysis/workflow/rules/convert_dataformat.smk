rule split_rootfiles:
    input:
        f"data/derived/datasets/arraycolumnsremoved/{paramspace.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/partitioned/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.root",
    log:
        f"logs/split_rootfiles/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
        notebook=f"logs/notebooks/split_rootfiles/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/split_rootfiles/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="DecayTree",
        output_tree_name="DecayTree",
        ipartition=lambda wildcards: int(wildcards["ipartition"]),
        npartitions=npartitions,
    threads: 1  # This is required by RDataFrame.Range()
    resources:
        mem_mib=2000,
    conda:
        "../envs/split_rootfiles.yml"
    notebook:
        "../notebooks/split_rootfiles.py.ipynb"


rule convert_dataformat:
    input:
        f"data/derived/datasets/partitioned/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/to_parquet/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
    log:
        f"logs/convert_dataformat/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
        notebook=f"logs/notebooks/convert_dataformat/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/convert_dataformat/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="DecayTree",
    threads: 1  # Setting it > 1 may cause incorrect results
    resources:
        mem_mib=2000,
    conda:
        "../envs/convert_dataformat.yml"
    notebook:
        "../notebooks/convert_dataformat.py.ipynb"
