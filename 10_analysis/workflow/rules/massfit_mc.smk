rule massfit_mc:
    input:
        model_filepath="workflow/scripts/massfit_model_2.py",
        data_filepaths=lambda wildcards: [
            filepath.format(
                datatype="data",
                channel="KSpipi",
                KStype=wildcards["KStype"],
                run=wildcards["run"],
            )
            for filepath in rules.combine_datasets.output
        ],
        mc_Bd2Dpipi_filepaths=lambda wildcards: [
            filepath.format(
                datatype="mc",
                channel="KSpipi",
                KStype=wildcards["KStype"],
                run=wildcards["run"],
            )
            for filepath in rules.combine_datasets.output
        ],
        mc_Bd2DKpi_filepaths="data/input/Bd2DKpi_Kpi/Bd2piKD0-D02Kpi-MC-Run{run}-MagCombined-withMCtruth-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withWeights-withSelection.root",
        mc_Bs2DKpi_filepaths="data/input/Bs2DKpi_Kpi/Bs2KpiD0-D02Kpi-MC-Run{run}-MagCombined-withMCtruth-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withWeights-withSelection.root",
        mc_Lb2Dppi_filepaths="data/input/Lb2Dppi_Kpi/Lb2ppiD0-D02Kpi-MC-Run{run}-MagCombined-withMCtruth-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withWeights-withSelection.root",
        mc_Bu2Dstpi_Dst2Dpi0_filepaths="data/input/Bu2Dstpi_Dst2Dpi0_Kpi/Bu2piDst-D02Kpi-Dst2Dpi0-MC-Run{run}-MagCombined-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withSelection.root",
        mc_Bu2Dstpi_Dst2Dgamma_filepaths="data/input/Bu2Dstpi_Dst2Dgamma_Kpi/Bu2piDst-D02Kpi-MC-Run{run}-MagCombined-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withSelection.root",
        mc_Bd2Dstpi_Dst2Dpi_filepaths="data/input/Bd2Dstpi_Dst2Dpi_Kpi/Bd2piDst-D02Kpi-Dst2Dpi-MC-Run{run}-MagCombined-withDNN-withPIDcorr-withMVA-withPIDMVA-WithDpiMatching-withSwappedMassHypotheses-withKKvetoes-withSelection.root",
    output:
        fitresult_filepath=f"data/output/massfit_mc/fitresult/{paramspace_massfit.wildcard_pattern}.root",
    log:
        f"logs/massfit_mc/{paramspace_massfit.wildcard_pattern}.log",
        notebook=f"logs/notebooks/massfit_mc/{paramspace_massfit.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_mc/{paramspace_massfit.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/massfit.yml"
    notebook:
        "../notebooks/massfit_mc.py.ipynb"
