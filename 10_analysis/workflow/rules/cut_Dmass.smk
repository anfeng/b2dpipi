rule cut_Dmass:
    input:
        expand_partitions(
            f"data/derived/datasets/cut_KSmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
    log:
        f"logs/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        criteria="((B_ConsKSPV_D0_M > 1864.84 - 25) & (B_ConsKSPV_D0_M < 1864.84 + 25))",
    threads: min(config["threads"], npartitions_aftermva)
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/filter.py.ipynb"
