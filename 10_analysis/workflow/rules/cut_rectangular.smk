def get_input(wildcards):
    for variable_name in rectangular_cuts.keys():
        current_value = wildcards[variable_name]
        index = rectangular_cuts[variable_name]["current"].index(current_value)
        previous_value = rectangular_cuts[variable_name]["previous"][index]
        if current_value != previous_value:
            wildcards_new = dict(wildcards)
            wildcards_new[variable_name] = previous_value
            return [
                filepath.format(**wildcards_new)
                for filepath in expand_partitions(
                    f"data/derived/datasets/cut_rectangular/{paramspace.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
                    "ipartition",
                    range(npartitions),
                )
            ]
        else:
            continue
    return expand_partitions(
        f"data/derived/datasets/cut_veto/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
        "ipartition",
        range(npartitions),
    )


rule cut_rectangular:
    input:
        get_input,
    output:
        expand_partitions(
            f"data/derived/datasets/cut_rectangular/{paramspace.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    log:
        f"logs/cut_rectangular/{paramspace.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_rectangular/{paramspace.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_rectangular/{paramspace.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        criteria=lambda wildcards: " & ".join(
            [
                f"({variable_name} > {variable_threshold})"
                for variable_name, variable_threshold in paramspace_rectcut.instance(
                    wildcards
                ).items()
            ]
        ),
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/filter.py.ipynb"
