# Omitting input to avoid lengthy notebook preamble causing vscode to crash
# Needs full_selection_target for inputs
rule plot:
    output:
        "figures/plot.png",
    log:
        f"logs/plot.log",
        notebook=f"logs/notebooks/plot.py.ipynb",
    benchmark:
        f"benchmarks/plot.benchmark.txt"
    threads: 50
    conda:
        "../envs/fit_beta.yml"
    notebook:
        "../notebooks/plot.py.ipynb"
