rule dtrfit:
    input:
        data_filepaths=lambda wildcards: [
            rules.ftcomb_convert_to_parquet_file.output[0].format(
                ipartition=ipartition, **wildcards
            )
            for ipartition in range(npartitions_aftermva)
        ],
    output:
        fitresult_filepath=f"data/output/dtrfit/fitresult/{paramspace_afterselection.wildcard_pattern}.root",
    log:
        f"logs/dtrfit/{paramspace_afterselection.wildcard_pattern}.log",
        notebook=f"logs/notebooks/dtrfit/{paramspace_afterselection.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/dtrfit/{paramspace_afterselection.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        datatype="mc",
    threads: config["threads"]
    conda:
        "../envs/dtrfit.yml"
    notebook:
        "../notebooks/dtrfit.py.ipynb"
