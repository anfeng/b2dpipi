criteria = " & ".join(
    [
        "(B_ConsD0PV_D0pi1_M > 2020)",
        "(B_ConsD0PV_D0pi2_M > 2020)",
        "(pipKS_isMuon == 0)",
        "(pimKS_isMuon == 0)",
        "(pipD_isMuon == 0)",
        "(pimD_isMuon == 0)",
        "(piKst_isMuon == 0)",
        "(KKst_isMuon == 0)",
    ]
)


rule cut_veto:
    input:
        expand_partitions(
            f"data/derived/datasets/cut_trigger/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/cut_veto/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    log:
        f"logs/cut_veto/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_veto/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_veto/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        criteria=criteria,
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/filter.py.ipynb"
