criteria = " & ".join(
    [
        "(B_ConsPV_status == 0)",
        "(B_ConsKSPV_status == 0)",
        "(B_ConsD0PV_status == 0)",
        "(B_ConsD0KSPV_status == 0)",
    ]
)


rule cut_DTFconverged:
    input:
        expand_partitions(
            f"data/derived/datasets/cut_preselection/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/cut_DTFconverged/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    log:
        f"logs/cut_DTFconverged/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_DTFconverged/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_DTFconverged/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        criteria=criteria,
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/filter.py.ipynb"
