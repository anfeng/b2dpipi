rule dtafit_plot:
    input:
        model_filepath="workflow/scripts/dtafit_model.py",
        fitresult_filepath=f"data/output/dtafit/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    output:
        hist_dt=f"figures/dtafit/hist_dt/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pdf",
    log:
        f"logs/dtafit_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/dtafit_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/dtafit_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    conda:
        "../envs/dtafit.yml"
    notebook:
        "../notebooks/dtafit_plot.py.ipynb"
