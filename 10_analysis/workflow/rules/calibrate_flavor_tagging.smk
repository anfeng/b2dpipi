rule calibrate_flavor_tagging:
    input:
        data_filepaths=lambda wildcards: expand_partitions(
            f"data/derived/datasets/ftcombed/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
        ftcalifit_fitresult_OS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="OS", run=2
        ),
        ftcalifit_fitresult_SS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="SS", run=2
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
    log:
        f"logs/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}.log",
        notebook=f"logs/notebooks/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        channel="KSpipi",
    threads: config["threads"]
    conda:
        "../envs/ftcalifit.yml"
    notebook:
        "../notebooks/calibrate_flavor_tagging.py.ipynb"


# temporarily use a different rule for channel=Kpi,KK,pipi for single-file output, as multi-file output fails for channel=Kpi,KK,pipi because of dask's bug
rule calibrate_flavor_tagging_hh:
    input:
        # data_filepaths=lambda wildcards: [
        #     rules.B2Dpipi_D2hh_convert_dataformat.output[0].format(
        #         datatype=wildcards["datatype"],
        #         channel=wildcards["channel"],
        #         run=wildcards["run"],
        #         ipartition=ipartition,
        #     )
        #     for ipartition in range(B2Dpipi_D2hh_workflow.npartitions)
        # ],
        data_filepaths=lambda wildcards: (
            expand_partitions(
                rules.ftcomb_convert_to_parquet_file.output[0],
                "ipartition",
                    range(B2Dpipi_D2hh_workflow.npartitions),
                )
            if wildcards["datatype"] != "toy"
            else [
                rules.B2Dpipi_D2hh_convert_dataformat.output[0].format(
                    datatype=wildcards["datatype"],
                    channel=wildcards["channel"],
                    run=wildcards["run"],
                    ipartition=ipartition,
                )
                for ipartition in range(B2Dpipi_D2hh_workflow.npartitions)
            ]
        ),
        ftcalifit_fitresult_OS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="OS", run=2
        ),
        ftcalifit_fitresult_SS_run2_filepath=rules.ftcalifit.output.fitresult_filepath.format(
            taggertype="SS", run=2
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(1),
        ),
    log:
        f"logs/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}.log",
        notebook=f"logs/notebooks/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/calibrate_flavor_tagging/{paramspace_afterselection.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        channel="Kpi|KK|pipi",
    threads: config["threads"]
    conda:
        "../envs/ftcalifit.yml"
    notebook:
        "../notebooks/calibrate_flavor_tagging.py.ipynb"
