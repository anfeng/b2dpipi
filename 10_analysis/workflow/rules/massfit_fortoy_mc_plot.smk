rule massfit_fortoy_mc_plot:
    input:
        fitresult_filepath=f"data/output/massfit_fortoy_mc/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    output:
        hist_B_M=f"figures/massfit_fortoy_mc/hist_B_M/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pdf",
    log:
        f"logs/massfit_fortoy_mc_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/massfit_fortoy_mc_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_fortoy_mc_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    conda:
        "../envs/analysis.yml"
    notebook:
        "../notebooks/massfit_fortoy_mc_plot.py.ipynb"
