rule massfit_forfom_data_plot:
    input:
        fitresult_filepath=f"data/output/massfit_forfom_data/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    output:
        hist_B_M=f"figures/massfit_forfom_data/hist_B_M/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pdf",
        eventnum_filepath=f"data/output/massfit_forfom_data/eventnum/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.yml",
    log:
        f"logs/massfit_forfom_data_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/massfit_forfom_data_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_forfom_data_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    conda:
        "../envs/massfit.yml"
    notebook:
        "../notebooks/massfit_forfom_data_plot.py.ipynb"
