id_equal = " && ".join(
    [
        "(TMath::Abs(B_TRUEID) == 511)",
        "(TMath::Abs(D_TRUEID) == 421)",
        "(TMath::Abs(KSD_TRUEID) == 310)",
        "(TMath::Abs(pipKS_TRUEID) == 211)",
        "(TMath::Abs(pimKS_TRUEID) == 211)",
        "(TMath::Abs(pipD_TRUEID) == 211)",
        "(TMath::Abs(pimD_TRUEID) == 211)",
        "(TMath::Abs(KKst_TRUEID) == 211)",
        "(TMath::Abs(piKst_TRUEID) == 211)",
    ]
)
mother_key_equal = " && ".join(
    [
        "(D_MC_MOTHER_KEY == KKst_MC_MOTHER_KEY)",
        "(D_MC_MOTHER_KEY == piKst_MC_MOTHER_KEY)",
        "(KSD_MC_MOTHER_KEY == pipD_MC_MOTHER_KEY)",
        "(KSD_MC_MOTHER_KEY == pimD_MC_MOTHER_KEY)",
        "(KSD_MC_GD_MOTHER_KEY == D_MC_MOTHER_KEY)",
        "(pipKS_MC_MOTHER_KEY == pimKS_MC_MOTHER_KEY)",
        "(pipKS_MC_GD_MOTHER_KEY == KSD_MC_MOTHER_KEY)",
        "(pipKS_MC_GD_GD_MOTHER_KEY == D_MC_MOTHER_KEY)",
    ]
)
bkgcat_condition = " && ".join(
    [
        "((B_BKGCAT <= 30) || (B_BKGCAT == 50))",
        "((D_BKGCAT <= 30) || (D_BKGCAT == 50))",
        "((KSD_BKGCAT <= 30) || (KSD_BKGCAT == 50))",
    ]
)


# for real data, this rule does nothing
rule match_mctruth:
    input:
        "data/input/{eventtype}_{year}_{polarity}.root",
    output:
        f"data/derived/datasets/mctruthmatched/{paramspace.wildcard_pattern}.root",
    log:
        f"logs/match_mctruth/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/match_mctruth/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/match_mctruth/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name=lambda wildcards: (
            "B02D0KPiD2KSHHLL/DecayTree"
            if wildcards.KStype == "LL"
            else "B02D0KPiD2KSHHDD/DecayTree"
        ),
        output_tree_name="DecayTree",
        criteria=lambda wildcards: (
            f"({id_equal}) && ({mother_key_equal}) && ({bkgcat_condition})"
            if wildcards.eventtype == "11166117"
            else "true"
        ),
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards: (
            40000 if wildcards["eventtype"] == "90000000" else 2000
        ),
    conda:
        "../envs/filter_root.yml"
    notebook:
        "../notebooks/filter_root.py.ipynb"
