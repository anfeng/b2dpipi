rule massfit_forfomcheck_data_plot:
    input:
        fitresult_filepath=f"data/output/massfit_forfomcheck_data/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.root",
    output:
        hist_B_M=f"figures/massfit_forfomcheck_data/hist_B_M/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.pdf",
        eventnum_filepath=f"data/output/massfit_forfomcheck_data/eventnum/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.yml",
    log:
        f"logs/massfit_forfomcheck_data_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.log",
        notebook=f"logs/notebooks/massfit_forfomcheck_data_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_forfomcheck_data_plot/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.benchmark.txt"
    wildcard_constraints:
        mvatype="varcascaded",
    conda:
        "../envs/massfit.yml"
    notebook:
        "../notebooks/massfit_forfomcheck_data_plot.py.ipynb"
