rule cut_KSmass:
    input:
        expand_partitions(
            f"data/derived/datasets/remove_multiplecandidates/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/cut_KSmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermva),
        ),
    log:
        f"logs/cut_KSmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_KSmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_KSmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        criteria=lambda wildcards: (
            "((B_ConsD0PV_KS0_M > 497.6 - 10.89) & (B_ConsD0PV_KS0_M < 497.6 + 10.89))"
            if wildcards["KStype"] == "LL"
            else "((B_ConsD0PV_KS0_M > 497.6 - 18.44) & (B_ConsD0PV_KS0_M < 497.6 + 18.44))"
        ),
    threads: min(config["threads"], npartitions_aftermva)
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/filter.py.ipynb"
