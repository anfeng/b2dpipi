def get_yaml_data(input, input_keyname, output_keyname):
    import yaml

    with open(input[input_keyname]) as fin:
        results = yaml.load(fin, yaml.FullLoader)
    return results[output_keyname]


def get_pickle_data(input, input_keyname, output_keyname):
    import pickle

    with open(input[input_keyname], "rb") as fin:
        results = pickle.load(fin)
    return results[output_keyname]


def expand_partitions(filepath, index_name, indices):
    return [filepath.replace(f"{{{index_name}}}", str(index)) for index in indices]
