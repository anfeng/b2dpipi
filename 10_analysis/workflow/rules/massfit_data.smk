rule massfit_data:
    input:
        model_filepath="workflow/scripts/massfit_model_2.py",
        data_filepaths=lambda wildcards: [
            filepath.format(
                datatype="data",
                channel="KSpipi",
                KStype=wildcards["KStype"],
                run=wildcards["run"],
            )
            for filepath in rules.combine_datasets.output
        ],
        massfit_mc_fitresult_filepath=f"data/output/massfit_mc/fitresult/{paramspace_massfit.wildcard_pattern}.root",
    output:
        fitresult_filepath=f"data/output/massfit_data/fitresult/{paramspace_massfit.wildcard_pattern}.root",
    log:
        f"logs/massfit_data/{paramspace_massfit.wildcard_pattern}.log",
        notebook=f"logs/notebooks/massfit_data/{paramspace_massfit.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_data/{paramspace_massfit.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/massfit.yml"
    notebook:
        "../notebooks/massfit_data.py.ipynb"
