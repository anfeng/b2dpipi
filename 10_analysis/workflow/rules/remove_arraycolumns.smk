rule remove_arraycolumns:
    input:
        f"data/derived/datasets/withnewcolumns/{paramspace.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/arraycolumnsremoved/{paramspace.wildcard_pattern}.root",
    log:
        f"logs/remove_arraycolumns/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/remove_arraycolumns/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/remove_arraycolumns/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="DecayTree",
        output_tree_name="DecayTree",
        dtfvar_namepattern="B_Cons",
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            1000 * threads if wildcards["eventtype"] == "90000000" else 100 * threads
        ),
    conda:
        "../envs/remove_arraycolumns.yml"
    notebook:
        "../notebooks/remove_arraycolumns.py.ipynb"
