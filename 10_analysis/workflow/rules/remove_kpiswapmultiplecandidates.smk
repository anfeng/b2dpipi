rule remove_kpiswapmultiplecandidates:
    input:
        expand_partitions(
            f"data/derived/datasets/to_parquet/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/remove_kpiswapmultiplecandidates/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    log:
        f"logs/remove_kpiswapmultiplecandidates/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/remove_kpiswapmultiplecandidates/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/remove_kpiswapmultiplecandidates/{paramspace.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/remove_kpiswapmultiplecandidates.py.ipynb"
