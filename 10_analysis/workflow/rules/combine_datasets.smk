rule combine_datasets:
    input:
        data_filepaths=lambda wildcards: expand(
            f"data/derived/datasets/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            mvatype="varcascaded",
            eventtype=90000000 if wildcards["datatype"] == "data" else 11166117,
            KStype=wildcards["KStype"],
            year=[2011, 2012] if wildcards["run"] == "1" else [2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            dz_significance_DB=0.5,
            KSD_FDCHI2_ORIVX=0,
            D_FDCHI2_ORIVX=0,
            ipartition=range(npartitions_aftermva),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/combine_datasets/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_combined),
        ),
    log:
        f"logs/combine_datasets/{paramspace_afterselection.wildcard_pattern}.log",
        notebook=f"logs/notebooks/combine_datasets/{paramspace_afterselection.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/combine_datasets/{paramspace_afterselection.wildcard_pattern}.benchmark.txt"
    threads: min(config["threads"], npartitions_combined)
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["datatype"] == "data" else 1000
        )
        * threads,
    conda:
        "../envs/combine_datasets.yml"
    notebook:
        "../notebooks/combine_datasets.py.ipynb"
