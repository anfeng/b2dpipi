def get_criteria(wildcards):
    if wildcards.year in ["2011", "2012"]:
        criteria = " & ".join(
            [
                "(B_L0HadronDecision_TOS | B_L0Global_TIS)",
                "(B_Hlt1TrackAllL0Decision_TOS)",
                "(B_Hlt2Topo2BodyBBDTDecision_TOS | B_Hlt2Topo3BodyBBDTDecision_TOS | B_Hlt2Topo4BodyBBDTDecision_TOS)",
            ]
        )
    elif wildcards.year in ["2015", "2016", "2017", "2018"]:
        criteria = " & ".join(
            [
                "(B_L0HadronDecision_TOS | B_L0Global_TIS)",
                "(B_Hlt1TrackMVADecision_TOS | B_Hlt1TwoTrackMVADecision_TOS)",
                "(B_Hlt2Topo2BodyDecision_TOS | B_Hlt2Topo3BodyDecision_TOS | B_Hlt2Topo4BodyDecision_TOS)",
            ]
        )
    return criteria


rule cut_trigger:
    input:
        expand_partitions(
            f"data/derived/datasets/cut_DTFconverged/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    output:
        expand_partitions(
            f"data/derived/datasets/cut_trigger/{paramspace.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    log:
        f"logs/cut_trigger/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_trigger/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_trigger/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        criteria=get_criteria,
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/filter.py.ipynb"
