rule dtrfit_plot:
    input:
        fitresult_filepath=rules.dtrfit.output["fitresult_filepath"],
    output:
        hist_ddt=f"figures/dtrfit/hist_ddt/{paramspace_afterselection.wildcard_pattern}.pdf",
    log:
        f"logs/dtrfit_plot/{paramspace_afterselection.wildcard_pattern}.log",
        notebook=f"logs/notebooks/dtrfit_plot/{paramspace_afterselection.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/dtrfit_plot/{paramspace_afterselection.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        datatype="mc",
    conda:
        "../envs/dtrfit.yml"
    notebook:
        "../notebooks/dtrfit_plot.py.ipynb"
