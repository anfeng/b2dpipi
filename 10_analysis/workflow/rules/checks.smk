import numpy as np


# -------- check whether FoM calculation is consistent with TMVA --------
rule prepare_fom_signal_samples_forcheck:
    input:
        "data/output/TMVA_cascaded_B/result/KStype~LL/run~2.root",
    output:
        "data/derived/datasets/fomsignalsamples_forcheck.root",
    benchmark:
        "benchmarks/prepare_fom_signal_samples_forcheck.benchmark.txt"
    params:
        input_tree_name="TMVAdataset/TestTree",
        output_tree_name="DecayTree",
        criteria="(classID == 0)",
    threads: config["threads"]
    wrapper:
        "v3.10.2/phys/root/filter"


rule prepare_fom_background_samples_forcheck:
    input:
        "data/output/TMVA_cascaded_B/result/KStype~LL/run~2.root",
    output:
        "data/derived/datasets/fombackgroundsamples_forcheck.root",
    benchmark:
        "benchmarks/prepare_fom_background_samples_forcheck.benchmark.txt"
    params:
        input_tree_name="TMVAdataset/TestTree",
        output_tree_name="DecayTree",
        criteria="(classID == 1)",
    threads: config["threads"]
    wrapper:
        "v3.10.2/phys/root/filter"


rule check_fom_old:
    input:
        signal_filepaths="data/derived/datasets/fomsignalsamples_forcheck.root",
        background_filepaths="data/derived/datasets/fombackgroundsamples_forcheck.root",
        eventnum_filepath="data/output/massfit_forfom_data/eventnum/KStype~LL/run~2.yml",
    output:
        fom_filepath=f"data/output/fom_forcheck.pkl",
    log:
        f"logs/check_fom_old.log",
    benchmark:
        f"benchmarks/check_fom_old.benchmark.txt"
    params:
        signal_tree_name="DecayTree",
        background_tree_name="DecayTree",
        nsignal=lambda wildcards, input: get_yaml_data(
            input, "eventnum_filepath", "pdf_B_B2Dpipi"
        ),
        nbackground=lambda wildcards, input: get_yaml_data(
            input, "eventnum_filepath", "pdf_B_fakeB"
        ),
        cut_points={"BDTG": np.linspace(0.8, 1, 100, endpoint=False)},
    threads: config["threads"]
    wrapper:
        "https://github.com/laf070810/data-analysis-helper-snakemake/raw/v0.4.0/wrapper/fom_optimization/wrapper.py"


# Omitting input to avoid lengthy notebook preamble causing vscode to crash
# Needs full_selection_target for inputs
rule calculate_efficiencies:
    output:
        "data/output/efficiencies.parquet",
    log:
        f"logs/calculate_efficiencies.log",
        notebook=f"logs/notebooks/calculate_efficiencies.py.ipynb",
    benchmark:
        f"benchmarks/calculate_efficiencies.benchmark.txt"
    threads: 50
    resources:
        mem_mib=lambda wildcards, threads: 300 * threads,
    conda:
        "../envs/analysis.yml"
    notebook:
        "../notebooks/calculate_efficiencies.py.ipynb"


# Omitting input to avoid lengthy notebook preamble causing vscode to crash
# Needs full_selection_target for inputs
rule check_multiplecandidates:
    output:
        "data/output/check_multiplecandidates.parquet",
    log:
        f"logs/check_multiplecandidates.log",
        notebook=f"logs/notebooks/check_multiplecandidates.py.ipynb",
    benchmark:
        f"benchmarks/check_multiplecandidates.benchmark.txt"
    threads: 50
    resources:
        mem_mib=lambda wildcards, threads: 300 * threads,
    conda:
        "../envs/analysis.yml"
    notebook:
        "../notebooks/check_multiplecandidates.py.ipynb"


rule check_fom:
    input:
        expand(
            f"data/output/massfit_forfomcheck_data/eventnum/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.yml",
            mvatype=["varcascaded"],
            KStype=["LL"],
            run=["2"],
            dz_significance_DB=["0.5"],
            KSD_FDCHI2_ORIVX=["0"],
            D_FDCHI2_ORIVX=["0"],
            mvacutpoint=[
                round(point, 8) for point in np.linspace(0, 1, 50, endpoint=False)
            ],
        ),
    output:
        "figures/check_fom.png",
    log:
        "logs/check_fom.log",
        notebook="logs/notebooks/check_fom.py.ipynb",
    benchmark:
        f"benchmarks/check_fom.benchmark.txt"
    threads: config["threads"] // 5
    conda:
        "../envs/analysis.yml"
    notebook:
        "../notebooks/check_fom.py.ipynb"


rule check_flavor_tagging:
    input:
        # KSpipi_KSLL_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSLL_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="LL", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
        # KSpipi_KSDD_run1_filepaths=[
        #     filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=1)
        #     for filepath in rules.bin_dalitz_plots.output
        # ],
        KSpipi_KSDD_run2_filepaths=[
            filepath.format(datatype="data", channel="KSpipi", KStype="DD", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
        hh_run12_filepaths=[
            filepath.format(datatype="data", channel="Kpi", KStype="NA", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
        # fitresult_filepath=rules.fit_beta.output.fitresult_filepath,
        KSpipi_mc_KSLL_run2_filepaths=[
            filepath.format(datatype="mc", channel="KSpipi", KStype="LL", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
        KSpipi_mc_KSDD_run2_filepaths=[
            filepath.format(datatype="mc", channel="KSpipi", KStype="DD", run=2)
            for filepath in rules.bin_dalitz_plots.output["data_filepaths"]
        ],
    output:
        "data/output/checks/check_flavor_tagging.parquet",
    log:
        f"logs/check_flavor_tagging.log",
        notebook=f"logs/notebooks/check_flavor_tagging.py.ipynb",
    benchmark:
        f"benchmarks/check_flavor_tagging.benchmark.txt"
    threads: 50
    resources:
        mem_mib=lambda wildcards, threads: 300 * threads,
    conda:
        "../envs/analysis.yml"
    notebook:
        "../notebooks/check_flavor_tagging.py.ipynb"
