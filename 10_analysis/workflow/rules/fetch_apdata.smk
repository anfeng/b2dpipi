# Parameters for the datasets to be analyzed
config = {
    "wg": "b2oc",
    "analysis": "b02d0pipi",
    "eventtype": ["90000000", "11166117"],
    "datatype": ["2011", "2012", "2015", "2016", "2017", "2018"],
    "polarity": ["magdown", "magup"],
    "output_filepath_prefix": "data/input",
}


module fetch_apdata:
    snakefile:
        github(
            "laf070810/data-analysis-helper-snakemake",
            path="workflow/Snakefile",
            tag="v0.0.3",
        )
    config:
        config


use rule * from fetch_apdata
