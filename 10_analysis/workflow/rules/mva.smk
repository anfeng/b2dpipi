def get_input_files_mva(wildcards):
    # translates paramspace_mva to paramspace
    if (
        (wildcards.mva == "independent_KS")
        or (wildcards.mva == "independent_D")
        or (wildcards.mva == "independent_B")
        or (wildcards.mva == "cutcascaded_KS")
        or (wildcards.mva == "varcascaded_KS")
        or (wildcards.mva == "varcascaded_D")
        or (wildcards.mva == "varcascaded_B")
        or (wildcards.mva == "combined")
    ):
        filedir = f"withindex/{paramspace.wildcard_pattern}"
    elif wildcards.mva == "cutcascaded_D":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_KS"
        )
    elif wildcards.mva == "cutcascaded_B":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_D"
        )
    else:
        raise Exception(f"unknown mva {wildcards.mva}")

    return {
        "signal_filepaths": expand(
            f"data/derived/datasets/{filedir}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            eventtype=[11166117],
            KStype=[wildcards.KStype],
            year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            ipartition=range(npartitions),
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        ),
        "background_filepaths": expand(
            f"data/derived/datasets/{filedir}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            eventtype=[90000000],
            KStype=[wildcards.KStype],
            year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            ipartition=range(npartitions),
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        ),
    }


def get_additional_input_files_mva(wildcards):
    if wildcards.mva == "varcascaded_D":
        return {
            "signal_mvavarcascadedKSresponses_filepaths": expand(
                f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
                mva=["varcascaded_KS"],
                eventtype=[11166117],
                KStype=[wildcards.KStype],
                year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
                polarity=["magdown", "magup"],
                **{
                    key: val
                    for key, val in wildcards.items()
                    if key in rectangular_cuts.keys()
                },
            ),
            "background_mvavarcascadedKSresponses_filepaths": expand(
                f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
                mva=["varcascaded_KS"],
                eventtype=[90000000],
                KStype=[wildcards.KStype],
                year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
                polarity=["magdown", "magup"],
                **{
                    key: val
                    for key, val in wildcards.items()
                    if key in rectangular_cuts.keys()
                },
            ),
        }
    elif wildcards.mva == "varcascaded_B":
        return {
            "signal_mvavarcascadedKSresponses_filepaths": expand(
                f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
                mva=["varcascaded_KS"],
                eventtype=[11166117],
                KStype=[wildcards.KStype],
                year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
                polarity=["magdown", "magup"],
                **{
                    key: val
                    for key, val in wildcards.items()
                    if key in rectangular_cuts.keys()
                },
            ),
            "signal_mvavarcascadedDresponses_filepaths": expand(
                f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
                mva=["varcascaded_D"],
                eventtype=[11166117],
                KStype=[wildcards.KStype],
                year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
                polarity=["magdown", "magup"],
                **{
                    key: val
                    for key, val in wildcards.items()
                    if key in rectangular_cuts.keys()
                },
            ),
            "background_mvavarcascadedKSresponses_filepaths": expand(
                f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
                mva=["varcascaded_KS"],
                eventtype=[90000000],
                KStype=[wildcards.KStype],
                year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
                polarity=["magdown", "magup"],
                **{
                    key: val
                    for key, val in wildcards.items()
                    if key in rectangular_cuts.keys()
                },
            ),
            "background_mvavarcascadedDresponses_filepaths": expand(
                f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
                mva=["varcascaded_D"],
                eventtype=[90000000],
                KStype=[wildcards.KStype],
                year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
                polarity=["magdown", "magup"],
                **{
                    key: val
                    for key, val in wildcards.items()
                    if key in rectangular_cuts.keys()
                },
            ),
        }
    else:
        return {}


def get_input_files_mva_application(wildcards):
    input_filepaths = {}

    # translates paramspace_mvaresponse to paramspace_mva
    input_filepaths["model_filepath"] = (
        f"data/output/mva/cvmodels/{paramspace_mva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pkl".format(
            mva=wildcards.mva,
            KStype=wildcards.KStype,
            run=1 if wildcards.year in ["2011", "2012"] else 2,
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        )
    )

    # translates paramspace_mvaresponse to paramspace
    if (
        (wildcards.mva == "independent_KS")
        or (wildcards.mva == "independent_D")
        or (wildcards.mva == "independent_B")
        or (wildcards.mva == "cutcascaded_KS")
        or (wildcards.mva == "varcascaded_KS")
        or (wildcards.mva == "varcascaded_D")
        or (wildcards.mva == "varcascaded_B")
        or (wildcards.mva == "combined")
    ):
        filedir = f"withindex/{paramspace.wildcard_pattern}"
    elif wildcards.mva == "cutcascaded_D":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_KS"
        )
    elif wildcards.mva == "cutcascaded_B":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_D"
        )
    else:
        raise Exception(f"unknown mva {wildcards.mva}")

    input_filepaths["input_filepaths"] = expand(
        f"data/derived/datasets/{filedir}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
        ipartition=range(npartitions),
        **{
            key: val
            for key, val in wildcards.items()
            if (key in dataframe.columns) or (key in dataframe_rectcut.columns)
        },
    )

    return input_filepaths


def get_additional_input_files_mva_application(wildcards):
    if wildcards.mva == "varcascaded_D":
        return {
            "input_mvavarcascadedKSresponses_filepaths": f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet".format(
                mva="varcascaded_KS",
                **{key: val for key, val in wildcards.items() if key != "mva"},
            )
        }
    elif wildcards.mva == "varcascaded_B":
        return {
            "input_mvavarcascadedKSresponses_filepaths": f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet".format(
                mva="varcascaded_KS",
                **{key: val for key, val in wildcards.items() if key != "mva"},
            ),
            "input_mvavarcascadedDresponses_filepaths": f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet".format(
                mva="varcascaded_D",
                **{key: val for key, val in wildcards.items() if key != "mva"},
            ),
        }
    else:
        return {}


def get_training_variables(wildcards):
    training_variables_KS = [
        # -------- KS variables --------
        "KSD_ENDVERTEX_CHI2NDOF",
        "D_ENDVERTEX_CHI2NDOF",
        "log10_KSD_PT",
        "log10_KSD_IPCHI2_OWNPV",
        "log10_KSD_FDCHI2_OWNPV",
        "log10_KSD_FDCHI2_ORIVX",
        "KSD_ConsPV_decayLength_significance",
        "KSD_ETA",
        "KSD_DIRA_ORIVX",
        # -------- KS children variables --------
        "log10_sum_IPCHI2_of_KSD_children",
        "log10_max_IPCHI2_of_KSD_children",
        "log10_sum_IP_of_KSD_children",
        "log10_max_IP_of_KSD_children",
        "log10_sum_PT_of_KSD_children",
        "pipKS_ProbNNp_corr",
        "pipKS_ProbNNk_corr",
        "pipKS_ProbNNpi_corr",
        "pimKS_ProbNNp_corr",
        "pimKS_ProbNNk_corr",
        "pimKS_ProbNNpi_corr",
    ]
    training_variables_D = [
        # -------- D variables --------
        "D_ENDVERTEX_CHI2NDOF",
        "B_ENDVERTEX_CHI2NDOF",
        "log10_D_PT",
        "log10_D_IPCHI2_OWNPV",
        "log10_D_FDCHI2_OWNPV",
        "log10_D_FDCHI2_ORIVX",
        "D_ConsPV_decayLength_significance",
        "D_ETA",
        "D_DIRA_ORIVX",
        "dz_significance_DB",
        # -------- D children variables --------
        "log10_sum_IPCHI2_of_D_children",
        "log10_max_IPCHI2_of_D_children",
        "log10_sum_IP_of_D_children",
        "log10_max_IP_of_D_children",
        "log10_sum_PT_of_D_children",
        "pipD_ProbNNp_corr",
        "pipD_ProbNNk_corr",
        "pipD_ProbNNpi_corr",
        "pimD_ProbNNp_corr",
        "pimD_ProbNNk_corr",
        "pimD_ProbNNpi_corr",
    ]
    training_variables_B = [
        # -------- B variables --------
        "B_ENDVERTEX_CHI2NDOF",
        "log10_B_PT",
        "log10_B_IPCHI2_OWNPV",
        "log10_B_FDCHI2_OWNPV",
        "B_ConsPV_decayLength_significance",
        "B_ETA",
        "B_DIRA_OWNPV",
        # -------- B children variables --------
        "log10_sum_IPCHI2_of_B_children",
        "log10_max_IPCHI2_of_B_children",
        "log10_sum_IP_of_B_children",
        "log10_max_IP_of_B_children",
        "log10_sum_PT_of_B_children",
        "KKst_ProbNNp_corr",
        "KKst_ProbNNk_corr",
        "KKst_ProbNNpi_corr",
        "piKst_ProbNNp_corr",
        "piKst_ProbNNk_corr",
        "piKst_ProbNNpi_corr",
    ]
    if (
        (wildcards.mva == "independent_KS")
        or (wildcards.mva == "cutcascaded_KS")
        or (wildcards.mva == "varcascaded_KS")
    ):
        return training_variables_KS
    elif (wildcards.mva == "independent_D") or (wildcards.mva == "cutcascaded_D"):
        return training_variables_D
    elif wildcards.mva == "varcascaded_D":
        return training_variables_D + ["MVA_KS_response"]
    elif (wildcards.mva == "independent_B") or (wildcards.mva == "cutcascaded_B"):
        return training_variables_B
    elif wildcards.mva == "varcascaded_B":
        return training_variables_B + ["MVA_KS_response", "MVA_D_response"]
    elif wildcards.mva == "combined":
        return list(
            dict.fromkeys(
                training_variables_KS + training_variables_D + training_variables_B
            )
        )
    else:
        raise Exception(f"unknown mva {wildcards.mva}")


def get_training_variables_original(wildcards):
    training_variables_original = list(get_training_variables(wildcards))
    if wildcards.mva == "varcascaded_D":
        training_variables_original.remove("MVA_KS_response")
    elif wildcards.mva == "varcascaded_B":
        training_variables_original.remove("MVA_KS_response")
        training_variables_original.remove("MVA_D_response")
    return training_variables_original


def get_spectator_variables(wildcards):
    if (
        (wildcards.mva == "independent_KS")
        or (wildcards.mva == "cutcascaded_KS")
        or (wildcards.mva == "varcascaded_KS")
    ):
        return ["B_ConsPV_KS0_M"]
    elif (
        (wildcards.mva == "independent_D")
        or (wildcards.mva == "cutcascaded_D")
        or (wildcards.mva == "varcascaded_D")
    ):
        return ["B_ConsPV_D0_M"]
    elif (
        (wildcards.mva == "independent_B")
        or (wildcards.mva == "cutcascaded_B")
        or (wildcards.mva == "varcascaded_B")
    ):
        return ["B_ConsPV_M"]
    elif wildcards.mva == "combined":
        return ["B_ConsPV_KS0_M", "B_ConsPV_D0_M", "B_ConsPV_M"]
    else:
        raise Exception(f"unknown mva {wildcards.mva}")


def get_background_cut(wildcards):
    if (
        (wildcards.mva == "independent_KS")
        or (wildcards.mva == "cutcascaded_KS")
        or (wildcards.mva == "varcascaded_KS")
    ):
        if wildcards.KStype == "LL":
            return "(((B_ConsPV_KS0_M > 510) & (B_ConsPV_KS0_M < 520)) | ((B_ConsPV_KS0_M > 473) & (B_ConsPV_KS0_M < 483)))"
        elif wildcards.KStype == "DD":
            return "(((B_ConsPV_KS0_M > 520) & (B_ConsPV_KS0_M < 530)) | ((B_ConsPV_KS0_M > 465) & (B_ConsPV_KS0_M < 475)))"
        else:
            raise Exception(f"unknown KStype {wildcards.KStype}")
    elif (
        (wildcards.mva == "independent_D")
        or (wildcards.mva == "cutcascaded_D")
        or (wildcards.mva == "varcascaded_D")
    ):
        # return "(((B_ConsPV_D0_M > 1910) & (B_ConsPV_D0_M < 1950)) | ((B_ConsPV_D0_M > 1780) & (B_ConsPV_D0_M < 1820)))"
        return "(B_ConsPV_D0_M > 1940) & (B_ConsPV_D0_M < 1950)"
    elif (
        (wildcards.mva == "independent_B")
        or (wildcards.mva == "cutcascaded_B")
        or (wildcards.mva == "varcascaded_B")
    ):
        return "(B_ConsPV_M > 5600) & (B_ConsPV_M < 5800)"
    elif wildcards.mva == "combined":
        # if wildcards.KStype == "LL":
        #     return "(B_ConsPV_M > 5600) & (B_ConsPV_M < 5800) & (((B_ConsPV_D0_M > 1910) & (B_ConsPV_D0_M < 1950)) | ((B_ConsPV_D0_M > 1780) & (B_ConsPV_D0_M < 1820))) & (((B_ConsPV_KS0_M > 510) & (B_ConsPV_KS0_M < 520)) | ((B_ConsPV_KS0_M > 473) & (B_ConsPV_KS0_M < 483)))"
        # elif wildcards.KStype == "DD":
        #     return "(B_ConsPV_M > 5600) & (B_ConsPV_M < 5800) & (((B_ConsPV_D0_M > 1910) & (B_ConsPV_D0_M < 1950)) | ((B_ConsPV_D0_M > 1780) & (B_ConsPV_D0_M < 1820))) & (((B_ConsPV_KS0_M > 520) & (B_ConsPV_KS0_M < 530)) | ((B_ConsPV_KS0_M > 465) & (B_ConsPV_KS0_M < 475)))"
        # else:
        #     raise Exception(f"unknown KStype {wildcards.KStype}")
        if wildcards.KStype == "LL":
            return "(B_ConsPV_M > 5600) & (B_ConsPV_M < 5800) & ((B_ConsPV_D0_M > 1940) & (B_ConsPV_D0_M < 1950)) & (((B_ConsPV_KS0_M > 510) & (B_ConsPV_KS0_M < 520)) | ((B_ConsPV_KS0_M > 473) & (B_ConsPV_KS0_M < 483)))"
        elif wildcards.KStype == "DD":
            return "(B_ConsPV_M > 5600) & (B_ConsPV_M < 5800) & ((B_ConsPV_D0_M > 1940) & (B_ConsPV_D0_M < 1950)) & (((B_ConsPV_KS0_M > 520) & (B_ConsPV_KS0_M < 530)) | ((B_ConsPV_KS0_M > 465) & (B_ConsPV_KS0_M < 475)))"
        else:
            raise Exception(f"unknown KStype {wildcards.KStype}")
    else:
        raise Exception(f"unknown mva {wildcards.mva}")


def get_mva_response_name(wildcards):
    if (
        (wildcards.mva == "independent_KS")
        or (wildcards.mva == "cutcascaded_KS")
        or (wildcards.mva == "varcascaded_KS")
    ):
        return "MVA_KS_response"
    elif (
        (wildcards.mva == "independent_D")
        or (wildcards.mva == "cutcascaded_D")
        or (wildcards.mva == "varcascaded_D")
    ):
        return "MVA_D_response"
    elif (
        (wildcards.mva == "independent_B")
        or (wildcards.mva == "cutcascaded_B")
        or (wildcards.mva == "varcascaded_B")
    ):
        return "MVA_B_response"
    elif wildcards.mva == "combined":
        return "MVA_response"
    else:
        raise Exception(f"unknown mva {wildcards.mva}")


rule mva:
    input:
        unpack(get_input_files_mva),
        unpack(get_additional_input_files_mva),
    output:
        model_filepath=f"data/output/mva/model/{paramspace_mva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.ubj",
        cvmodels_filepath=f"data/output/mva/cvmodels/{paramspace_mva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pkl",
        data_filepath=f"data/output/mva/data/{paramspace_mva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
    log:
        f"logs/mva/{paramspace_mva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/mva/{paramspace_mva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/mva/{paramspace_mva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        background_cut=get_background_cut,
        training_variables=get_training_variables,
        spectator_variables=get_spectator_variables,
        training_variables_original=get_training_variables_original,
        signal_additional_filepaths_key=lambda wildcards: [
            key
            for key in get_additional_input_files_mva(wildcards).keys()
            if "signal" in key
        ],
        background_additional_filepaths_key=lambda wildcards: [
            key
            for key in get_additional_input_files_mva(wildcards).keys()
            if "background" in key
        ],
    threads: config["threads"]
    conda:
        "../envs/mva.yml"
    notebook:
        "../notebooks/mva.py.ipynb"


rule mva_application:
    input:
        unpack(get_input_files_mva_application),
        unpack(get_additional_input_files_mva_application),
    output:
        output_filepaths=f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
    log:
        f"logs/mva_application/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/mva_application/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/mva_application/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        training_variables=get_training_variables,
        spectator_variables=get_spectator_variables,
        training_variables_original=get_training_variables_original,
        input_additional_filepaths_key=lambda wildcards: list(
            get_additional_input_files_mva_application(wildcards).keys()
        ),
        mva_response_name=get_mva_response_name,
        use_cvmodels=True,
    threads: config["threads"]
    conda:
        "../envs/mva.yml"
    notebook:
        "../notebooks/mva_application.py.ipynb"
