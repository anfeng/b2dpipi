import os


def get_input_files_ftcomb_convert_to_root_file(wildcards):
    if wildcards["channel"] == "KSpipi":
        return [
            filepath.format(**wildcards)
            for filepath in (
                rules.massfit_data_sweight.output
                if wildcards["datatype"] == "data"
                else rules.combine_datasets.output
            )
        ]
    elif wildcards["channel"] in ["Kpi", "KK", "pipi"]:
        if wildcards["KStype"] != "NA":
            raise Exception(f"Unsupported KStype {wildcards['KStype']} for channel Kpi")
        return rules.B2Dpipi_D2hh_convert_dataformat.output[0].format(
            datatype=wildcards["datatype"],
            channel=wildcards["channel"],
            run=wildcards["run"],
            ipartition=wildcards["ipartition"],
        )
    else:
        raise Exception(f"Unknown channel {wildcards['channel']}")


epm_config = lambda wildcards: {
    # OS taggers:
    "OS_Kaon_Use": 1,
    "OS_Muon_Use": 1,
    "OS_Electron_Use": 1,
    "OS_Charm_Use": 1,
    "VtxCharge_Use": 1,
    "OS_Kaon_BranchDec": '"B_OSKaonLatest_TAGDEC"',
    "OS_Kaon_BranchProb": '"B_OSKaonLatest_TAGETA"',
    "OS_Muon_BranchDec": '"B_OSMuonLatest_TAGDEC"',
    "OS_Muon_BranchProb": '"B_OSMuonLatest_TAGETA"',
    "OS_Electron_BranchDec": '"B_OSElectronLatest_TAGDEC"',
    "OS_Electron_BranchProb": '"B_OSElectronLatest_TAGETA"',
    "OS_Charm_BranchDec": '"B_OSCharm_TAGDEC"',
    "OS_Charm_BranchProb": '"B_OSCharm_TAGETA"',
    "VtxCharge_BranchDec": '"B_OSVtxCh_TAGDEC"',
    "VtxCharge_BranchProb": '"B_OSVtxCh_TAGETA"',
    # SS taggers:
    "SS_Kaon_Use": 1,
    "SS_Pion_Use": 1,
    "SS_Proton_Use": 1,
    "SS_Kaon_BranchDec": '"B_SSKaonLatest_TAGDEC"',
    "SS_Kaon_BranchProb": '"B_SSKaonLatest_TAGETA"',
    "SS_Pion_BranchDec": '"B_SSPion_TAGDEC"',
    "SS_Pion_BranchProb": '"B_SSPion_TAGETA"',
    "SS_Proton_BranchDec": '"B_SSProton_TAGDEC"',
    "SS_Proton_BranchProb": '"B_SSProton_TAGETA"',
    # OS combination settings:
    "OS_Kaon_InOSComb": 1,
    "OS_Muon_InOSComb": 1,
    "OS_Electron_InOSComb": 1,
    "OS_Charm_InOSComb": 1,
    "VtxCharge_InOSComb": 1,
    "PerformOfflineCombination_OS": 1,
    "OS_Combination_Write": 1,
    # SS combination settings:
    "SS_Kaon_InSSComb": 0,
    "SS_Pion_InSSComb": 1,
    "SS_Proton_InSSComb": 1,
    "PerformOfflineCombination_SS": 1,
    "SS_Combination_Write": 1,
    # OS+SS combination settings:
    "OS_Kaon_InComb": 1,
    "OS_Muon_InComb": 1,
    "OS_Electron_InComb": 1,
    "OS_Charm_InComb": 1,
    "VtxCharge_InComb": 1,
    "SS_Kaon_InComb": 0,
    "SS_Pion_InComb": 1,
    "SS_Proton_InComb": 1,
    "PerformOfflineCombination_OSplusSS": 1,
    "Combination_Write": 1,
    # other options:
    "TupleName": '"mytree"',
    "CalibratedOutputFile": '"output.root"',
    "CalibrationMode": '"Bd"',
    "WriteCalibratedMistagBranches": 1,
}


rule compile_epm:
    output:
        epm_dirpath=directory("epm"),
    log:
        f"logs/compile_epm.log",
    benchmark:
        f"benchmarks/compile_epm.benchmark.txt"
    threads: config["threads"]
    container:
        None
    shell:
        "("
        "git clone https://gitlab.cern.ch/lhcb-ft/EspressoPerformanceMonitor.git {output.epm_dirpath}"
        " && cd {output.epm_dirpath}"
        " && git checkout v0.81"
        " && git submodule update --init --recursive"
        " && apptainer exec --bind $(pwd -P) --bind /cvmfs:/cvmfs:ro /cvmfs/lhcb.cern.ch/containers/os-base/centos7-devel/prod/amd64 bash -c 'source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh && cmake -B build'"
        " && apptainer exec --bind $(pwd -P) --bind /cvmfs:/cvmfs:ro /cvmfs/lhcb.cern.ch/containers/os-base/centos7-devel/prod/amd64 bash -c 'source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh && cmake --build build -j {threads}'"
        ") | tee {log[0]}"


rule ftcomb_convert_to_root_file:
    input:
        get_input_files_ftcomb_convert_to_root_file,
    output:
        f"data/derived/datasets/ftcomb/input/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.root",
    log:
        f"logs/ftcomb_convert_to_root_file/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
        notebook=f"logs/notebooks/ftcomb_convert_to_root_file/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/ftcomb_convert_to_root_file/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.benchmark.txt"
    params:
        output_tree_name="mytree",
    threads: config["threads"]
    conda:
        "../envs/ftcomb.yml"
    notebook:
        "../notebooks/convert_parquet_to_root_by_uproot.py.ipynb"


rule ftcomb:
    input:
        epm_dirpath=rules.compile_epm.output["epm_dirpath"],
        data_filepath=rules.ftcomb_convert_to_root_file.output[0],
    output:
        ftcomb_dirpath=directory(
            f"data/derived/datasets/ftcomb/output/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}/"
        ),
        ftcomb_outputfilepath=f"data/derived/datasets/ftcomb/output/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}/output.root",
    log:
        f"logs/ftcomb/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/ftcomb/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.benchmark.txt"
    params:
        epm_config=epm_config,
    threads: config["threads"]
    run:
        import os
        import subprocess
        import re

        os.makedirs(output["ftcomb_dirpath"], exist_ok=True)
        config_filepath = os.path.join(output["ftcomb_dirpath"], "__temp_epm_config.py")
        with open(config_filepath, "w") as f:
            f.write(
                "\n".join(
                    [f"{key} = {val}" for key, val in params.epm_config.items()]
                    + [f'RootFile = "{os.path.abspath(input.data_filepath)}"']
                )
            )

        subprocess.run(
            f"apptainer exec --bind $(pwd -P) --bind /cvmfs:/cvmfs:ro /cvmfs/lhcb.cern.ch/containers/os-base/centos7-devel/prod/amd64 bash -c 'source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh && basedir=$(pwd -P) && cd {output.ftcomb_dirpath} && ${{basedir}}/{input.epm_dirpath}/build/bin/SimpleEvaluator ${{basedir}}/{config_filepath}' | tee {log[0]}",
            shell=True,
        )


rule ftcomb_convert_to_parquet_file:
    input:
        input_filepath=rules.ftcomb_convert_to_root_file.output,
        ftcomb_filepath=rules.ftcomb.output["ftcomb_outputfilepath"],
    output:
        f"data/derived/datasets/ftcombed/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
    log:
        f"logs/ftcomb_convert_to_parquet_file/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.log",
        notebook=f"logs/notebooks/ftcomb_convert_to_parquet_file/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/ftcomb_convert_to_parquet_file/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="mytree",
        ftcomb_tree_name="TaggingTree",
    threads: config["threads"]
    conda:
        "../envs/ftcomb.yml"
    notebook:
        "../notebooks/ftcomb_convert_to_parquet_file.py.ipynb"
