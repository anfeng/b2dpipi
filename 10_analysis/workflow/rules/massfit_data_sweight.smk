rule massfit_data_sweight:
    input:
        fitresult_filepath=rules.massfit_data.output.fitresult_filepath,
        data_filepaths=lambda wildcards: [
            filepath.format(**wildcards) for filepath in rules.combine_datasets.output
        ],
    output:
        expand_partitions(
            f"data/derived/datasets/withsweight/{paramspace_afterselection.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions_aftermassfit),
        ),
    log:
        f"logs/massfit_data_sweight/{paramspace_afterselection.wildcard_pattern}.log",
        notebook=f"logs/notebooks/massfit_data_sweight/{paramspace_afterselection.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_data_sweight/{paramspace_afterselection.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        datatype="data",
        channel="KSpipi",
    conda:
        "../envs/massfit.yml"
    notebook:
        "../notebooks/massfit_data_sweight.py.ipynb"
