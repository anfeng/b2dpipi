rule massfit_fortoy_data:
    input:
        model_filepath="workflow/scripts/massfit_model.py",
        data_filepaths=lambda wildcards: expand(
            f"data/derived/datasets/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            mvatype=[wildcards.mvatype],
            eventtype=[90000000],
            KStype=[wildcards.KStype],
            year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            ipartition=range(npartitions_aftermva),
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        ),
        massfit_fortoy_mc_fitresult_filepath=f"data/output/massfit_fortoy_mc/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    output:
        fitresult_filepath=f"data/output/massfit_fortoy_data/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    log:
        f"logs/massfit_fortoy_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/massfit_fortoy_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_fortoy_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/analysis.yml"
    notebook:
        "../notebooks/massfit_fortoy_data.py.ipynb"
