def get_input(wildcards):
    import yaml

    input_filepaths = {}

    # -------- input data file --------
    # translates paramspace_aftermva to paramspace
    if (
        (wildcards.mvatype == "independent")
        or (wildcards.mvatype == "varcascaded")
        or (wildcards.mvatype == "combined")
        or (wildcards.mvatype == "cutcascaded_KS")
    ):
        filedir = f"withindex/{paramspace.wildcard_pattern}"
    elif wildcards.mvatype == "cutcascaded_D":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_KS"
        )
    elif wildcards.mvatype == "cutcascaded_B":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_D"
        )
    else:
        raise Exception(f"unknown mvatype {wildcards.mvatype}")

    input_filepaths["input_filepaths"] = expand(
        f"data/derived/datasets/{filedir}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
        ipartition=range(npartitions),
        **wildcards,
    )

    # -------- MVA response file --------
    # translates paramspace_aftermva to paramspace_mvaresponse
    if wildcards.mvatype == "independent":
        mva_names = [
            "independent_KS",
            "independent_D",
            "independent_B",
        ]
    elif wildcards.mvatype == "varcascaded":
        mva_names = ["varcascaded_B"]
    else:
        mva_names = [wildcards.mvatype]

    for mva_name in mva_names:
        input_filepaths[f"input_additional_{mva_name}_filepaths"] = expand(
            f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
            mva=[mva_name],
            **wildcards,
        )

    # -------- FoM file for getting the best MVA cut --------
    # translates paramspace_aftermva to paramspace_fom
    input_filepaths["fom_filepath"] = (
        f"data/output/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pkl".format(
            mvatype=wildcards.mvatype,
            KStype=wildcards.KStype,
            run="1" if wildcards.year in ["2011", "2012"] else "2",
            **{
                key: val
                for key, val in wildcards.items()
                if key in dataframe_rectcut.columns
            },
        )
    )

    return input_filepaths


rule cut_mva:
    input:
        unpack(get_input),
    output:
        expand_partitions(
            f"data/derived/datasets/cut_mva/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            "ipartition",
            range(npartitions),
        ),
    log:
        f"logs/cut_mva/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/cut_mva/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/cut_mva/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        criteria=lambda wildcards, input: " & ".join(
            [
                f"({variable_name} > {cut_point})"
                for variable_name, cut_point in get_pickle_data(
                    input, "fom_filepath", "cut_points_best"
                ).items()
            ]
        ),
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards, threads: (
            2500 if wildcards["eventtype"] == "90000000" else 1000
        )
        * threads,
    conda:
        "../envs/filter.yml"
    notebook:
        "../notebooks/cut_mva.py.ipynb"
