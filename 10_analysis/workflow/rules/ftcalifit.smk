# rule reweight_for_ftcalifit:
#     input:
#         original_filepaths=expand(
#             rules.B2Dpipi_D2hh_convert_dataformat.output,
#             channel=["dataKpi"],
#             run=["12"],
#             ipartition=[0],
#         ),
#         target_filepaths=lambda wildcards: expand(
#             f"data/derived/datasets/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
#             mvatype=[wildcards.mvatype],
#             eventtype=[90000000],
#             KStype=[wildcards.KStype],
#             year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
#             polarity=["magdown", "magup"],
#             ipartition=range(npartitions_aftermva),
#             **{
#                 key: val
#                 for key, val in wildcards.items()
#                 if key in rectangular_cuts.keys()
#             },
#         ),
#     output:
#         f"data/derived/datasets/reweight_for_ftcalifit/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
#     log:
#         f"logs/reweight_for_ftcalifit/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
#         notebook=f"logs/notebooks/reweight_for_ftcalifit/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
#     benchmark:
#         f"benchmarks/reweight_for_ftcalifit/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
#     threads: config["threads"]
#     conda:
#         "../envs/reweight_for_ftcalifit.yml"
#     notebook:
#         "../notebooks/reweight_for_ftcalifit.py.ipynb"


rule ftcalifit:
    input:
        model_filepath="workflow/scripts/ftcalifit_model.py",
        data_Kpi_filepaths=lambda wildcards: [
            rules.ftcomb_convert_to_parquet_file.output[0].format(
                datatype="data",
                channel="Kpi",
                KStype="NA",
                run=wildcards["run"],
                ipartition=ipartition,
            )
            for ipartition in range(B2Dpipi_D2hh_workflow.npartitions)
        ],
    output:
        fitresult_filepath=f"data/output/ftcalifit/fitresult/{paramspace_ftcalifit.wildcard_pattern}.root",
        binning_filepath=f"data/output/ftcalifit/binning/{paramspace_ftcalifit.wildcard_pattern}.yml",
    log:
        f"logs/ftcalifit/{paramspace_ftcalifit.wildcard_pattern}.log",
        notebook=f"logs/notebooks/ftcalifit/{paramspace_ftcalifit.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/ftcalifit/{paramspace_ftcalifit.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/ftcalifit.yml"
    notebook:
        "../notebooks/ftcalifit.py.ipynb"
