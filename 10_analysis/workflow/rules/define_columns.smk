from data_analysis_helper.expr import get_invariant_mass_expression


branches = [
    # -------- B MVA variables --------
    [
        "B_ENDVERTEX_CHI2NDOF",
        "B_ENDVERTEX_CHI2 / B_ENDVERTEX_NDOF",
    ],
    ["log10_B_PT", "log10(B_PT)"],
    ["log10_B_IPCHI2_OWNPV", "log10(B_IPCHI2_OWNPV)"],
    ["log10_B_FDCHI2_OWNPV", "log10(B_FDCHI2_OWNPV)"],
    [
        "B_ConsPV_decayLength_significance",
        "B_ConsPV_decayLength[0] / B_ConsPV_decayLengthErr[0]",
    ],
    [
        "B_ETA",
        "ROOT::Math::PxPyPzEVector(B_PX, B_PY, B_PZ, B_PE).Eta()",
    ],
    [
        "log10_sum_IPCHI2_of_B_children",
        "log10(D_IPCHI2_OWNPV + KSD_IPCHI2_OWNPV + pipKS_IPCHI2_OWNPV + pimKS_IPCHI2_OWNPV + pipD_IPCHI2_OWNPV + pimD_IPCHI2_OWNPV + KKst_IPCHI2_OWNPV + piKst_IPCHI2_OWNPV)",
    ],
    [
        "log10_max_IPCHI2_of_B_children",
        "log10(max({D_IPCHI2_OWNPV, KSD_IPCHI2_OWNPV, pipKS_IPCHI2_OWNPV, pimKS_IPCHI2_OWNPV, pipD_IPCHI2_OWNPV, pimD_IPCHI2_OWNPV, KKst_IPCHI2_OWNPV, piKst_IPCHI2_OWNPV}))",
    ],
    [
        "log10_sum_IP_of_B_children",
        "log10(D_IP_OWNPV + KSD_IP_OWNPV + pipKS_IP_OWNPV + pimKS_IP_OWNPV + pipD_IP_OWNPV + pimD_IP_OWNPV + KKst_IP_OWNPV + piKst_IP_OWNPV)",
    ],
    [
        "log10_max_IP_of_B_children",
        "log10(max({D_IP_OWNPV, KSD_IP_OWNPV, pipKS_IP_OWNPV, pimKS_IP_OWNPV, pipD_IP_OWNPV, pimD_IP_OWNPV, KKst_IP_OWNPV, piKst_IP_OWNPV}))",
    ],
    [
        "log10_sum_PT_of_B_children",
        "log10(D_PT + KSD_PT + pipKS_PT + pimKS_PT + pipD_PT + pimD_PT + KKst_PT + piKst_PT)",
    ],
    # -------- D MVA variables --------
    [
        "D_ENDVERTEX_CHI2NDOF",
        "D_ENDVERTEX_CHI2 / D_ENDVERTEX_NDOF",
    ],
    ["log10_D_PT", "log10(D_PT)"],
    ["log10_D_IPCHI2_OWNPV", "log10(D_IPCHI2_OWNPV)"],
    ["log10_D_FDCHI2_OWNPV", "log10(D_FDCHI2_OWNPV)"],
    ["log10_D_FDCHI2_ORIVX", "log10(D_FDCHI2_ORIVX)"],
    [
        "D_ConsPV_decayLength_significance",
        "B_ConsPV_D0_decayLength[0] / B_ConsPV_D0_decayLengthErr[0]",
    ],
    [
        "D_ETA",
        "ROOT::Math::PxPyPzEVector(D_PX, D_PY, D_PZ, D_PE).Eta()",
    ],
    [
        "dz_significance_DB",
        "(D_ENDVERTEX_Z - B_ENDVERTEX_Z) / sqrt(D_ENDVERTEX_ZERR * D_ENDVERTEX_ZERR + B_ENDVERTEX_ZERR * B_ENDVERTEX_ZERR)",
    ],
    [
        "log10_sum_IPCHI2_of_D_children",
        "log10(KSD_IPCHI2_OWNPV + pipKS_IPCHI2_OWNPV + pimKS_IPCHI2_OWNPV + pipD_IPCHI2_OWNPV + pimD_IPCHI2_OWNPV)",
    ],
    [
        "log10_max_IPCHI2_of_D_children",
        "log10(max({KSD_IPCHI2_OWNPV, pipKS_IPCHI2_OWNPV, pimKS_IPCHI2_OWNPV, pipD_IPCHI2_OWNPV, pimD_IPCHI2_OWNPV}))",
    ],
    [
        "log10_sum_IP_of_D_children",
        "log10(KSD_IP_OWNPV + pipKS_IP_OWNPV + pimKS_IP_OWNPV + pipD_IP_OWNPV + pimD_IP_OWNPV)",
    ],
    [
        "log10_max_IP_of_D_children",
        "log10(max({KSD_IP_OWNPV, pipKS_IP_OWNPV, pimKS_IP_OWNPV, pipD_IP_OWNPV, pimD_IP_OWNPV}))",
    ],
    [
        "log10_sum_PT_of_D_children",
        "log10(KSD_PT + pipKS_PT + pimKS_PT + pipD_PT + pimD_PT)",
    ],
    [
        "KSD_ENDVERTEX_CHI2NDOF",
        "KSD_ENDVERTEX_CHI2 / KSD_ENDVERTEX_NDOF",
    ],
    # -------- KS MVA variables --------
    ["log10_KSD_PT", "log10(KSD_PT)"],
    ["log10_KSD_IPCHI2_OWNPV", "log10(KSD_IPCHI2_OWNPV)"],
    ["log10_KSD_FDCHI2_OWNPV", "log10(KSD_FDCHI2_OWNPV)"],
    ["log10_KSD_FDCHI2_ORIVX", "log10(KSD_FDCHI2_ORIVX)"],
    [
        "KSD_ConsPV_decayLength_significance",
        "B_ConsPV_KS0_decayLength[0] / B_ConsPV_KS0_decayLengthErr[0]",
    ],
    [
        "KSD_ETA",
        "ROOT::Math::PxPyPzEVector(KSD_PX, KSD_PY, KSD_PZ, KSD_PE).Eta()",
    ],
    [
        "log10_sum_IPCHI2_of_KSD_children",
        "log10(pipKS_IPCHI2_OWNPV + pimKS_IPCHI2_OWNPV)",
    ],
    [
        "log10_max_IPCHI2_of_KSD_children",
        "log10(max({pipKS_IPCHI2_OWNPV, pimKS_IPCHI2_OWNPV}))",
    ],
    [
        "log10_sum_IP_of_KSD_children",
        "log10(pipKS_IP_OWNPV + pimKS_IP_OWNPV)",
    ],
    [
        "log10_max_IP_of_KSD_children",
        "log10(max({pipKS_IP_OWNPV, pimKS_IP_OWNPV}))",
    ],
    [
        "log10_sum_PT_of_KSD_children",
        "log10(pipKS_PT + pimKS_PT)",
    ],
    # -------- veto variables --------
    [
        "B_ConsPV_D0pi1_M",
        get_invariant_mass_expression(
            [
                "B_ConsPV_D0_KS0_piplus",
                "B_ConsPV_D0_KS0_piplus_0",
                "B_ConsPV_D0_piplus",
                "B_ConsPV_D0_piplus_0",
                "B_ConsPV_rho_770_0_piplus",
            ]
        ),
    ],
    [
        "B_ConsPV_D0pi2_M",
        get_invariant_mass_expression(
            [
                "B_ConsPV_D0_KS0_piplus",
                "B_ConsPV_D0_KS0_piplus_0",
                "B_ConsPV_D0_piplus",
                "B_ConsPV_D0_piplus_0",
                "B_ConsPV_rho_770_0_piplus_0",
            ]
        ),
    ],
    [
        "B_ConsD0PV_D0pi1_M",
        get_invariant_mass_expression(
            [
                "B_ConsD0PV_D0_KS0_piplus",
                "B_ConsD0PV_D0_KS0_piplus_0",
                "B_ConsD0PV_D0_piplus",
                "B_ConsD0PV_D0_piplus_0",
                "B_ConsD0PV_rho_770_0_piplus",
            ]
        ),
    ],
    [
        "B_ConsD0PV_D0pi2_M",
        get_invariant_mass_expression(
            [
                "B_ConsD0PV_D0_KS0_piplus",
                "B_ConsD0PV_D0_KS0_piplus_0",
                "B_ConsD0PV_D0_piplus",
                "B_ConsD0PV_D0_piplus_0",
                "B_ConsD0PV_rho_770_0_piplus_0",
            ]
        ),
    ],
    # -------- other variables --------
    ["MVA_fold", f"gRandom->Rndm()"],
]


rule define_columns:
    input:
        f"data/derived/datasets/withpidcorr/{paramspace.wildcard_pattern}.root",
    output:
        f"data/derived/datasets/withnewcolumns/{paramspace.wildcard_pattern}.root",
    log:
        f"logs/define_columns/{paramspace.wildcard_pattern}.log",
        notebook=f"logs/notebooks/define_columns/{paramspace.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/define_columns/{paramspace.wildcard_pattern}.benchmark.txt"
    params:
        input_tree_name="DecayTree",
        output_tree_name="DecayTree",
        branches=branches,
    threads: config["threads"]
    resources:
        mem_mib=lambda wildcards: (
            50000 if wildcards["eventtype"] == "90000000" else 2000
        ),
    conda:
        "../envs/define_columns_root.yml"
    notebook:
        "../notebooks/define_columns_root.py.ipynb"
