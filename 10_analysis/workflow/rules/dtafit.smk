rule dtafit:
    input:
        model_filepath="workflow/scripts/dtafit_model.py",
        data_filepaths=lambda wildcards: expand(
            f"data/derived/datasets/cut_Dmass/{paramspace_aftermva.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
            mvatype=[wildcards.mvatype],
            eventtype=[90000000],
            KStype=[wildcards.KStype],
            year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            ipartition=range(npartitions_aftermva),
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        ),
    output:
        fitresult_filepath=f"data/output/dtafit/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    log:
        f"logs/dtafit/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/dtafit/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/dtafit/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    threads: config["threads"]
    conda:
        "../envs/dtafit.yml"
    notebook:
        "../notebooks/dtafit.py.ipynb"
