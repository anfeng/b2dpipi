def get_input(wildcards):
    input_filepaths = {}

    input_filepaths["model_filepath"] = "workflow/scripts/massfit_model.py"

    # translates paramspace_fom to paramspace
    if (
        (wildcards.mvatype == "independent")
        or (wildcards.mvatype == "varcascaded")
        or (wildcards.mvatype == "combined")
        or (wildcards.mvatype == "cutcascaded_KS")
    ):
        filedir = f"withindex/{paramspace.wildcard_pattern}"
    elif wildcards.mvatype == "cutcascaded_D":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_KS"
        )
    elif wildcards.mvatype == "cutcascaded_B":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_D"
        )
    else:
        raise Exception(f"unknown mvatype {wildcards.mvatype}")

    input_filepaths["data_filepaths"] = expand(
        f"data/derived/datasets/{filedir}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
        eventtype=[90000000],
        KStype=[wildcards.KStype],
        year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
        polarity=["magdown", "magup"],
        ipartition=range(npartitions),
        **{
            key: val for key, val in wildcards.items() if key in rectangular_cuts.keys()
        },
    )

    # translates paramspace_fom to paramspace_mvaresponse
    if wildcards.mvatype == "independent":
        mva_names = [
            "independent_KS",
            "independent_D",
            "independent_B",
        ]
    elif wildcards.mvatype == "varcascaded":
        mva_names = ["varcascaded_B"]
    else:
        mva_names = [wildcards.mvatype]

    for mva_name in mva_names:
        input_filepaths[f"data_additional_{mva_name}_filepaths"] = expand(
            f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
            mva=[mva_name],
            eventtype=[90000000],
            KStype=[wildcards.KStype],
            year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
            polarity=["magdown", "magup"],
            ipartition=range(npartitions),
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        )

    return input_filepaths


rule massfit_forfomcheck_data:
    input:
        unpack(get_input),
        massfit_forfom_mc_fitresult_filepath=f"data/output/massfit_forfom_mc/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    output:
        fitresult_filepath=f"data/output/massfit_forfomcheck_data/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.root",
    log:
        f"logs/massfit_forfomcheck_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.root.log",
        notebook=f"logs/notebooks/massfit_forfomcheck_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.root.py.ipynb",
    benchmark:
        f"benchmarks/massfit_forfomcheck_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}/mvacutpoint~{{mvacutpoint}}.root.benchmark.txt"
    wildcard_constraints:
        mvatype="varcascaded",
    threads: config["threads"] // 5
    resources:
        mem_mib=lambda wildcards, threads: 2000 * threads,
    conda:
        "../envs/massfit.yml"
    notebook:
        "../notebooks/massfit_forfomcheck_data.py.ipynb"
