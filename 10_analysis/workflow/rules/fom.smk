import numpy as np


def get_input(wildcards):
    input_filepaths = {}

    # -------- real data file for signal region cut --------
    # translates paramspace_fom to paramspace
    if (
        (wildcards.mvatype == "independent")
        or (wildcards.mvatype == "varcascaded")
        or (wildcards.mvatype == "combined")
        or (wildcards.mvatype == "cutcascaded_KS")
    ):
        filedir = f"withindex/{paramspace.wildcard_pattern}"
    elif wildcards.mvatype == "cutcascaded_D":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_KS"
        )
    elif wildcards.mvatype == "cutcascaded_B":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_D"
        )
    else:
        raise Exception(f"unknown mvatype {wildcards.mvatype}")

    input_filepaths["data_filepaths"] = expand(
        f"data/derived/datasets/{filedir}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
        eventtype=[90000000],
        KStype=[wildcards.KStype],
        year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
        polarity=["magdown", "magup"],
        ipartition=range(npartitions),
        **{
            key: val for key, val in wildcards.items() if key in rectangular_cuts.keys()
        },
    )

    # -------- MVA response files --------
    # translates paramspace_fom to paramspace_mvaresponse
    if wildcards.run == "1":
        year = [2011, 2012]
    elif wildcards.run == "2":
        year = [2015, 2016, 2017, 2018]

    if wildcards.mvatype == "independent":
        mva_names = [
            "independent_KS",
            "independent_D",
            "independent_B",
        ]
    elif wildcards.mvatype == "varcascaded":
        mva_names = ["varcascaded_B"]
    else:
        mva_names = [wildcards.mvatype]

    for mva_name in mva_names:
        input_filepaths[f"mc_mvaresponse_{mva_name}_filepaths"] = expand(
            f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
            mva=[mva_name],
            eventtype=[11166117],
            KStype=[wildcards.KStype],
            year=year,
            polarity=["magdown", "magup"],
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        )
        input_filepaths[f"data_mvaresponse_{mva_name}_filepaths"] = expand(
            f"data/derived/datasets/mva_responses/{paramspace_mvaresponse.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.parquet",
            mva=[mva_name],
            eventtype=[90000000],
            KStype=[wildcards.KStype],
            year=year,
            polarity=["magdown", "magup"],
            **{
                key: val
                for key, val in wildcards.items()
                if key in rectangular_cuts.keys()
            },
        )

    return input_filepaths


def get_cut_points(wildcards):
    if wildcards.mvatype == "independent":
        if (wildcards.KStype == "DD") and (wildcards.run == "1"):
            return {
                "MVA_KS_response": np.linspace(0, 0.4, 20, endpoint=False),
                "MVA_D_response": np.linspace(0.3, 0.7, 20, endpoint=False),
                "MVA_B_response": np.linspace(0.6, 1, 20, endpoint=False),
            }
        else:
            return {
                "MVA_KS_response": np.linspace(0, 0.03, 20, endpoint=False),
                "MVA_D_response": np.linspace(0, 0.2, 20, endpoint=False),
                "MVA_B_response": np.linspace(0.2, 0.9, 20, endpoint=False),
            }
    elif wildcards.mvatype == "varcascaded":
        if wildcards.run == "1":
            return {"MVA_B_response": np.linspace(0.6, 1, 1000, endpoint=False)}
        else:
            return {"MVA_B_response": np.linspace(0.6, 0.8, 1000, endpoint=False)}
    elif wildcards.mvatype == "combined":
        if (wildcards.KStype == "LL") and (wildcards.run == "2"):
            return {"MVA_response": np.linspace(0.99, 1, 1000, endpoint=False)}
        else:
            return {"MVA_response": np.linspace(0.999, 1, 1000, endpoint=False)}
    elif wildcards.mvatype == "cutcascaded_KS":
        if (wildcards.KStype == "DD") and (wildcards.run == "1"):
            return {"MVA_KS_response": np.linspace(0.5, 0.9, 1000, endpoint=False)}
        else:
            return {"MVA_KS_response": np.linspace(0.1, 0.5, 1000, endpoint=False)}
    elif wildcards.mvatype == "cutcascaded_D":
        return {"MVA_D_response": np.linspace(0.5, 0.98, 1000, endpoint=False)}
    elif wildcards.mvatype == "cutcascaded_B":
        return {"MVA_B_response": np.linspace(0.6, 0.98, 1000, endpoint=False)}


rule fom:
    input:
        unpack(get_input),
        eventnum_filepath=f"data/output/massfit_forfom_data/eventnum/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.yml",
    output:
        fom_filepath=f"data/output/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pkl",
    log:
        f"logs/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    params:
        data_cut=lambda wildcards: (
            "(B_ConsPV_M > 5600) & (B_ConsPV_M < 5800) & (B_ConsKSPV_D0_M > 1864.84 - 25) & (B_ConsKSPV_D0_M < 1864.84 + 25) & (B_ConsD0PV_KS0_M > 497.6 - 10.89) & (B_ConsD0PV_KS0_M < 497.6 + 10.89)"
            if wildcards["KStype"] == "LL"
            else "(B_ConsPV_M > 5600) & (B_ConsPV_M < 5800) & (B_ConsKSPV_D0_M > 1864.84 - 25) & (B_ConsKSPV_D0_M < 1864.84 + 25) & (B_ConsD0PV_KS0_M > 497.6 - 18.44) & (B_ConsD0PV_KS0_M < 497.6 + 18.44)"
        ),
        data_cut_variables=["B_ConsPV_M", "B_ConsKSPV_D0_M", "B_ConsD0PV_KS0_M"],
        fom_type="S/sqrt(S+B)",
        nsignal=lambda wildcards, input: get_yaml_data(
            input, "eventnum_filepath", "pdf_B_B2Dpipi"
        ),
        nbackground=lambda wildcards, input: get_yaml_data(
            input, "eventnum_filepath", "pdf_B_fakeB"
        ),
        cut_points=get_cut_points,
    threads: config["threads"] // 5
    conda:
        "../envs/fom.yml"
    notebook:
        "../notebooks/fom.py.ipynb"


# def get_cut_points(wildcards):
#     if wildcards.mvatype == "independent":
#         if (wildcards.KStype == "DD") and (wildcards.run == "1"):
#             return {
#                 "MVA_KS_response": np.linspace(0.8, 1, 20, endpoint=False),
#                 "MVA_D_response": np.linspace(0.6, 1, 20, endpoint=False),
#                 "MVA_B_response": np.linspace(0.8, 1, 20, endpoint=False),
#             }
#         else:
#             return {
#                 "MVA_KS_response": np.linspace(0, 0.5, 20, endpoint=False),
#                 "MVA_D_response": np.linspace(0.1, 0.7, 20, endpoint=False),
#                 "MVA_B_response": np.linspace(0.4, 1, 20, endpoint=False),
#             }
#     elif wildcards.mvatype == "varcascaded":
#         return {"MVA_B_response": np.linspace(0.8, 1, 1000, endpoint=False)}
#     elif wildcards.mvatype == "combined":
#         if (wildcards.KStype == "LL") and (wildcards.run == "2"):
#             return {"MVA_response": np.linspace(0.999, 1, 1000, endpoint=False)}
#         else:
#             return {"MVA_response": np.linspace(0.9999, 1, 1000, endpoint=False)}
#     elif wildcards.mvatype == "cutcascaded_KS":
#         if wildcards.run == "1":
#             return {"MVA_KS_response": np.linspace(0.45, 0.98, 1000, endpoint=False)}
#         else:
#             return {"MVA_KS_response": np.linspace(0.3, 0.7, 1000, endpoint=False)}
#     elif wildcards.mvatype == "cutcascaded_D":
#         return {"MVA_D_response": np.linspace(0.5, 0.98, 1000, endpoint=False)}
#     elif wildcards.mvatype == "cutcascaded_B":
#         return {"MVA_B_response": np.linspace(0.5, 0.98, 1000, endpoint=False)}


# rule fom_special:
#     input:
#         unpack(get_input),
#     output:
#         fom_filepath=f"data/output/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pkl",
#     log:
#         f"logs/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
#         notebook=f"logs/notebooks/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
#     benchmark:
#         f"benchmarks/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
#     params:
#         data_cut="(B_ConsPV_M > 5250) & (B_ConsPV_M < 5310) & (B_ConsKSPV_D0_M > 1864.84 - 25) & (B_ConsKSPV_D0_M < 1864.84 + 25) & (B_ConsD0PV_KS0_M > 497.6 - 10.89) & (B_ConsD0PV_KS0_M < 497.6 + 10.89)",
#         data_cut_variables=["B_ConsPV_M", "B_ConsKSPV_D0_M", "B_ConsD0PV_KS0_M"],
#         fom_type="S'/sqrt(S+B)",
#         cut_points=get_cut_points,
#     threads: config["threads"] // 5
#     conda:
#         "../envs/fom.yml"
#     notebook:
#         "../notebooks/fom.py.ipynb"


rule fom_plot:
    input:
        fom_filepaths=expand(
            f"data/output/fom/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.pkl",
            KStype=["LL", "DD"],
            run=[1, 2],
            mvatype=[
                "independent",
                "varcascaded",
                "combined",
                "cutcascaded_KS",
                "cutcascaded_D",
                "cutcascaded_B",
            ],
            **{key: val["current"] for key, val in rectangular_cuts.items()},
        ),
    output:
        figure_fom=f"figures/fom_plot.png",
        best_rectcut=f"data/output/fom_plot/best_rectcut.yml",
    log:
        f"logs/fom_plot.log",
        notebook=f"logs/notebooks/fom_plot.py.ipynb",
    benchmark:
        f"benchmarks/fom_plot.benchmark.txt"
    params:
        rectangular_cuts=rectangular_cuts,
    threads: config["threads"]
    conda:
        "../envs/analysis.yml"
    notebook:
        "../notebooks/fom_plot.py.ipynb"
