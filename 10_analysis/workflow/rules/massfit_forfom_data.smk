def get_input(wildcards):
    input_filepaths = {}

    input_filepaths["model_filepath"] = "workflow/scripts/massfit_model.py"

    # translates paramspace_fom to paramspace
    if (
        (wildcards.mvatype == "independent")
        or (wildcards.mvatype == "varcascaded")
        or (wildcards.mvatype == "combined")
        or (wildcards.mvatype == "cutcascaded_KS")
    ):
        filedir = f"withindex/{paramspace.wildcard_pattern}"
    elif wildcards.mvatype == "cutcascaded_D":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_KS"
        )
    elif wildcards.mvatype == "cutcascaded_B":
        filedir = f"cut_mva/{paramspace_aftermva.wildcard_pattern}".replace(
            "{mvatype}", "cutcascaded_D"
        )
    else:
        raise Exception(f"unknown mvatype {wildcards.mvatype}")

    input_filepaths["data_filepaths"] = expand(
        f"data/derived/datasets/{filedir}/{paramspace_rectcut.wildcard_pattern}/{paramspace_partition.wildcard_pattern}.parquet",
        eventtype=[90000000],
        KStype=[wildcards.KStype],
        year=[2011, 2012] if wildcards.run == "1" else [2015, 2016, 2017, 2018],
        polarity=["magdown", "magup"],
        ipartition=range(npartitions),
        **{
            key: val for key, val in wildcards.items() if key in rectangular_cuts.keys()
        },
    )

    return input_filepaths


rule massfit_forfom_data:
    input:
        unpack(get_input),
        massfit_forfom_mc_fitresult_filepath=f"data/output/massfit_forfom_mc/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    output:
        fitresult_filepath=f"data/output/massfit_forfom_data/fitresult/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.root",
    log:
        f"logs/massfit_forfom_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.log",
        notebook=f"logs/notebooks/massfit_forfom_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.py.ipynb",
    benchmark:
        f"benchmarks/massfit_forfom_data/{paramspace_fom.wildcard_pattern}/{paramspace_rectcut.wildcard_pattern}.benchmark.txt"
    threads: 1  # Setting this > 1 may cause the job to hang forever, possibly due to some bug of RooFit's multiprocessed fitting
    resources:
        mem_mib=lambda wildcards, threads: 4000 * threads,
    conda:
        "../envs/massfit.yml"
    notebook:
        "../notebooks/massfit_forfom_data.py.ipynb"
