import numpy as np
import ROOT
import yaml

# physical Dbin index: -N, -N + 1, ..., -1, 1, ..., N - 1, N
# physical Bbin index: -M, -M + 1, ..., -1, 1, ..., M - 1, M
# fully-mapped programmatic Dbin index: 0, 1, ..., 2 * N - 1 corresponding to -N, -N + 1, ..., -1, 1, ..., N - 1, N
# fully-mapped programmatic Bbin index: 0, 1, ..., 2 * M - 1 corresponding to -M, -M + 1, ..., -1, 1, ..., M - 1, M
# half-mapped programmatic Dbin index: 0, 1, ..., N - 1 corresponding to 1, ..., N - 1, N
# half-mapped programmatic Bbin index: 0, 1, ..., M - 1 corresponding to 1, ..., M - 1, M


def get_indexed_variable(variable_name, index_expression, upper_limit, index=0):
    # returns an expression yielding {variable_name}{index_expression}
    # , where variable_name is a variable and index_expression, whose value ranges from 0 (included) to upper_limit (excluded), is an expression of some variables
    # , e.g. get_indexed_variable('K', 'i + 1', 10) yields K1 if the value of i + 1 is 1
    if index < upper_limit:
        return f"((({index_expression}) == {index}) ? ({variable_name}{index}) : ({get_indexed_variable(variable_name, index_expression, upper_limit, index + 1)}))"
    else:
        return 0


def get_physically_indexed_variable(variable_name, index_expression, half_limit):
    if variable_name == "K" or variable_name == "k":
        # convert physical index to fully-mapped programmatic index:
        return get_indexed_variable(
            variable_name,
            f"((({index_expression}) < 0) ? (({index_expression}) + {half_limit}) : (({index_expression}) + {half_limit} - 1))",
            2 * half_limit,
        )
    elif variable_name == "C" or variable_name == "c":
        # convert physical index to half-mapped programmatic index:
        return get_indexed_variable(
            variable_name,
            f"((({index_expression}) < 0) ? (-({index_expression}) - 1) : (({index_expression}) - 1))",
            half_limit,
        )
    elif variable_name == "S" or variable_name == "s":
        # convert physical index to half-mapped programmatic index:
        return f'((({index_expression}) < 0) ? (-{get_indexed_variable(variable_name, f"-({index_expression}) - 1", half_limit)}) : ({get_indexed_variable(variable_name, f"({index_expression}) - 1", half_limit)}))'


def get_workspace_beta(
    M,
    N,
    *,
    workspace=None,
    suffix_channel="",
    suffix_KStype="",
    suffix_run="",
    **kwargs,
):
    # N=0 is for no-D-Dalitz-plot case, e.g. D -> hh

    suffix_dataset = suffix_KStype + suffix_run
    suffix = suffix_channel + suffix_KStype + suffix_run

    if workspace is None:
        workspace = ROOT.RooWorkspace("w", "workspace")

    # Generic B-decay with user coefficients
    # -------------------------

    # Construct pdf
    # -------------------------

    # using physical bin index:
    dict_of_Dbin_index = {}
    dict_of_Bbin_index = {}
    for i in list(range(-N, 0)) + list(range(1, N + 1)):
        dict_of_Dbin_index[str(i)] = i
    for j in list(range(-M, 0)) + list(range(1, M + 1)):
        dict_of_Bbin_index[str(j)] = j

    # -------- part of observables --------
    dt = ROOT.RooRealVar("dt", "dt", 0.3, 14)
    # dt.setBins(50)
    if kwargs["ftcali"] == "uncalibrated":
        eta_OS = ROOT.RooRealVar("eta_OS", "eta_OS", 0, 0.5)
        eta_SS = ROOT.RooRealVar("eta_SS", "eta_SS", 0, 0.5)
        tagdec_OS = ROOT.RooCategory(
            "tagdec_OS", "tagdec_OS", {"B0": 1, "B0bar": -1, "untagged": 0}
        )
        tagdec_SS = ROOT.RooCategory(
            "tagdec_SS", "tagdec_SS", {"B0": 1, "B0bar": -1, "untagged": 0}
        )
    elif kwargs["ftcali"] == "calibrated":
        omega = ROOT.RooRealVar("omega", "omega", 0, 0.5)
        omegabar = ROOT.RooRealVar("omegabar", "omegabar", 0, 0.5)
        tagdec = ROOT.RooCategory(
            "tagdec", "tagdec", {"B0": 1, "B0bar": -1, "untagged": 0}
        )

    index_Dbin = ROOT.RooCategory("i", "index_Dbin", dict_of_Dbin_index)
    index_Bbin = ROOT.RooCategory("j", "index_Bbin", dict_of_Bbin_index)

    all_vars = [dt, index_Dbin, index_Bbin]

    # -------- shared parameters --------
    dm = ROOT.RooRealVar("dm", "delta m(B0)", 0.5074)
    tau = ROOT.RooRealVar("tau", "tau (B0)", 1.517)
    dgamma = ROOT.RooFit.RooConst(0)

    # using fully-mapped programmatic Dbin index:
    K_var = [ROOT.RooRealVar(f"K{i}", f"K[{i}]", 0, 1) for i in range(2 * N)]
    # using half-mapped programmatic Dbin index:
    C_var = [ROOT.RooRealVar(f"C{i}", f"C[{i}]", -2, 2) for i in range(N)]
    # using half-mapped programmatic Dbin index:
    S_var = [ROOT.RooRealVar(f"S{i}", f"S[{i}]", -2, 2) for i in range(N)]
    # using fully-mapped programmatic Bbin index:
    k_var = [ROOT.RooRealVar(f"k{i}", f"k[{i}]", 0, 1) for i in range(2 * M)]
    # using half-mapped programmatic Bbin index:
    c_var = [ROOT.RooRealVar(f"c{i}", f"c[{i}]", -2, 2) for i in range(M)]
    # using half-mapped programmatic Bbin index:
    s_var = [ROOT.RooRealVar(f"s{i}", f"s[{i}]", -2, 2) for i in range(M)]
    beta_var = ROOT.RooRealVar("beta", "beta", 0, np.pi / 4)

    all_vars += (
        [dm, tau, dgamma, beta_var] + K_var + C_var + S_var + k_var + c_var + s_var
    )

    # -------- combined tagging decision and mistag rate --------
    if (
        kwargs["ftcali"] == "uncalibrated"
    ):  # WARNING: this case has not been updated to the correct version
        eta_OS = ROOT.RooRealVar("eta_OS", "eta_OS", 0, 0.5)
        eta_SS = ROOT.RooRealVar("eta_SS", "eta_SS", 0, 0.5)
        tagdec_OS = ROOT.RooCategory(
            "tagdec_OS", "tagdec_OS", {"B0": 1, "B0bar": -1, "untagged": 0}
        )
        tagdec_SS = ROOT.RooCategory(
            "tagdec_SS", "tagdec_SS", {"B0": 1, "B0bar": -1, "untagged": 0}
        )
        workspace.Import(eta_OS)
        workspace.Import(eta_SS)
        all_vars += [eta_OS, eta_SS, tagdec_OS, tagdec_SS]

        for tagger in ["OS", "SS"]:
            suffix_tagger = f"_{tagger}"

            eta_mean = ROOT.RooRealVar(
                f"eta_mean{suffix_tagger}{suffix_run}",
                f"eta_mean{suffix_tagger}{suffix_run}",
                0.25,
            )  # to be set later, based on data
            p0 = ROOT.RooRealVar(
                f"p0{suffix_tagger}{suffix_run}",
                f"p0{suffix_tagger}{suffix_run}",
                0.4,
                0,
                0.5,
            )
            p1 = ROOT.RooRealVar(
                f"p1{suffix_tagger}{suffix_run}",
                f"p1{suffix_tagger}{suffix_run}",
                1,
                0.5,
                1.2,
            )
            dp0 = ROOT.RooRealVar(
                f"dp0{suffix_tagger}{suffix_run}",
                f"dp0{suffix_tagger}{suffix_run}",
                0,
                -0.2,
                0.2,
            )
            dp1 = ROOT.RooRealVar(
                f"dp1{suffix_tagger}{suffix_run}",
                f"dp1{suffix_tagger}{suffix_run}",
                0,
                -0.2,
                0.2,
            )

            all_vars += [eta_mean, p0, p1, dp0, dp1]

            omega = ROOT.RooFormulaVar(
                f"omega{suffix_tagger}{suffix}",
                f"omega{suffix_tagger}{suffix}",
                f"p0{suffix_tagger}{suffix_run} + dp0{suffix_tagger}{suffix_run} + "
                f"(p1{suffix_tagger}{suffix_run} + dp1{suffix_tagger}{suffix_run}) * (eta{suffix_tagger} - eta_mean{suffix_tagger}{suffix_run})",
                [workspace.var(f"eta{suffix_tagger}"), p0, p1, dp0, dp1, eta_mean],
            )
            omegabar = ROOT.RooFormulaVar(
                f"omegabar{suffix_tagger}{suffix}",
                f"omegabar{suffix_tagger}{suffix}",
                f"p0{suffix_tagger}{suffix_run} - dp0{suffix_tagger}{suffix_run} + "
                f"(p1{suffix_tagger}{suffix_run} - dp1{suffix_tagger}{suffix_run}) * (eta{suffix_tagger} - eta_mean{suffix_tagger}{suffix_run})",
                [workspace.var(f"eta{suffix_tagger}"), p0, p1, dp0, dp1, eta_mean],
            )

            workspace.Import(omega)
            workspace.Import(omegabar)

        tagdec = ROOT.RooFormulaVar(
            f"tagdec{suffix}",
            f"tagdec{suffix}",
            f"((tagdec_OS == -tagdec_SS) && (omega_OS{suffix} > omega_SS{suffix})) ? tagdec_SS : tagdec_OS",
            [
                tagdec_OS,
                tagdec_SS,
                workspace.function(f"omega_OS{suffix}"),
                workspace.function(f"omega_SS{suffix}"),
            ],
        )
        omega = ROOT.RooFormulaVar(
            f"omega{suffix}",
            f"omega{suffix}",
            f"(tagdec_OS == tagdec_SS) ? "
            f"(omega_OS{suffix} * omega_SS{suffix} / (omega_OS{suffix} * omega_SS{suffix} + (1 - omega_OS{suffix}) * (1 - omega_SS{suffix}))) : "
            f"("
            f"((tagdec_OS == -tagdec_SS) && (omega_OS{suffix} < omega_SS{suffix})) ? "
            f"(omega_OS{suffix} * (1 - omega_SS{suffix}) / (omega_OS{suffix} * (1 - omega_SS{suffix}) + (1 - omega_OS{suffix}) * omega_SS{suffix})) : "
            f"(omega_SS{suffix} * (1 - omega_OS{suffix}) / (omega_OS{suffix} * (1 - omega_SS{suffix}) + (1 - omega_OS{suffix}) * omega_SS{suffix}))"
            f")",
            [
                tagdec_OS,
                tagdec_SS,
                workspace.function(f"omega_OS{suffix}"),
                workspace.function(f"omega_SS{suffix}"),
            ],
        )
        omegabar = ROOT.RooFormulaVar(
            f"omegabar{suffix}",
            f"omegabar{suffix}",
            f"(tagdec_OS == tagdec_SS) ? "
            f"(omegabar_OS{suffix} * omegabar_SS{suffix} / (omegabar_OS{suffix} * omegabar_SS{suffix} + (1 - omegabar_OS{suffix}) * (1 - omegabar_SS{suffix}))) : "
            f"("
            f"((tagdec_OS == -tagdec_SS) && (omegabar_OS{suffix} < omegabar_SS{suffix})) ? "
            f"(omegabar_OS{suffix} * (1 - omegabar_SS{suffix}) / (omegabar_OS{suffix} * (1 - omegabar_SS{suffix}) + (1 - omegabar_OS{suffix}) * omegabar_SS{suffix})) : "
            f"(omegabar_SS{suffix} * (1 - omegabar_OS{suffix}) / (omegabar_OS{suffix} * (1 - omegabar_SS{suffix}) + (1 - omegabar_OS{suffix}) * omegabar_SS{suffix}))"
            f")",
            [
                tagdec_OS,
                tagdec_SS,
                workspace.function(f"omegabar_OS{suffix}"),
                workspace.function(f"omegabar_SS{suffix}"),
            ],
        )
    elif kwargs["ftcali"] == "calibrated":
        omega = ROOT.RooRealVar("omega", "omega", 0, 0.5)
        omegabar = ROOT.RooRealVar("omegabar", "omegabar", 0, 0.5)
        tagdec = ROOT.RooCategory(
            "tagdec", "tagdec", {"B0": 1, "B0bar": -1, "untagged": 0}
        )
    else:
        raise Exception(f"Unknown ftcali arg: {kwargs['ftcali']}")

    # -------- Dij, Fij and Uij --------
    if N > 0:
        D_var = ROOT.RooFormulaVar(
            f"D{suffix}",
            f"Dij{suffix}",
            f"({get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)}"
            f" - {get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )
    else:
        D_var = ROOT.RooFormulaVar(
            f"D{suffix}",
            f"Dij{suffix}",
            f"({get_physically_indexed_variable('k', 'j', M)} - {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )

    if N > 0:
        F_var = ROOT.RooFormulaVar(
            f"F{suffix}",
            f"Fij{suffix}",
            f"-sqrt({get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)} * {get_physically_indexed_variable('k', '-j', M)})"
            f" * (({get_physically_indexed_variable('C', 'i', N)} * {get_physically_indexed_variable('s', 'j', M)} - {get_physically_indexed_variable('S', 'i', N)} * {get_physically_indexed_variable('c', 'j', M)}) * cos(2 * beta)"
            f" - ({get_physically_indexed_variable('C', 'i', N)} * {get_physically_indexed_variable('c', 'j', M)} + {get_physically_indexed_variable('S', 'i', N)} * {get_physically_indexed_variable('s', 'j', M)}) * sin(2 * beta))",
            all_vars,
        )
    else:
        F_var = ROOT.RooFormulaVar(
            f"F{suffix}",
            f"Fij{suffix}",
            f"-sqrt({get_physically_indexed_variable('k', 'j', M)} * {get_physically_indexed_variable('k', '-j', M)})"
            f" * ({get_physically_indexed_variable('s', 'j', M)} * cos(2 * beta) - {get_physically_indexed_variable('c', 'j', M)} * sin(2 * beta))",
            all_vars,
        )

    if N > 0:
        U_var = ROOT.RooFormulaVar(
            f"U{suffix}",
            f"Uij{suffix}",
            f"({get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)}"
            f" + {get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )
    else:
        U_var = ROOT.RooFormulaVar(
            f"U{suffix}",
            f"Uij{suffix}",
            f"({get_physically_indexed_variable('k', 'j', M)} + {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )

    # -------- Construct coefficient functions for RooBDecay --------
    if kwargs["ftcali"] == "uncalibrated":
        suffix_ft = suffix
    elif kwargs["ftcali"] == "calibrated":
        suffix_ft = ""
    else:
        raise Exception(f"Unknown ftcali arg: {kwargs['ftcali']}")

    coeff_cosh = ROOT.RooFormulaVar(
        f"coeff_cosh{suffix}",
        f"coeff_cosh{suffix}",
        f"(1 - tagdec{suffix_ft} * (omega{suffix_ft} - omegabar{suffix_ft})) * U{suffix}",
        [tagdec, omega, omegabar, U_var],
    )
    coeff_sinh = ROOT.RooFit.RooConst(1)
    coeff_cos = ROOT.RooFormulaVar(
        f"coeff_cos{suffix}",
        f"coeff_cos{suffix}",
        f"(tagdec{suffix_ft} * (1 - omega{suffix_ft} - omegabar{suffix_ft})) * D{suffix}",
        [tagdec, omega, omegabar, D_var],
    )
    coeff_sin = ROOT.RooFormulaVar(
        f"coeff_sin{suffix}",
        f"coeff_sin{suffix}",
        f"(tagdec{suffix_ft} * (1 - omega{suffix_ft} - omegabar{suffix_ft})) * F{suffix}",
        [tagdec, omega, omegabar, F_var],
    )

    # -------- decay time acceptance --------
    if kwargs["dta_share_params"]:
        suffix_dta = ""
    else:
        suffix_dta = suffix_dataset

    dta_knots_y = []
    for i in range(9):
        if i == 4:
            dta_knots_y.append(
                ROOT.RooRealVar(
                    f"dta_knot_y_{i}{suffix_dta}",
                    f"dta_knot_y_{i}{suffix_dta}",
                    1,
                )
            )
        else:
            dta_knots_y.append(
                ROOT.RooRealVar(
                    f"dta_knot_y_{i}{suffix_dta}",
                    f"dta_knot_y_{i}{suffix_dta}",
                    1,
                    0,
                    2,
                )
            )
    dta_unbinned = ROOT.RooCubicSplineFun(
        f"dta_unbinned{suffix}",
        f"dta_unbinned{suffix}",
        dt,
        [0.3, 0.58, 0.91, 1.35, 1.96, 3.01, 10],
        dta_knots_y,
    )

    nbins_dta = int((dt.getMax() - dt.getMin()) / 0.01)
    acceptanceBinning = ROOT.RooUniformBinning(
        dt.getMin(), dt.getMax(), nbins_dta, "acceptanceBinning"
    )
    dt.setBinning(acceptanceBinning, "acceptanceBinning")
    dta = ROOT.RooBinnedPdf(
        f"dta{suffix}", f"dta{suffix}", dt, "acceptanceBinning", dta_unbinned
    )
    dta.setForceUnitIntegral(True)

    # -------- decay time resolution --------
    mean_1_dtr = ROOT.RooRealVar(
        f"mean_1_dtr{suffix_dataset}", f"mean_1_dtr{suffix_dataset}", 0, -0.1, 0.1
    )
    width_1_dtr = ROOT.RooRealVar(
        f"width_1_dtr{suffix_dataset}", f"width_1_dtr{suffix_dataset}", 0.05, 0.005, 0.2
    )
    pdf_1_dtr = ROOT.RooGaussModel(
        f"pdf_1_dtr{suffix}", f"pdf_1_dtr{suffix}", dt, mean_1_dtr, width_1_dtr
    )
    f_1_dtr = ROOT.RooRealVar(
        f"f_1_dtr{suffix_dataset}", f"f_1_dtr{suffix_dataset}", 0.5, 0, 1
    )

    mean_2_dtr = ROOT.RooRealVar(
        f"mean_2_dtr{suffix_dataset}", f"mean_2_dtr{suffix_dataset}", 0, -0.1, 0.1
    )
    width_2_dtr = ROOT.RooRealVar(
        f"width_2_dtr{suffix_dataset}", f"width_2_dtr{suffix_dataset}", 0.05, 0.005, 0.2
    )
    pdf_2_dtr = ROOT.RooGaussModel(
        f"pdf_2_dtr{suffix}", f"pdf_2_dtr{suffix}", dt, mean_2_dtr, width_2_dtr
    )
    f_2_dtr = ROOT.RooRealVar(
        f"f_2_dtr{suffix_dataset}", f"f_2_dtr{suffix_dataset}", 0.5, 0, 1
    )

    mean_3_dtr = ROOT.RooRealVar(
        f"mean_3_dtr{suffix_dataset}", f"mean_3_dtr{suffix_dataset}", 0, -0.1, 0.1
    )
    width_3_dtr = ROOT.RooRealVar(
        f"width_3_dtr{suffix_dataset}", f"width_3_dtr{suffix_dataset}", 0.05, 0.005, 0.2
    )
    pdf_3_dtr = ROOT.RooGaussModel(
        f"pdf_3_dtr{suffix}", f"pdf_3_dtr{suffix}", dt, mean_3_dtr, width_3_dtr
    )

    dtr = ROOT.RooAddModel(
        f"dtr{suffix}",
        f"dtr{suffix}",
        [pdf_1_dtr, pdf_2_dtr, pdf_3_dtr],
        [f_1_dtr, f_2_dtr],
    )

    # -------- decay time resolution with decay time acceptance  --------
    dtr_with_dta = ROOT.RooEffResModel(
        f"dtr_with_dta{suffix}", f"dtr_with_dta{suffix}", dtr, dta
    )

    # -------- Construct generic B decay pdf using above user coefficients --------
    pdf_total = ROOT.RooBDecay(
        f"pdf_total{suffix}",
        f"pdf_total{suffix}",
        dt,
        tau,
        dgamma,
        coeff_cosh,
        coeff_sinh,
        coeff_cos,
        coeff_sin,
        dm,
        dtr_with_dta,
        type="SingleSided",
    )

    workspace.Import(pdf_total)

    # keep a reference to the variables to prevent them from being deleted, otherwise random segfault may occur
    kept_variables = all_vars

    return workspace, kept_variables


def get_workspace(M, N, *, workspace=None, **kwargs):
    if workspace is None:
        workspace = ROOT.RooWorkspace("w", "workspace")

    kept_variables = []

    sample_category = ROOT.RooCategory("sample_category", "sample_category")

    for KStype in ["LL", "DD"]:
        for run in kwargs["included_runs"]:
            workspace, variables = get_workspace_beta(
                M,
                N,
                workspace=workspace,
                suffix_KStype=f"_KS{KStype}",
                suffix_run=f"_run{run}",
                **kwargs,
            )
            kept_variables += variables

            sample_category.defineType(f"KS{KStype}_run{run}")

    pdf_total = ROOT.RooSimultaneous(
        "pdf_total",
        "pdf_total",
        {
            f"KS{KStype}_run{run}": workspace.pdf(f"pdf_total_KS{KStype}_run{run}")
            for KStype in ["LL", "DD"]
            for run in kwargs["included_runs"]
        },
        sample_category,
    )
    workspace.Import(pdf_total)

    return workspace, kept_variables


def get_cp_parameters(b2dpipi_parameters_filepath, M, N):
    with open(b2dpipi_parameters_filepath) as f:
        parameters = yaml.load(f, yaml.FullLoader)
        ci = parameters["ci"]
        si = parameters["si"]
        ki = parameters["ki"]
        kmi = parameters["kmi"]
    Ci = [
        -0.0154534380,
        0.8478508543,
        0.1892644422,
        -0.9137806340,
        -0.1550303715,
        0.3648258385,
        0.8644270022,
        0.8565834993,
    ]
    Si = [
        -0.8104234346,
        -0.1331743422,
        -0.8703767427,
        -0.0799645449,
        0.8545587019,
        0.7922451754,
        0.2067869318,
        -0.3323938070,
    ]
    Ki = [
        0.0203365780,
        0.0040000361,
        0.0032598069,
        0.0637987973,
        0.0312136149,
        0.0038962435,
        0.0493697765,
        0.0634571919,
    ]
    Kmi = [
        0.0960459496,
        0.1463195996,
        0.1433025377,
        0.1085394555,
        0.0540369696,
        0.0767518358,
        0.1145536063,
        0.0211180007,
    ]
    beta = 22 / 180 * np.pi

    ki = [kmi[-i - 1] for i in range(-M, 0)] + ki
    Ki = [Kmi[-i - 1] for i in range(-N, 0)] + Ki

    return Ci, Si, Ki, ci, si, ki, beta
