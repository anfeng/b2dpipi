import ROOT


def import_model(workspace: ROOT.RooWorkspace, **kwargs) -> None:
    dt = ROOT.RooRealVar("dt", "dt", 0.3, 14)
    # mix_state = ROOT.RooCategory(
    #     "mix_state", "mix_state", {"mixed": -1, "unmixed": 1, "untagged": 0}
    # )
    mix_state = ROOT.RooCategory("mix_state", "mix_state", {"mixed": -1, "unmixed": 1})
    eta = ROOT.RooRealVar("eta", "eta", 0, 0.5)
    tagdec = ROOT.RooCategory("tagdec", "tagdec", {"B0": 1, "B0bar": -1, "untagged": 0})

    eta_mean = ROOT.RooRealVar(
        "eta_mean", "eta_mean", 0.25
    )  # to be set later, based on data
    p0 = ROOT.RooRealVar("p0", "p0", 0.4, 0, 0.5)
    p1 = ROOT.RooRealVar("p1", "p1", 1, 0.5, 1.2)
    dp0 = ROOT.RooRealVar("dp0", "dp0", 0, -0.2, 0.2)
    dp1 = ROOT.RooRealVar("dp1", "dp1", 0, -0.2, 0.2)

    omega = ROOT.RooFormulaVar(
        "omega",
        "omega",
        "p0 + dp0 + (p1 + dp1) * (eta - eta_mean)",
        [eta, p0, p1, dp0, dp1, eta_mean],
    )
    omegabar = ROOT.RooFormulaVar(
        "omegabar",
        "omegabar",
        "p0 - dp0 + (p1 - dp1) * (eta - eta_mean)",
        [eta, p0, p1, dp0, dp1, eta_mean],
    )

    coeff_cosh = ROOT.RooFormulaVar(
        "coeff_cosh",
        "coeff_cosh",
        "1 - tagdec * (omega - omegabar)",
        [tagdec, omega, omegabar],
    )
    coeff_cos = ROOT.RooFormulaVar(
        "coeff_cos",
        "coeff_cos",
        "mix_state * (1 - omega - omegabar)",
        [mix_state, omega, omegabar],
    )

    # -------- decay time acceptance --------
    dta_knots_y = []
    for i in range(9):
        if i == 4:
            dta_knots_y.append(ROOT.RooRealVar(f"dta_knot_y_{i}", f"dta_knot_y_{i}", 1))
        else:
            dta_knots_y.append(
                ROOT.RooRealVar(f"dta_knot_y_{i}", f"dta_knot_y_{i}", 1, 0, 10)
            )
    dta_unbinned = ROOT.RooCubicSplineFun(
        "dta_unbinned",
        "dta_unbinned",
        dt,
        [0.3, 0.58, 0.91, 1.35, 1.96, 3.01, 10],
        dta_knots_y,
    )

    nbins_dta = int((dt.getMax() - dt.getMin()) / 0.01)
    acceptanceBinning = ROOT.RooUniformBinning(
        dt.getMin(), dt.getMax(), nbins_dta, "acceptanceBinning"
    )
    dt.setBinning(acceptanceBinning, "acceptanceBinning")
    dta = ROOT.RooBinnedPdf("dta", "dta", dt, "acceptanceBinning", dta_unbinned)
    dta.setForceUnitIntegral(True)

    # -------- decay time resolution --------
    mean_1_dtr = ROOT.RooRealVar("mean_1_dtr", "mean_1_dtr", 0, -0.1, 0.1)
    width_1_dtr = ROOT.RooRealVar("width_1_dtr", "width_1_dtr", 0.05, 0.005, 0.2)
    pdf_1_dtr = ROOT.RooGaussModel(
        "pdf_1_dtr", "pdf_1_dtr", dt, mean_1_dtr, width_1_dtr
    )
    f_1_dtr = ROOT.RooRealVar("f_1_dtr", "f_1_dtr", 0.5, 0, 1)

    mean_2_dtr = ROOT.RooRealVar("mean_2_dtr", "mean_2_dtr", 0, -0.1, 0.1)
    width_2_dtr = ROOT.RooRealVar("width_2_dtr", "width_2_dtr", 0.05, 0.005, 0.2)
    pdf_2_dtr = ROOT.RooGaussModel(
        "pdf_2_dtr", "pdf_2_dtr", dt, mean_2_dtr, width_2_dtr
    )
    f_2_dtr = ROOT.RooRealVar("f_2_dtr", "f_2_dtr", 0.5, 0, 1)

    mean_3_dtr = ROOT.RooRealVar("mean_3_dtr", "mean_3_dtr", 0, -0.1, 0.1)
    width_3_dtr = ROOT.RooRealVar("width_3_dtr", "width_3_dtr", 0.05, 0.005, 0.2)
    pdf_3_dtr = ROOT.RooGaussModel(
        "pdf_3_dtr", "pdf_3_dtr", dt, mean_3_dtr, width_3_dtr
    )

    dtr = ROOT.RooAddModel(
        "dtr", "dtr", [pdf_1_dtr, pdf_2_dtr, pdf_3_dtr], [f_1_dtr, f_2_dtr]
    )

    # -------- decay time resolution with decay time acceptance  --------
    dtr_with_dta = ROOT.RooEffResModel("dtr_with_dta", "dtr_with_dta", dtr, dta)

    # tm = ROOT.RooTruthModel("tm", "truth model", dt)

    pdf_total = ROOT.RooBDecay(
        "pdf_total",
        "pdf_total",
        dt,
        ROOT.RooFit.RooConst(1.517),
        ROOT.RooFit.RooConst(0),
        coeff_cosh,
        ROOT.RooFit.RooConst(0),
        coeff_cos,
        ROOT.RooFit.RooConst(0),
        ROOT.RooFit.RooConst(0.5069),
        dtr_with_dta,
        ROOT.RooBDecay.SingleSided,
    )

    workspace.Import(pdf_total)
