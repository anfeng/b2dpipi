import concurrent.futures
import functools
import re

import numpy as np
import yaml
from scipy.integrate import quad, trapezoid
from sympy import lambdify, symbols
from sympy.parsing.mathematica import parse_mathematica


def convert_physical_index_to_fully_mapped_programmatic_index(
    physical_index, half_limit
):
    return np.where(
        physical_index < 0, physical_index + half_limit, physical_index + half_limit - 1
    )


def convert_physical_index_to_half_mapped_programmatic_index(physical_index):
    return np.where(physical_index < 0, -physical_index - 1, physical_index - 1)


def getUDF(i, j, M, N, K, C, S, k, c, s, beta):
    i_fm = convert_physical_index_to_fully_mapped_programmatic_index(i, N)
    j_fm = convert_physical_index_to_fully_mapped_programmatic_index(j, M)
    mi_fm = convert_physical_index_to_fully_mapped_programmatic_index(-i, N)
    mj_fm = convert_physical_index_to_fully_mapped_programmatic_index(-j, M)
    i_hm = convert_physical_index_to_half_mapped_programmatic_index(i)
    j_hm = convert_physical_index_to_half_mapped_programmatic_index(j)

    Ki = np.take(K, i_fm)
    Kmi = np.take(K, mi_fm)
    Ci = np.take(C, i_hm)
    Si = np.where(i < 0, -np.take(S, i_hm), np.take(S, i_hm))

    kj = np.take(k, j_fm)
    kmj = np.take(k, mj_fm)
    cj = np.take(c, j_hm)
    sj = np.where(j < 0, -np.take(s, j_hm), np.take(s, j_hm))

    Uij = (Kmi * kj + Ki * kmj) / 2
    Dij = (Kmi * kj - Ki * kmj) / 2
    Fij = -np.sqrt(Ki * Kmi * kj * kmj) * (
        (Ci * sj - Si * cj) * np.cos(2 * beta) - (Ci * cj + Si * sj) * np.sin(2 * beta)
    )

    return Uij, Dij, Fij


def get_symbolic_functions(pdf_expr_filepaths, norm_expr_filepaths, norm_type="type3"):
    pdfs = []
    norms = []

    for i, pdf_expr_filepath in enumerate(pdf_expr_filepaths):
        with open(pdf_expr_filepath) as f_pdf:
            print(f"reading PDF {i + 1}...")
            file_content = f_pdf.read()
            file_content = file_content.replace("*^", "*10^")
            file_content = re.sub(r"\^-(\d+)", r"^(-\1)", file_content)
            pdfs.append(parse_mathematica(file_content))

    for i, norm_expr_filepath in enumerate(norm_expr_filepaths):
        with open(norm_expr_filepath) as f_norm:
            print(f"reading normalization factor {i + 1}...")
            file_content = f_norm.read()
            file_content = file_content.replace("*^", "*10^")
            file_content = re.sub(r"\^-(\d+)", r"^(-\1)", file_content)
            norms.append(parse_mathematica(file_content))
    normalization_factor = sum(norms)

    symbols_pdf = symbols(
        "t Uij Dij Fij q omega omegabar dm tau f1 f2 mu1 mu2 mu3 sigma1 sigma2 sigma3 y1 y2 y3 y4 y5 y6 y7 y8 y9"
    )
    assert len(pdfs[0].free_symbols) == len(symbols_pdf)
    pdf_funcs = [lambdify(symbols_pdf, pdf) for pdf in pdfs]

    if norm_type == "type1":
        symbols_norm = symbols(
            "q omega omegabar Utot Dtot Ftot dm tau f1 f2 mu1 mu2 mu3 sigma1 sigma2 sigma3 y1 y2 y3 y4 y5 y6 y7 y8 y9"
        )
    elif norm_type in ["type2", "type3"]:
        symbols_norm = symbols(
            "Utot tau f1 f2 mu1 mu2 mu3 sigma1 sigma2 sigma3 y1 y2 y3 y4 y5 y6 y7 y8 y9"
        )
    else:
        raise Exception(f"unknown norm type: {norm_type}")
    assert len(norms[0].free_symbols) == len(symbols_norm)
    norm_func = lambdify(symbols_norm, normalization_factor)

    return pdf_funcs, norm_func, pdfs, normalization_factor


class B2DPiPiModel:
    def __init__(
        self,
        *,
        KStype,
        run,
        pdf_funcs,
        norm_func,
        norm_type="type3",
        M,
        N,
        dta_knots_x,
        num_threads=1,
        param_names=[],
    ):
        self.KStype = KStype
        self.run = run
        self.pdf_funcs = pdf_funcs
        self.norm_func = norm_func
        self.norm_type = norm_type
        self.M = M
        self.N = N
        self.dta_knots_x = dta_knots_x
        self.num_threads = num_threads
        self.param_names = param_names

        self.indices_Dbin = list(range(-N, 0)) + list(range(1, N + 1))
        self.indices_Bbin = list(range(-M, 0)) + list(range(1, M + 1))

        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=num_threads)

    def unnormalized_pdf(
        self,
        t,
        i,
        j,
        q,
        omega,
        omegabar,
        params,
    ):
        # hyper parameters
        KStype = self.KStype
        run = self.run
        M = self.M
        N = self.N
        dta_knots_x = self.dta_knots_x
        pdf_funcs = self.pdf_funcs

        # parameters
        K = np.array([params[f"K{i}"] for i in range(2 * N)])
        C = np.array([params[f"C{i}"] for i in range(N)])
        S = np.array([params[f"S{i}"] for i in range(N)])
        k = np.array([params[f"k{j}"] for j in range(2 * M)])
        c = np.array([params[f"c{j}"] for j in range(M)])
        s = np.array([params[f"s{j}"] for j in range(M)])
        beta = params["beta"]
        dm = params["dm"]
        tau = params["tau"]
        f1 = params[f"f1_KS{KStype}_run{run}"]
        f2 = params[f"f2_KS{KStype}_run{run}"]
        mu1 = params[f"mu1_KS{KStype}_run{run}"]
        mu2 = params[f"mu2_KS{KStype}_run{run}"]
        mu3 = params[f"mu3_KS{KStype}_run{run}"]
        sigma1 = params[f"sigma1_KS{KStype}_run{run}"]
        sigma2 = params[f"sigma2_KS{KStype}_run{run}"]
        sigma3 = params[f"sigma3_KS{KStype}_run{run}"]
        y1 = params["y1"]
        y2 = params["y2"]
        y3 = params["y3"]
        y4 = params["y4"]
        y5 = params["y5"]
        y6 = params["y6"]
        y7 = params["y7"]
        y8 = params["y8"]
        y9 = params["y9"]

        Uij, Dij, Fij = getUDF(i, j, M, N, K, C, S, k, c, s, beta)

        segment_indices = np.digitize(t, dta_knots_x)
        segment_indices = np.where(segment_indices == 0, 1, segment_indices)
        segment_indices = np.where(
            segment_indices == len(dta_knots_x), len(dta_knots_x) - 1, segment_indices
        )
        segment_indices = segment_indices - 1

        pdf_values = np.array(
            [
                pdf(
                    t,
                    Uij,
                    Dij,
                    Fij,
                    q,
                    omega,
                    omegabar,
                    dm,
                    tau,
                    f1,
                    f2,
                    mu1,
                    mu2,
                    mu3,
                    sigma1,
                    sigma2,
                    sigma3,
                    y1,
                    y2,
                    y3,
                    y4,
                    y5,
                    y6,
                    y7,
                    y8,
                    y9,
                )
                for pdf in pdf_funcs
            ]
        )

        pdf_value = np.choose(segment_indices, pdf_values)

        return np.where(pdf_value < 0, np.nextafter(0, 1), pdf_value)

    def normalize_analytic(self, *, q=0, omega=0, omegabar=0, params):
        # hyper parameters
        KStype = self.KStype
        run = self.run
        M = self.M
        N = self.N
        indices_Dbin = self.indices_Dbin
        indices_Bbin = self.indices_Bbin
        norm_func = self.norm_func

        # parameters
        K = np.array([params[f"K{i}"] for i in range(2 * N)])
        C = np.array([params[f"C{i}"] for i in range(N)])
        S = np.array([params[f"S{i}"] for i in range(N)])
        k = np.array([params[f"k{j}"] for j in range(2 * M)])
        c = np.array([params[f"c{j}"] for j in range(M)])
        s = np.array([params[f"s{j}"] for j in range(M)])
        beta = params["beta"]
        dm = params["dm"]
        tau = params["tau"]
        f1 = params[f"f1_KS{KStype}_run{run}"]
        f2 = params[f"f2_KS{KStype}_run{run}"]
        mu1 = params[f"mu1_KS{KStype}_run{run}"]
        mu2 = params[f"mu2_KS{KStype}_run{run}"]
        mu3 = params[f"mu3_KS{KStype}_run{run}"]
        sigma1 = params[f"sigma1_KS{KStype}_run{run}"]
        sigma2 = params[f"sigma2_KS{KStype}_run{run}"]
        sigma3 = params[f"sigma3_KS{KStype}_run{run}"]
        y1 = params["y1"]
        y2 = params["y2"]
        y3 = params["y3"]
        y4 = params["y4"]
        y5 = params["y5"]
        y6 = params["y6"]
        y7 = params["y7"]
        y8 = params["y8"]
        y9 = params["y9"]

        Utot = 0
        Dtot = 0
        Ftot = 0
        for i in indices_Dbin:
            for j in indices_Bbin:
                Uij, Dij, Fij = getUDF(i, j, M, N, K, C, S, k, c, s, beta)
                Utot += Uij
                Dtot += Dij
                Ftot += Fij

        if self.norm_type == "type1":
            return norm_func(
                q,
                omega,
                omegabar,
                Utot,
                Dtot,
                Ftot,
                dm,
                tau,
                f1,
                f2,
                mu1,
                mu2,
                mu3,
                sigma1,
                sigma2,
                sigma3,
                y1,
                y2,
                y3,
                y4,
                y5,
                y6,
                y7,
                y8,
                y9,
            )
        elif self.norm_type in ["type2", "type3"]:
            return norm_func(
                Utot,
                tau,
                f1,
                f2,
                mu1,
                mu2,
                mu3,
                sigma1,
                sigma2,
                sigma3,
                y1,
                y2,
                y3,
                y4,
                y5,
                y6,
                y7,
                y8,
                y9,
            )
        else:
            raise Exception(f"unknown norm type: {self.norm_type}")

    @functools.cache
    def normalize_quad(self, *, q=0, omega=0, omegabar=0, **params):
        dta_knots_x = self.dta_knots_x
        indices_Dbin = self.indices_Dbin
        indices_Bbin = self.indices_Bbin

        print(
            f"doing normalization integration for q={q}, omega={omega}, omegabar={omegabar}"
        )
        if self.norm_type == "type1":
            return sum(
                [
                    quad(
                        lambda t: self.unnormalized_pdf(
                            t, i, j, q, omega, omegabar, params
                        ),
                        dta_knots_x[0],
                        dta_knots_x[-1],
                    )[0]
                    for i in indices_Dbin
                    for j in indices_Bbin
                ]
            )
        elif self.norm_type in ["type2", "type3"]:
            return sum(
                [
                    quad(
                        lambda t: self.unnormalized_pdf(
                            t, i, j, q, omega, omegabar, params
                        ),
                        dta_knots_x[0],
                        dta_knots_x[-1],
                    )[0]
                    for i in indices_Dbin
                    for j in indices_Bbin
                    for q in [1, 0, -1]
                ]
            )
        else:
            raise Exception(f"unknown norm type: {self.norm_type}")

    def normalize_trapezoid(self, *, q=0, omega=0, omegabar=0, params):
        print(
            f"doing normalization integration for q={q}, omega={omega}, omegabar={omegabar}"
        )
        dta_knots_x = self.dta_knots_x
        indices_Dbin = self.indices_Dbin
        indices_Bbin = self.indices_Bbin

        t_sample = np.linspace(dta_knots_x[0], dta_knots_x[-1], 100)
        fcn = lambda i, j, q: trapezoid(
            np.array(
                [
                    self.unnormalized_pdf(t, i, j, q, omega, omegabar, params)
                    for t in t_sample
                ]
            ),
            x=t_sample,
            axis=0,
        )
        if self.norm_type == "type1":
            return sum([fcn(i, j, q) for i in indices_Dbin for j in indices_Bbin])
        elif self.norm_type in ["type2", "type3"]:
            return sum(
                [
                    fcn(i, j, q)
                    for i in indices_Dbin
                    for j in indices_Bbin
                    for q in [1, 0, -1]
                ]
            )
        else:
            raise Exception(f"unknown norm type: {self.norm_type}")

    def pdf(
        self,
        t,
        i,
        j,
        q,
        omega,
        omegabar,
        params,
        *,
        integration_method="analytic",
        num_threads=None,
    ):
        if num_threads is None:
            num_threads = self.num_threads
        executor = self.executor

        # wrapper functions
        def get_unnormalized_pdf_values(t, i, j, q, omega, omegabar):
            return self.unnormalized_pdf(t, i, j, q, omega, omegabar, params)

        def get_normalization_factor(q, omega, omegabar):
            if integration_method == "analytic":
                normalization_factor = self.normalize_analytic(
                    q=q, omega=omega, omegabar=omegabar, params=params
                )
            elif integration_method == "quad":
                print("doing normalization integration...")
                normalization_factor = np.vectorize(
                    lambda q, omega, omegabar: self.normalize_quad(
                        q=q, omega=omega, omegabar=omegabar, **params
                    )
                )(q, omega, omegabar)
            elif integration_method == "trapezoid":
                normalization_factor = self.normalize_trapezoid(
                    q=q, omega=omega, omegabar=omegabar, params=params
                )
            else:
                raise Exception(f"unknown integration method {integration_method}")

            return normalization_factor

        # do the calculation
        if num_threads == 1:
            unnormalized_pdf_values = get_unnormalized_pdf_values(
                t, i, j, q, omega, omegabar
            )
            normalization_factor = get_normalization_factor(q, omega, omegabar)
        else:
            unnormalized_pdf_values = np.concatenate(
                [
                    res
                    for res in executor.map(
                        get_unnormalized_pdf_values,
                        np.array_split(t, num_threads),
                        np.array_split(i, num_threads),
                        np.array_split(j, num_threads),
                        np.array_split(q, num_threads),
                        np.array_split(omega, num_threads),
                        np.array_split(omegabar, num_threads),
                    )
                ]
            )
            if self.norm_type == "type1":
                normalization_factor = np.concatenate(
                    [
                        res
                        for res in executor.map(
                            get_normalization_factor,
                            np.array_split(q, num_threads),
                            np.array_split(omega, num_threads),
                            np.array_split(omegabar, num_threads),
                        )
                    ]
                )
            elif self.norm_type in ["type2", "type3"]:
                normalization_factor = get_normalization_factor(0, 0, 0)
            else:
                raise Exception(f"unknown norm type: {self.norm_type}")

        return unnormalized_pdf_values / normalization_factor

    def wrapped_pdf(self, x, *params):
        # This x order corresponds to variable order in input data
        t = x[0]
        i = x[1].astype(np.int64)
        j = x[2].astype(np.int64)
        q = x[3].astype(np.int64)
        omega = x[4]
        omegabar = x[5]

        # The order of "params" corresponds to "name" attr of Minuit() object
        params_dict = {}
        for index, key in enumerate(self.param_names):
            params_dict[key] = params[index]

        return self.pdf(t, i, j, q, omega, omegabar, params_dict)


def get_cp_parameters(b2dpipi_parameters_filepath, M, N):
    with open(b2dpipi_parameters_filepath) as f:
        parameters = yaml.load(f, yaml.FullLoader)
        ci = parameters["ci"]
        si = parameters["si"]
        ki = parameters["ki"]
        kmi = parameters["kmi"]
    Ci = [
        -0.0154534380,
        0.8478508543,
        0.1892644422,
        -0.9137806340,
        -0.1550303715,
        0.3648258385,
        0.8644270022,
        0.8565834993,
    ]
    Si = [
        -0.8104234346,
        -0.1331743422,
        -0.8703767427,
        -0.0799645449,
        0.8545587019,
        0.7922451754,
        0.2067869318,
        -0.3323938070,
    ]
    Ki = [
        0.0203365780,
        0.0040000361,
        0.0032598069,
        0.0637987973,
        0.0312136149,
        0.0038962435,
        0.0493697765,
        0.0634571919,
    ]
    Kmi = [
        0.0960459496,
        0.1463195996,
        0.1433025377,
        0.1085394555,
        0.0540369696,
        0.0767518358,
        0.1145536063,
        0.0211180007,
    ]
    beta = 22 / 180 * np.pi

    ki = [kmi[-i - 1] for i in range(-M, 0)] + ki
    Ki = [Kmi[-i - 1] for i in range(-N, 0)] + Ki

    return Ci, Si, Ki, ci, si, ki, beta


def get_pdf_parameters(snakemake_object):
    from collections import OrderedDict

    import ROOT

    M = int(snakemake_object.wildcards["M"])
    N = int(snakemake_object.wildcards["N"])
    config = snakemake_object.config["fit_beta_config"]

    parameters = OrderedDict()

    # -------- decay time resolution fit results --------
    for KStype in ["LL", "DD"]:
        for run in config["included_runs"]:
            dtrfit_fitresult_file = ROOT.TFile(
                str(
                    snakemake_object.input[
                        f"dtrfit_fitresult_KS{KStype}_run{run}_filepath"
                    ]
                )
            )
            dtrfit_fitresult = dtrfit_fitresult_file.Get("fitresult_best")
            parameters[f"mu1_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("mean_1_dtr").getVal()
            )
            parameters[f"sigma1_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("width_1_dtr").getVal()
            )
            parameters[f"f1_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("f_1_dtr").getVal()
            )
            parameters[f"mu2_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("mean_2_dtr").getVal()
            )
            parameters[f"sigma2_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("width_2_dtr").getVal()
            )
            parameters[f"f2_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("f_2_dtr").getVal()
            )
            parameters[f"mu3_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("mean_3_dtr").getVal()
            )
            parameters[f"sigma3_KS{KStype}_run{run}"] = (
                dtrfit_fitresult.floatParsFinal().find("width_3_dtr").getVal()
            )

    # -------- Dalitz plot parameters --------
    C, S, K, c, s, k, beta = get_cp_parameters(
        snakemake_object.input["b2dpipi_parameters_filepath"], M, N
    )
    for i in range(2 * N):
        parameters[f"K{i}"] = K[i]
    for i in range(N):
        parameters[f"C{i}"] = C[i]
        parameters[f"S{i}"] = S[i]
    for j in range(2 * M):
        parameters[f"k{j}"] = k[j]
    for j in range(M):
        parameters[f"c{j}"] = c[j]
        parameters[f"s{j}"] = s[j]
    parameters["beta"] = beta

    # -------- decay time acceptance parameters --------
    parameters[f"y1"] = 0.15
    parameters[f"y2"] = 0.51
    parameters[f"y3"] = 0.80
    parameters[f"y4"] = 1
    parameters[f"y5"] = 1.09
    parameters[f"y6"] = 1.13
    parameters[f"y7"] = 1
    parameters[f"y8"] = 1
    parameters[f"y9"] = 0.85

    # -------- other parameters --------
    parameters["dm"] = 0.5074
    parameters["tau"] = 1.517

    return parameters


def get_models(snakemake_object, *, parameters=None):
    import numba

    M = int(snakemake_object.wildcards["M"])
    N = int(snakemake_object.wildcards["N"])
    config = snakemake_object.config["fit_beta_config"]
    dta_knots_x = config["dta_knots_x"]

    if parameters is None:
        parameters = get_pdf_parameters(snakemake_object)

    # -------- read symbolic expressions for PDF and normalization factor --------
    pdf_funcs, norm_func, _, _ = get_symbolic_functions(
        snakemake_object.input["pdf_expression_filepaths"],
        snakemake_object.input["norm_expression_filepaths"],
    )
    pdf_funcs = [numba.njit(pdf_func, parallel=True) for pdf_func in pdf_funcs]
    norm_func = numba.njit(norm_func, parallel=True)

    # -------- define PDFs --------
    models = {}

    if snakemake_object.wildcards["model_name"] == "model_beta":
        for KStype in ["LL", "DD"]:
            for run in config["included_runs"]:
                models[f"KS{KStype}_run{run}"] = B2DPiPiModel(
                    KStype=KStype,
                    run=run,
                    pdf_funcs=pdf_funcs,
                    norm_func=norm_func,
                    M=M,
                    N=N,
                    dta_knots_x=dta_knots_x,
                    # num_threads=min(snakemake.threads, 5),
                    num_threads=1,
                    param_names=list(parameters.keys()),
                )
    elif snakemake_object.wildcards["model_name"] == "model_hh":
        if snakemake_object.wildcards["datatype"] != "toy":
            included_channels = ["KK", "pipi"]
        else:
            included_channels = ["KK"]

        for channel in included_channels:
            for run in config["included_runs"]:
                models[f"{channel}_run{run}"] = B2DPiPiModel(
                    KStype="LL",
                    run=run,
                    pdf_funcs=pdf_funcs,
                    norm_func=norm_func,
                    M=M,
                    N=N,
                    dta_knots_x=dta_knots_x,
                    # num_threads=min(snakemake.threads, 5),
                    num_threads=1,
                    param_names=list(parameters.keys()),
                )
    else:
        raise Exception(
            f"unknown model name {snakemake_object.wildcards['model_name']}"
        )

    return models


def get_data(snakemake_object):
    import dask.dataframe as dd
    from dask.distributed import Client, LocalCluster

    cluster = LocalCluster(n_workers=snakemake_object.threads, threads_per_worker=1)
    client = Client(cluster)

    config = snakemake_object.config["fit_beta_config"]
    dta_knots_x = config["dta_knots_x"]

    data = {}

    if snakemake_object.wildcards["model_name"] == "model_beta":
        for KStype in ["LL", "DD"]:
            for run in config["included_runs"]:
                df = dd.read_parquet(
                    snakemake_object.input[f"KSpipi_KS{KStype}_run{run}_filepaths"],
                    columns=[
                        "B_ConsPV_M",
                        "dt",
                        "i",
                        "j",
                        "tagdec",
                        "omega",
                        "omegabar",
                        # "N_B2Dpipi_sw",
                    ],
                )
                df = df.compute()
                df = df.query("(B_ConsPV_M > 5240) & (B_ConsPV_M < 5320)")
                df = df.query("(i != 0) & (j != 0)")
                df = df.query(f"(dt >= {dta_knots_x[0]}) & (dt <= {dta_knots_x[-1]})")

                # This variable order corresponds to iminuit data order
                data[f"KS{KStype}_run{run}"] = (
                    df[["dt", "i", "j", "tagdec", "omega", "omegabar"]].to_numpy().T
                )
    elif snakemake_object.wildcards["model_name"] == "model_hh":
        if snakemake_object.wildcards["datatype"] != "toy":
            for channel in ["KK", "pipi"]:
                for run in config["included_runs"]:
                    df = dd.read_parquet(
                        snakemake_object.input[f"{channel}_run{run}_filepaths"],
                        columns=[
                            "B_M_PV",
                            "dt",
                            "i",
                            "j",
                            "tagdec",
                            "omega",
                            "omegabar",
                            # "N_B2Dpipi_sw",
                        ],
                    )
                    df = df.compute()
                    df = df.query("(B_M_PV > 5240) & (B_M_PV < 5320)")
                    df = df.query("(i != 0) & (j != 0)")
                    df = df.query(
                        f"(dt >= {dta_knots_x[0]}) & (dt <= {dta_knots_x[-1]})"
                    )

                    # This variable order corresponds to iminuit data order
                    data[f"{channel}_run{run}"] = (
                        df[["dt", "i", "j", "tagdec", "omega", "omegabar"]].to_numpy().T
                    )
        else:
            for channel in ["KK"]:
                for run in config["included_runs"]:
                    df = dd.read_parquet(
                        snakemake_object.input[f"{channel}_run{run}_filepaths"],
                        columns=[
                            "dt",
                            "i",
                            "j",
                            "tagdec",
                            "omega",
                            "omegabar",
                        ],
                    )
                    df = df.compute()
                    df = df.query("(i != 0) & (j != 0)")
                    df = df.query(
                        f"(dt >= {dta_knots_x[0]}) & (dt <= {dta_knots_x[-1]})"
                    )

                    # This variable order corresponds to iminuit data order
                    data[f"{channel}_run{run}"] = (
                        df[["dt", "i", "j", "tagdec", "omega", "omegabar"]].to_numpy().T
                    )
    else:
        raise Exception(
            f"unknown model name {snakemake_object.wildcards['model_name']}"
        )

    client.close()
    cluster.close()

    return data
