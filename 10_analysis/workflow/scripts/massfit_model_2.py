import ROOT


def import_model(
    workspace: ROOT.RooWorkspace, *, m_B=None, Bdecay="B2Dpipi", **kwargs
) -> None:

    scale_sigma_B = ROOT.RooRealVar("scale_sigma_B", "scale_sigma_B", 1, 0.2, 5)

    if Bdecay == "B2Dpipi":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_B2Dpipi", "m_B_B2Dpipi", 5150, 5400)

        m0_B_B2Dpipi = ROOT.RooRealVar("m0_B_B2Dpipi", "m0_B_B2Dpipi", 5280, 5260, 5300)
        sigma0L_B_B2Dpipi = ROOT.RooRealVar(
            "sigma0L_B_B2Dpipi", "sigma0L_B_B2Dpipi", 20, 1, 100
        )
        sigma0R_B_B2Dpipi = ROOT.RooRealVar(
            "sigma0R_B_B2Dpipi", "sigma0R_B_B2Dpipi", 20, 1, 100
        )
        alphaL_B_B2Dpipi = ROOT.RooRealVar(
            "alphaL_B_B2Dpipi", "alphaL_B_B2Dpipi", 1, 0.01, 10
        )
        alphaR_B_B2Dpipi = ROOT.RooRealVar(
            "alphaR_B_B2Dpipi", "alphaR_B_B2Dpipi", 1, 0.01, 10
        )
        n_B_B2Dpipi = ROOT.RooRealVar("n_B_B2Dpipi", "n_B_B2Dpipi", 1, 0.1, 10)

        sigmaL_B_B2Dpipi = ROOT.RooFormulaVar(
            f"sigmaL_B_B2Dpipi",
            f"sigmaL_B_B2Dpipi",
            "scale_sigma_B * sigma0L_B_B2Dpipi",
            [scale_sigma_B, sigma0L_B_B2Dpipi],
        )
        sigmaR_B_B2Dpipi = ROOT.RooFormulaVar(
            f"sigmaR_B_B2Dpipi",
            f"sigmaR_B_B2Dpipi",
            "scale_sigma_B * sigma0R_B_B2Dpipi",
            [scale_sigma_B, sigma0R_B_B2Dpipi],
        )

        pdf_DCB_B_B2Dpipi = ROOT.RooCrystalBall(
            f"pdf_DCB_B_B2Dpipi",
            f"pdf_DCB_B_B2Dpipi",
            m_B,
            m0_B_B2Dpipi,
            sigmaL_B_B2Dpipi,
            sigmaR_B_B2Dpipi,
            alphaL_B_B2Dpipi,
            n_B_B2Dpipi,
            alphaR_B_B2Dpipi,
            n_B_B2Dpipi,
        )

        m0_gauss_B_B2Dpipi = ROOT.RooRealVar(
            "m0_gauss_B_B2Dpipi", "m0_gauss_B_B2Dpipi", 5280, 5220, 5340
        )
        sigma0_gauss_B_B2Dpipi = ROOT.RooRealVar(
            "sigma0_gauss_B_B2Dpipi", "sigma0_gauss_B_B2Dpipi", 20, 1, 50
        )
        sigma_gauss_B_B2Dpipi = ROOT.RooFormulaVar(
            f"sigma_gauss_B_B2Dpipi",
            f"sigma_gauss_B_B2Dpipi",
            "scale_sigma_B * sigma0_gauss_B_B2Dpipi",
            [scale_sigma_B, sigma0_gauss_B_B2Dpipi],
        )

        pdf_gauss_B_B2Dpipi = ROOT.RooGaussian(
            f"pdf_gauss_B_B2Dpipi",
            f"pdf_gauss_B_B2Dpipi",
            m_B,
            m0_gauss_B_B2Dpipi,
            sigma_gauss_B_B2Dpipi,
        )

        f_DCB_B_B2Dpipi = ROOT.RooRealVar(
            "f_DCB_B_B2Dpipi", "f_DCB_B_B2Dpipi", 0.5, 0, 1
        )

        pdf_B = ROOT.RooAddPdf(
            "pdf_B_B2Dpipi",
            "pdf_B_B2Dpipi",
            [pdf_DCB_B_B2Dpipi, pdf_gauss_B_B2Dpipi],
            [f_DCB_B_B2Dpipi],
        )

    elif Bdecay == "B2DKpi":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_B2DKpi", "m_B_B2DKpi", 5100, 5350)

        m0_B_B2DKpi = ROOT.RooRealVar("m0_B_B2DKpi", "m0_B_B2DKpi", 5240, 5200, 5280)
        sigma0L_B_B2DKpi = ROOT.RooRealVar(
            "sigma0L_B_B2DKpi", "sigma0L_B_B2DKpi", 20, 5, 50
        )
        sigma0R_B_B2DKpi = ROOT.RooRealVar(
            "sigma0R_B_B2DKpi", "sigma0R_B_B2DKpi", 20, 5, 50
        )
        alphaL_B_B2DKpi = ROOT.RooRealVar(
            "alphaL_B_B2DKpi", "alphaL_B_B2DKpi", 1, 0.1, 10
        )
        alphaR_B_B2DKpi = ROOT.RooRealVar(
            "alphaR_B_B2DKpi", "alphaR_B_B2DKpi", 1, 0.1, 10
        )
        n_B_B2DKpi = ROOT.RooRealVar("n_B_B2DKpi", "n_B_B2DKpi", 1, 0.05, 2)

        sigmaL_B_B2DKpi = ROOT.RooFormulaVar(
            f"sigmaL_B_B2DKpi",
            f"sigmaL_B_B2DKpi",
            "scale_sigma_B * sigma0L_B_B2DKpi",
            [scale_sigma_B, sigma0L_B_B2DKpi],
        )
        sigmaR_B_B2DKpi = ROOT.RooFormulaVar(
            f"sigmaR_B_B2DKpi",
            f"sigmaR_B_B2DKpi",
            "scale_sigma_B * sigma0R_B_B2DKpi",
            [scale_sigma_B, sigma0R_B_B2DKpi],
        )

        pdf_DCB_B_B2DKpi = ROOT.RooCrystalBall(
            f"pdf_DCB_B_B2DKpi",
            f"pdf_DCB_B_B2DKpi",
            m_B,
            m0_B_B2DKpi,
            sigmaL_B_B2DKpi,
            sigmaR_B_B2DKpi,
            alphaL_B_B2DKpi,
            n_B_B2DKpi,
            alphaR_B_B2DKpi,
            n_B_B2DKpi,
        )

        m0_gauss_B_B2DKpi = ROOT.RooRealVar(
            "m0_gauss_B_B2DKpi", "m0_gauss_B_B2DKpi", 5230, 5170, 5290
        )
        sigma0_gauss_B_B2DKpi = ROOT.RooRealVar(
            "sigma0_gauss_B_B2DKpi", "sigma0_gauss_B_B2DKpi", 20, 1, 50
        )
        sigma_gauss_B_B2DKpi = ROOT.RooFormulaVar(
            f"sigma_gauss_B_B2DKpi",
            f"sigma_gauss_B_B2DKpi",
            "scale_sigma_B * sigma0_gauss_B_B2DKpi",
            [scale_sigma_B, sigma0_gauss_B_B2DKpi],
        )

        pdf_gauss_B_B2DKpi = ROOT.RooGaussian(
            f"pdf_gauss_B_B2DKpi",
            f"pdf_gauss_B_B2DKpi",
            m_B,
            m0_gauss_B_B2DKpi,
            sigma_gauss_B_B2DKpi,
        )

        f_DCB_B_B2DKpi = ROOT.RooRealVar("f_DCB_B_B2DKpi", "f_DCB_B_B2DKpi", 0.5, 0, 1)

        pdf_B = ROOT.RooAddPdf(
            "pdf_B_B2DKpi",
            "pdf_B_B2DKpi",
            [pdf_DCB_B_B2DKpi, pdf_gauss_B_B2DKpi],
            [f_DCB_B_B2DKpi],
        )

    elif Bdecay == "Bs2DKpi":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_Bs2DKpi", "m_B_Bs2DKpi", 5100, 5450)

        m0_B_Bs2DKpi = ROOT.RooRealVar("m0_B_Bs2DKpi", "m0_B_Bs2DKpi", 5320, 5300, 5350)
        sigma0L_B_Bs2DKpi = ROOT.RooRealVar(
            "sigma0L_B_Bs2DKpi", "sigma0L_B_Bs2DKpi", 20, 5, 50
        )
        sigma0R_B_Bs2DKpi = ROOT.RooRealVar(
            "sigma0R_B_Bs2DKpi", "sigma0R_B_Bs2DKpi", 20, 5, 50
        )
        alphaL_B_Bs2DKpi = ROOT.RooRealVar(
            "alphaL_B_Bs2DKpi", "alphaL_B_Bs2DKpi", 1, 0.1, 10
        )
        alphaR_B_Bs2DKpi = ROOT.RooRealVar(
            "alphaR_B_Bs2DKpi", "alphaR_B_Bs2DKpi", 1, 0.1, 10
        )
        n_B_Bs2DKpi = ROOT.RooRealVar("n_B_Bs2DKpi", "n_B_Bs2DKpi", 1, 0.1, 2)

        sigmaL_B_Bs2DKpi = ROOT.RooFormulaVar(
            f"sigmaL_B_Bs2DKpi",
            f"sigmaL_B_Bs2DKpi",
            "scale_sigma_B * sigma0L_B_Bs2DKpi",
            [scale_sigma_B, sigma0L_B_Bs2DKpi],
        )
        sigmaR_B_Bs2DKpi = ROOT.RooFormulaVar(
            f"sigmaR_B_Bs2DKpi",
            f"sigmaR_B_Bs2DKpi",
            "scale_sigma_B * sigma0R_B_Bs2DKpi",
            [scale_sigma_B, sigma0R_B_Bs2DKpi],
        )

        pdf_DCB_B_Bs2DKpi = ROOT.RooCrystalBall(
            f"pdf_DCB_B_Bs2DKpi",
            f"pdf_DCB_B_Bs2DKpi",
            m_B,
            m0_B_Bs2DKpi,
            sigmaL_B_Bs2DKpi,
            sigmaR_B_Bs2DKpi,
            alphaL_B_Bs2DKpi,
            n_B_Bs2DKpi,
            alphaR_B_Bs2DKpi,
            n_B_Bs2DKpi,
        )

        m0_gauss_B_Bs2DKpi = ROOT.RooRealVar(
            "m0_gauss_B_Bs2DKpi", "m0_gauss_B_Bs2DKpi", 5300, 5240, 5360
        )
        sigma0_gauss_B_Bs2DKpi = ROOT.RooRealVar(
            "sigma0_gauss_B_Bs2DKpi", "sigma0_gauss_B_Bs2DKpi", 20, 1, 50
        )
        sigma_gauss_B_Bs2DKpi = ROOT.RooFormulaVar(
            f"sigma_gauss_B_Bs2DKpi",
            f"sigma_gauss_B_Bs2DKpi",
            "scale_sigma_B * sigma0_gauss_B_Bs2DKpi",
            [scale_sigma_B, sigma0_gauss_B_Bs2DKpi],
        )

        pdf_gauss_B_Bs2DKpi = ROOT.RooGaussian(
            f"pdf_gauss_B_Bs2DKpi",
            f"pdf_gauss_B_Bs2DKpi",
            m_B,
            m0_gauss_B_Bs2DKpi,
            sigma_gauss_B_Bs2DKpi,
        )

        f_DCB_B_Bs2DKpi = ROOT.RooRealVar(
            "f_DCB_B_Bs2DKpi", "f_DCB_B_Bs2DKpi", 0.5, 0, 1
        )

        pdf_B = ROOT.RooAddPdf(
            "pdf_B_Bs2DKpi",
            "pdf_B_Bs2DKpi",
            [pdf_DCB_B_Bs2DKpi, pdf_gauss_B_Bs2DKpi],
            [f_DCB_B_Bs2DKpi],
        )

    elif Bdecay == "Lb2Dppi":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_Lb2Dppi", "m_B_Lb2Dppi", 5050, 5650)

        m0_B_Lb2Dppi = ROOT.RooRealVar("m0_B_Lb2Dppi", "m0_B_Lb2Dppi", 5480, 5400, 5500)
        sigma0L_B_Lb2Dppi = ROOT.RooRealVar(
            "sigma0L_B_Lb2Dppi", "sigma0L_B_Lb2Dppi", 50, 5, 200
        )
        sigma0R_B_Lb2Dppi = ROOT.RooRealVar(
            "sigma0R_B_Lb2Dppi", "sigma0R_B_Lb2Dppi", 50, 5, 200
        )
        alphaL_B_Lb2Dppi = ROOT.RooRealVar(
            "alphaL_B_Lb2Dppi", "alphaL_B_Lb2Dppi", 1, 0.1, 10
        )
        alphaR_B_Lb2Dppi = ROOT.RooRealVar(
            "alphaR_B_Lb2Dppi", "alphaR_B_Lb2Dppi", 1, 0.1, 10
        )
        n_B_Lb2Dppi = ROOT.RooRealVar("n_B_Lb2Dppi", "n_B_Lb2Dppi", 1, 0, 5)

        sigmaL_B_Lb2Dppi = ROOT.RooFormulaVar(
            f"sigmaL_B_Lb2Dppi",
            f"sigmaL_B_Lb2Dppi",
            "scale_sigma_B * sigma0L_B_Lb2Dppi",
            [scale_sigma_B, sigma0L_B_Lb2Dppi],
        )
        sigmaR_B_Lb2Dppi = ROOT.RooFormulaVar(
            f"sigmaR_B_Lb2Dppi",
            f"sigmaR_B_Lb2Dppi",
            "scale_sigma_B * sigma0R_B_Lb2Dppi",
            [scale_sigma_B, sigma0R_B_Lb2Dppi],
        )

        pdf_DCB_B_Lb2Dppi = ROOT.RooCrystalBall(
            f"pdf_DCB_B_Lb2Dppi",
            f"pdf_DCB_B_Lb2Dppi",
            m_B,
            m0_B_Lb2Dppi,
            sigmaL_B_Lb2Dppi,
            sigmaR_B_Lb2Dppi,
            alphaL_B_Lb2Dppi,
            n_B_Lb2Dppi,
            alphaR_B_Lb2Dppi,
            n_B_Lb2Dppi,
        )

        m0_gauss_B_Lb2Dppi = ROOT.RooRealVar(
            "m0_gauss_B_Lb2Dppi", "m0_gauss_B_Lb2Dppi", 5220, 5160, 5280
        )
        sigma0_gauss_B_Lb2Dppi = ROOT.RooRealVar(
            "sigma0_gauss_B_Lb2Dppi", "sigma0_gauss_B_Lb2Dppi", 150, 10, 250
        )
        sigma_gauss_B_Lb2Dppi = ROOT.RooFormulaVar(
            f"sigma_gauss_B_Lb2Dppi",
            f"sigma_gauss_B_Lb2Dppi",
            "scale_sigma_B * sigma0_gauss_B_Lb2Dppi",
            [scale_sigma_B, sigma0_gauss_B_Lb2Dppi],
        )

        pdf_gauss_B_Lb2Dppi = ROOT.RooGaussian(
            f"pdf_gauss_B_Lb2Dppi",
            f"pdf_gauss_B_Lb2Dppi",
            m_B,
            m0_gauss_B_Lb2Dppi,
            sigma_gauss_B_Lb2Dppi,
        )

        f_DCB_B_Lb2Dppi = ROOT.RooRealVar(
            "f_DCB_B_Lb2Dppi", "f_DCB_B_Lb2Dppi", 0.5, 0, 1
        )

        pdf_B = ROOT.RooAddPdf(
            "pdf_B_Lb2Dppi",
            "pdf_B_Lb2Dppi",
            [pdf_DCB_B_Lb2Dppi, pdf_gauss_B_Lb2Dppi],
            [f_DCB_B_Lb2Dppi],
        )

    elif Bdecay == "Bu2Dpi":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_Bu2Dpi", "m_B_Bu2Dpi", 5380, 5760)

        m0_B_Bu2Dpi = ROOT.RooRealVar("m0_B_Bu2Dpi", "m0_B_Bu2Dpi", 5420, 5380, 5460)
        A_B_Bu2Dpi = ROOT.RooRealVar("A_B_Bu2Dpi", "A_B_Bu2Dpi", -30, -60, 0)
        B_B_Bu2Dpi = ROOT.RooRealVar("B_B_Bu2Dpi", "B_B_Bu2Dpi", 0, -10, 10)
        C_B_Bu2Dpi = ROOT.RooRealVar("C_B_Bu2Dpi", "C_B_Bu2Dpi", 20, 0, 50)
        pdf_phys_B_Bu2Dpi = ROOT.RooDstD0BG(
            "pdf_phys_B_Bu2Dpi",
            "pdf_phys_B_Bu2Dpi",
            m_B,
            m0_B_Bu2Dpi,
            C_B_Bu2Dpi,
            A_B_Bu2Dpi,
            B_B_Bu2Dpi,
        )

        sigma0_B_Bu2Dpi = ROOT.RooRealVar(
            "sigma0_B_Bu2Dpi", "sigma0_B_Bu2Dpi", 20, 5, 50
        )
        sigma_resolution_B_Bu2Dpi = ROOT.RooFormulaVar(
            f"sigma_resolution_B_Bu2Dpi",
            f"sigma_resolution_B_Bu2Dpi",
            "scale_sigma_B * sigma0_B_Bu2Dpi",
            [scale_sigma_B, sigma0_B_Bu2Dpi],
        )
        pdf_resolution_B_Bu2Dpi = ROOT.RooGaussian(
            "pdf_resolution_B_Bu2Dpi",
            "pdf_resolution_B_Bu2Dpi",
            m_B,
            0,
            sigma_resolution_B_Bu2Dpi,
        )

        pdf_B = ROOT.RooFFTConvPdf(
            "pdf_B_Bu2Dpi",
            "pdf_B_Bu2Dpi",
            m_B,
            pdf_phys_B_Bu2Dpi,
            pdf_resolution_B_Bu2Dpi,
        )

    elif Bdecay == "Bu2Dstpi_pi0":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_Bu2Dstpi_pi0", "m_B_Bu2Dstpi_pi0", 5050, 5800)

        m0_B_Bu2Dstpi_pi0 = ROOT.RooRealVar(
            "m0_B_Bu2Dstpi_pi0", "m0_B_Bu2Dstpi_pi0", 5230, 5180, 5300
        )
        A_B_Bu2Dstpi_pi0 = ROOT.RooRealVar(
            "A_B_Bu2Dstpi_pi0", "A_B_Bu2Dstpi_pi0", -40, -80, 0
        )
        B_B_Bu2Dstpi_pi0 = ROOT.RooRealVar(
            "B_B_Bu2Dstpi_pi0", "B_B_Bu2Dstpi_pi0", 0, -10, 10
        )
        C_B_Bu2Dstpi_pi0 = ROOT.RooRealVar(
            "C_B_Bu2Dstpi_pi0", "C_B_Bu2Dstpi_pi0", 20, 0, 50
        )
        pdf_phys_B_Bu2Dstpi_pi0 = ROOT.RooDstD0BG(
            "pdf_phys_B_Bu2Dstpi_pi0",
            "pdf_phys_B_Bu2Dstpi_pi0",
            m_B,
            m0_B_Bu2Dstpi_pi0,
            C_B_Bu2Dstpi_pi0,
            A_B_Bu2Dstpi_pi0,
            B_B_Bu2Dstpi_pi0,
        )

        sigma0_B_Bu2Dstpi_pi0 = ROOT.RooRealVar(
            "sigma0_B_Bu2Dstpi_pi0", "sigma0_B_Bu2Dstpi_pi0", 80, 5, 150
        )
        sigma_resolution_B_Bu2Dstpi_pi0 = ROOT.RooFormulaVar(
            f"sigma_resolution_B_Bu2Dstpi_pi0",
            f"sigma_resolution_B_Bu2Dstpi_pi0",
            "scale_sigma_B * sigma0_B_Bu2Dstpi_pi0",
            [scale_sigma_B, sigma0_B_Bu2Dstpi_pi0],
        )
        pdf_resolution_B_Bu2Dstpi_pi0 = ROOT.RooGaussian(
            "pdf_resolution_B_Bu2Dstpi_pi0",
            "pdf_resolution_B_Bu2Dstpi_pi0",
            m_B,
            0,
            sigma_resolution_B_Bu2Dstpi_pi0,
        )

        pdf_B = ROOT.RooFFTConvPdf(
            "pdf_B_Bu2Dstpi_pi0",
            "pdf_B_Bu2Dstpi_pi0",
            m_B,
            pdf_phys_B_Bu2Dstpi_pi0,
            pdf_resolution_B_Bu2Dstpi_pi0,
        )

    elif Bdecay == "Bu2Dstpi_gamma":

        if m_B is None:
            m_B = ROOT.RooRealVar(
                "m_B_Bu2Dstpi_gamma", "m_B_Bu2Dstpi_gamma", 5050, 5800
            )

        m0_B_Bu2Dstpi_gamma = ROOT.RooRealVar(
            "m0_B_Bu2Dstpi_gamma", "m0_B_Bu2Dstpi_gamma", 5260, 5150, 5370
        )
        A_B_Bu2Dstpi_gamma = ROOT.RooRealVar(
            "A_B_Bu2Dstpi_gamma", "A_B_Bu2Dstpi_gamma", -40, -80, 0
        )
        B_B_Bu2Dstpi_gamma = ROOT.RooRealVar(
            "B_B_Bu2Dstpi_gamma", "B_B_Bu2Dstpi_gamma", 0, -10, 10
        )
        C_B_Bu2Dstpi_gamma = ROOT.RooRealVar(
            "C_B_Bu2Dstpi_gamma", "C_B_Bu2Dstpi_gamma", 100, 0, 200
        )
        pdf_phys_B_Bu2Dstpi_gamma = ROOT.RooDstD0BG(
            "pdf_phys_B_Bu2Dstpi_gamma",
            "pdf_phys_B_Bu2Dstpi_gamma",
            m_B,
            m0_B_Bu2Dstpi_gamma,
            C_B_Bu2Dstpi_gamma,
            A_B_Bu2Dstpi_gamma,
            B_B_Bu2Dstpi_gamma,
        )

        sigma0_B_Bu2Dstpi_gamma = ROOT.RooRealVar(
            "sigma0_B_Bu2Dstpi_gamma", "sigma0_B_Bu2Dstpi_gamma", 80, 5, 150
        )
        sigma_resolution_B_Bu2Dstpi_gamma = ROOT.RooFormulaVar(
            f"sigma_resolution_B_Bu2Dstpi_gamma",
            f"sigma_resolution_B_Bu2Dstpi_gamma",
            "scale_sigma_B * sigma0_B_Bu2Dstpi_gamma",
            [scale_sigma_B, sigma0_B_Bu2Dstpi_gamma],
        )
        pdf_resolution_B_Bu2Dstpi_gamma = ROOT.RooGaussian(
            "pdf_resolution_B_Bu2Dstpi_gamma",
            "pdf_resolution_B_Bu2Dstpi_gamma",
            m_B,
            0,
            sigma_resolution_B_Bu2Dstpi_gamma,
        )

        pdf_B = ROOT.RooFFTConvPdf(
            "pdf_B_Bu2Dstpi_gamma",
            "pdf_B_Bu2Dstpi_gamma",
            m_B,
            pdf_phys_B_Bu2Dstpi_gamma,
            pdf_resolution_B_Bu2Dstpi_gamma,
        )

    elif Bdecay == "Bd2Dstpi":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_Bd2Dstpi", "m_B_Bd2Dstpi", 5050, 5750)

        m0_B_Bd2Dstpi = ROOT.RooRealVar(
            "m0_B_Bd2Dstpi", "m0_B_Bd2Dstpi", 5230, 5180, 5300
        )
        A_B_Bd2Dstpi = ROOT.RooRealVar("A_B_Bd2Dstpi", "A_B_Bd2Dstpi", -30, -60, 0)
        B_B_Bd2Dstpi = ROOT.RooRealVar("B_B_Bd2Dstpi", "B_B_Bd2Dstpi", 0, -10, 10)
        C_B_Bd2Dstpi = ROOT.RooRealVar("C_B_Bd2Dstpi", "C_B_Bd2Dstpi", 10, 0, 30)
        pdf_phys_B_Bd2Dstpi = ROOT.RooDstD0BG(
            "pdf_phys_B_Bd2Dstpi",
            "pdf_phys_B_Bd2Dstpi",
            m_B,
            m0_B_Bd2Dstpi,
            C_B_Bd2Dstpi,
            A_B_Bd2Dstpi,
            B_B_Bd2Dstpi,
        )

        sigma0_B_Bd2Dstpi = ROOT.RooRealVar(
            "sigma0_B_Bd2Dstpi", "sigma0_B_Bd2Dstpi", 50, 5, 120
        )
        sigma_resolution_B_Bd2Dstpi = ROOT.RooFormulaVar(
            f"sigma_resolution_B_Bd2Dstpi",
            f"sigma_resolution_B_Bd2Dstpi",
            "scale_sigma_B * sigma0_B_Bd2Dstpi",
            [scale_sigma_B, sigma0_B_Bd2Dstpi],
        )
        pdf_resolution_B_Bd2Dstpi = ROOT.RooGaussian(
            "pdf_resolution_B_Bd2Dstpi",
            "pdf_resolution_B_Bd2Dstpi",
            m_B,
            0,
            sigma_resolution_B_Bd2Dstpi,
        )

        pdf_B = ROOT.RooFFTConvPdf(
            "pdf_B_Bd2Dstpi",
            "pdf_B_Bd2Dstpi",
            m_B,
            pdf_phys_B_Bd2Dstpi,
            pdf_resolution_B_Bd2Dstpi,
        )

    elif Bdecay == "fakeB":

        if m_B is None:
            m_B = ROOT.RooRealVar("m_B_fakeB", "m_B_fakeB", 5240, 5700)

        c_B_fakeB = ROOT.RooRealVar("c_B_fakeB", "c_B_fakeB", 0.002, 0, 0.01)

        pdf_B = ROOT.RooExponential(
            f"pdf_B_fakeB",
            f"pdf_B_fakeB",
            m_B,
            c_B_fakeB,
            negateCoefficient=True,
        )

    else:

        raise Exception(f"invalid B decay {Bdecay}")

    workspace.Import(pdf_B)
