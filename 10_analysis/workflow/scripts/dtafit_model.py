import ROOT


def import_model(workspace: ROOT.RooWorkspace, **kwargs) -> None:
    dt = ROOT.RooRealVar("dt", "dt", 0.3, 14)

    # -------- decay time acceptance --------
    dta_knots_y = []
    for i in range(9):
        dta_knots_y.append(
            ROOT.RooRealVar(f"dta_knot_y_{i}", f"dta_knot_y_{i}", 0.5, 0, 1)
        )
    dta_unbinned = ROOT.RooCubicSplineFun(
        "dta_unbinned",
        "dta_unbinned",
        dt,
        [0.3, 0.58, 0.91, 1.35, 1.96, 3.01, 10],
        dta_knots_y,
    )

    nbins_dta = int((dt.getMax() - dt.getMin()) / 0.01)
    acceptanceBinning = ROOT.RooUniformBinning(
        dt.getMin(), dt.getMax(), nbins_dta, "acceptanceBinning"
    )
    dt.setBinning(acceptanceBinning, "acceptanceBinning")
    dta = ROOT.RooBinnedPdf("dta", "dta", dt, "acceptanceBinning", dta_unbinned)
    dta.setForceUnitIntegral(True)

    # -------- decay time resolution --------
    mean_dtr = ROOT.RooRealVar("mean_dtr", "mean_dtr", 0)
    width_dtr = ROOT.RooRealVar("width_dtr", "width_dtr", 0.05)
    dtr = ROOT.RooGaussModel("dtr", "dtr", dt, mean_dtr, width_dtr)

    # -------- decay time resolution with decay time acceptance  --------
    dtr_with_dta = ROOT.RooEffResModel("dtr_with_dta", "dtr_with_dta", dtr, dta)

    # -------- total PDF --------
    tau = ROOT.RooRealVar("tau", "tau", 1.519, 1.2, 1.8)
    pdf_total = ROOT.RooBDecay(
        "pdf_total",
        "pdf_total",
        dt,
        tau,
        ROOT.RooFit.RooConst(0),
        ROOT.RooFit.RooConst(1),
        ROOT.RooFit.RooConst(0),
        ROOT.RooFit.RooConst(0),
        ROOT.RooFit.RooConst(0),
        ROOT.RooFit.RooConst(0),
        dtr_with_dta,
        ROOT.RooBDecay.SingleSided,
    )

    workspace.Import(pdf_total)
