import ROOT


def import_model(workspace: ROOT.RooWorkspace, *, Bdecay="B2Dpipi", **kwargs) -> None:

    m_B = ROOT.RooRealVar("B_M_massfit", "B_M_massfit", 5240, 5700)
    scale_sigma_B = ROOT.RooRealVar("scale_sigma_B", "scale_sigma_B", 1, 0.2, 5)

    if Bdecay == "B2Dpipi":

        m0_B = ROOT.RooRealVar("m0_B_B2Dpipi", "m0_B_B2Dpipi", 5280, 5260, 5300)
        sigma0L_B = ROOT.RooRealVar(
            "sigma0L_B_B2Dpipi", "sigma0L_B_B2Dpipi", 20, 1, 100
        )
        sigma0R_B = ROOT.RooRealVar(
            "sigma0R_B_B2Dpipi", "sigma0R_B_B2Dpipi", 20, 1, 100
        )
        alphaL_B = ROOT.RooRealVar("alphaL_B_B2Dpipi", "alphaL_B_B2Dpipi", 1, 0.01, 10)
        alphaR_B = ROOT.RooRealVar("alphaR_B_B2Dpipi", "alphaR_B_B2Dpipi", 1, 0.01, 10)
        n_B = ROOT.RooRealVar("n_B_B2Dpipi", "n_B_B2Dpipi", 1, 0.1, 10)

        sigmaL_B = ROOT.RooFormulaVar(
            f"sigmaL_B_B2Dpipi",
            f"sigmaL_B_B2Dpipi",
            "scale_sigma_B * sigma0L_B_B2Dpipi",
            [scale_sigma_B, sigma0L_B],
        )
        sigmaR_B = ROOT.RooFormulaVar(
            f"sigmaR_B_B2Dpipi",
            f"sigmaR_B_B2Dpipi",
            "scale_sigma_B * sigma0R_B_B2Dpipi",
            [scale_sigma_B, sigma0R_B],
        )

        pdf_B = ROOT.RooCrystalBall(
            f"pdf_B_B2Dpipi",
            f"pdf_B_B2Dpipi",
            m_B,
            m0_B,
            sigmaL_B,
            sigmaR_B,
            alphaL_B,
            n_B,
            alphaR_B,
            n_B,
        )

    elif Bdecay == "fakeB":

        c_B_fakeB = ROOT.RooRealVar("c_B_fakeB", "c_B_fakeB", 0.002, 0, 0.01)

        pdf_B = ROOT.RooExponential(
            f"pdf_B_fakeB",
            f"pdf_B_fakeB",
            m_B,
            c_B_fakeB,
            negateCoefficient=True,
        )

    else:

        raise Exception(f"invalid B decay {Bdecay}")

    workspace.Import(pdf_B)


def import_model_forfom(workspace: ROOT.RooWorkspace, **kwargs):
    pdf_list = []
    N_list = []
    for Bdecay in ["B2Dpipi", "fakeB"]:
        suffix_B = f"_{Bdecay}"

        import_model(workspace, Bdecay=Bdecay, **kwargs)
        pdf_list.append(workspace.pdf(f"pdf_B{suffix_B}"))

        if Bdecay == "B2Dpipi":
            N = ROOT.RooRealVar(
                f"N{suffix_B}",
                f"N{suffix_B}",
                20000,
                1000,
                100000,
            )
        else:
            N = ROOT.RooRealVar(
                f"N{suffix_B}",
                f"N{suffix_B}",
                500000,
                100000,
                10000000,
            )
        N_list.append(N)

    pdf_total = ROOT.RooAddPdf("pdf_total", "pdf_total", pdf_list, N_list)
    workspace.Import(pdf_total)


def import_model_fortoy(workspace: ROOT.RooWorkspace, **kwargs):
    pdf_list = []
    N_list = []
    for Bdecay in ["B2Dpipi", "fakeB"]:
        suffix_B = f"_{Bdecay}"

        import_model(workspace, Bdecay=Bdecay, **kwargs)
        pdf_list.append(workspace.pdf(f"pdf_B{suffix_B}"))

        if Bdecay == "B2Dpipi":
            N = ROOT.RooRealVar(
                f"N{suffix_B}",
                f"N{suffix_B}",
                20000,
                10000,
                100000,
            )
        else:
            N = ROOT.RooRealVar(
                f"N{suffix_B}",
                f"N{suffix_B}",
                5000,
                1000,
                10000,
            )
        N_list.append(N)

    pdf_total = ROOT.RooAddPdf("pdf_total", "pdf_total", pdf_list, N_list)
    workspace.Import(pdf_total)
