import numpy as np
import ROOT
from utils import Tee, get_cp_parameters

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

# physical Dbin index: -N, -N + 1, ..., -1, 1, ..., N - 1, N
# physical Bbin index: -M, -M + 1, ..., -1, 1, ..., M - 1, M
# fully-mapped programmatic Dbin index: 0, 1, ..., 2 * N - 1 corresponding to -N, -N + 1, ..., -1, 1, ..., N - 1, N
# fully-mapped programmatic Bbin index: 0, 1, ..., 2 * M - 1 corresponding to -M, -M + 1, ..., -1, 1, ..., M - 1, M
# half-mapped programmatic Dbin index: 0, 1, ..., N - 1 corresponding to 1, ..., N - 1, N
# half-mapped programmatic Bbin index: 0, 1, ..., M - 1 corresponding to 1, ..., M - 1, M


def get_indexed_variable(variable_name, index_expression, upper_limit, index=0):
    # returns an expression yielding {variable_name}{index_expression}
    # , where variable_name is a variable and index_expression, whose value ranges from 0 (included) to upper_limit (excluded), is an expression of some variables
    # , e.g. get_indexed_variable('K', 'i + 1', 10) yields K1 if the value of i + 1 is 1
    if index < upper_limit:
        return f"((({index_expression}) == {index}) ? ({variable_name}{index}) : ({get_indexed_variable(variable_name, index_expression, upper_limit, index + 1)}))"
    else:
        return 0


def get_physically_indexed_variable(variable_name, index_expression, half_limit):
    if variable_name == "K" or variable_name == "k":
        # convert physical index to fully-mapped programmatic index:
        return get_indexed_variable(
            variable_name,
            f"((({index_expression}) < 0) ? (({index_expression}) + {half_limit}) : (({index_expression}) + {half_limit} - 1))",
            2 * half_limit,
        )
    elif variable_name == "C" or variable_name == "c":
        # convert physical index to half-mapped programmatic index:
        return get_indexed_variable(
            variable_name,
            f"((({index_expression}) < 0) ? (-({index_expression}) - 1) : (({index_expression}) - 1))",
            half_limit,
        )
    elif variable_name == "S" or variable_name == "s":
        # convert physical index to half-mapped programmatic index:
        return f'((({index_expression}) < 0) ? (-{get_indexed_variable(variable_name, f"-({index_expression}) - 1", half_limit)}) : ({get_indexed_variable(variable_name, f"({index_expression}) - 1", half_limit)}))'


def get_workspace_beta(M, N, workspace=None, suffix=""):
    # N=0 is for no-D-Dalitz-plot case, e.g. D -> hh

    if workspace is None:
        workspace = ROOT.RooWorkspace("w", "workspace")

    # Generic B-decay with user coefficients
    # -------------------------

    # Construct pdf
    # -------------------------

    # using physical bin index:
    dict_of_Dbin_index = {}
    dict_of_Bbin_index = {}
    for i in list(range(-N, 0)) + list(range(1, N + 1)):
        dict_of_Dbin_index[str(i)] = i
    for j in list(range(-M, 0)) + list(range(1, M + 1)):
        dict_of_Bbin_index[str(j)] = j

    # Observable
    dt = ROOT.RooRealVar("dt", "dt", 0, 10)
    dt.setBins(50)
    tagFlav = ROOT.RooCategory(
        "tagFlav", "Flavour of the tagged B0", {"B0": 1, "B0bar": -1}
    )
    index_Dbin = ROOT.RooCategory("i", "index_Dbin", dict_of_Dbin_index)
    index_Bbin = ROOT.RooCategory("j", "index_Bbin", dict_of_Bbin_index)

    # Parameters
    dm = ROOT.RooRealVar("dm", "delta m(B0)", 0.472)
    tau = ROOT.RooRealVar("tau", "tau (B0)", 1.547)
    w = ROOT.RooRealVar("w", "flavour mistag rate", 0.36)

    # using fully-mapped programmatic Dbin index:
    K_var = [ROOT.RooRealVar(f"K{i}", f"K[{i}]", 0, 1) for i in range(2 * N)]
    # using half-mapped programmatic Dbin index:
    C_var = [ROOT.RooRealVar(f"C{i}", f"C[{i}]", -2, 2) for i in range(N)]
    # using half-mapped programmatic Dbin index:
    S_var = [ROOT.RooRealVar(f"S{i}", f"S[{i}]", -2, 2) for i in range(N)]
    # using fully-mapped programmatic Bbin index:
    k_var = [ROOT.RooRealVar(f"k{i}", f"k[{i}]", 0, 1) for i in range(2 * M)]
    # using half-mapped programmatic Bbin index:
    c_var = [ROOT.RooRealVar(f"c{i}", f"c[{i}]", -2, 2) for i in range(M)]
    # using half-mapped programmatic Bbin index:
    s_var = [ROOT.RooRealVar(f"s{i}", f"s[{i}]", -2, 2) for i in range(M)]
    beta_var = ROOT.RooRealVar("beta", "beta", 0, np.pi / 2)

    all_vars = (
        K_var
        + C_var
        + S_var
        + k_var
        + c_var
        + s_var
        + [beta_var, index_Dbin, index_Bbin]
    )

    # Use delta function resolution model
    tm = ROOT.RooTruthModel(f"tm{suffix}", f"truth model {suffix}", dt)

    if N > 0:
        Adir = ROOT.RooFormulaVar(
            f"D{suffix}",
            f"Dij{suffix}",
            f"({get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)}"
            f" - {get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )
    else:
        Adir = ROOT.RooFormulaVar(
            f"D{suffix}",
            f"Dij{suffix}",
            f"({get_physically_indexed_variable('k', 'j', M)} - {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )

    if N > 0:
        Amix = ROOT.RooFormulaVar(
            f"F{suffix}",
            f"Fij{suffix}",
            f"-sqrt({get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)} * {get_physically_indexed_variable('k', '-j', M)})"
            f" * (({get_physically_indexed_variable('C', 'i', N)} * {get_physically_indexed_variable('s', 'j', M)} - {get_physically_indexed_variable('S', 'i', N)} * {get_physically_indexed_variable('c', 'j', M)}) * cos(2 * beta)"
            f" - ({get_physically_indexed_variable('C', 'i', N)} * {get_physically_indexed_variable('c', 'j', M)} + {get_physically_indexed_variable('S', 'i', N)} * {get_physically_indexed_variable('s', 'j', M)}) * sin(2 * beta))",
            all_vars,
        )
    else:
        Amix = ROOT.RooFormulaVar(
            f"F{suffix}",
            f"Fij{suffix}",
            f"-sqrt({get_physically_indexed_variable('k', 'j', M)} * {get_physically_indexed_variable('k', '-j', M)})"
            f" * ({get_physically_indexed_variable('s', 'j', M)} * cos(2 * beta) - {get_physically_indexed_variable('c', 'j', M)} * sin(2 * beta))",
            all_vars,
        )

    # Derived input parameters for pdf
    DG = ROOT.RooFit.RooConst(0)

    # Construct coefficient functions for sin,cos, modulations of decay
    # distribution
    if N > 0:
        fcosh = ROOT.RooFormulaVar(
            f"U{suffix}",
            f"Uij{suffix}",
            f"({get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)}"
            f" + {get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )
    else:
        fcosh = ROOT.RooFormulaVar(
            f"U{suffix}",
            f"Uij{suffix}",
            f"({get_physically_indexed_variable('k', 'j', M)} + {get_physically_indexed_variable('k', '-j', M)}) / 2",
            all_vars,
        )
    fsinh = ROOT.RooFit.RooConst(1)
    fcos = ROOT.RooFormulaVar(
        f"fcos{suffix}", f"fcos{suffix}", "@0*@1*(1-2*@2)", [Adir, tagFlav, w]
    )
    fsin = ROOT.RooFormulaVar(
        f"fsin{suffix}", f"fsin{suffix}", "@0*@1*(1-2*@2)", [Amix, tagFlav, w]
    )

    # Construct generic B decay pdf using above user coefficients
    bcpg0 = ROOT.RooBDecay(
        f"bcpg0{suffix}",
        f"bcpg0{suffix}",
        dt,
        tau,
        DG,
        fcosh,
        fsinh,
        fcos,
        fsin,
        dm,
        tm,
        type="SingleSided",
    )

    workspace.Import(bcpg0)

    # keep a reference to the variables to prevent them from being deleted, otherwise random segfault may occur
    kept_variables = [
        dt,
        tagFlav,
        index_Dbin,
        index_Bbin,
        dm,
        tau,
        w,
        K_var,
        C_var,
        S_var,
        k_var,
        c_var,
        s_var,
        beta_var,
    ]

    return workspace, kept_variables


def get_workspace_cos2betasin2beta(M, N):
    workspace = ROOT.RooWorkspace("w", "workspace")

    # Generic B-decay with user coefficients
    # -------------------------

    # Construct pdf
    # -------------------------

    # using physical bin index:
    dict_of_Dbin_index = {}
    dict_of_Bbin_index = {}
    for i in list(range(-N, 0)) + list(range(1, N + 1)):
        dict_of_Dbin_index[str(i)] = i
    for j in list(range(-M, 0)) + list(range(1, M + 1)):
        dict_of_Bbin_index[str(j)] = j

    # Observable
    dt = ROOT.RooRealVar("dt", "dt", 0, 10)
    dt.setBins(50)
    tagFlav = ROOT.RooCategory(
        "tagFlav", "Flavour of the tagged B0", {"B0": 1, "B0bar": -1}
    )
    index_Dbin = ROOT.RooCategory("i", "index_Dbin", dict_of_Dbin_index)
    index_Bbin = ROOT.RooCategory("j", "index_Bbin", dict_of_Bbin_index)

    # Parameters
    dm = ROOT.RooRealVar("dm", "delta m(B0)", 0.472)
    tau = ROOT.RooRealVar("tau", "tau (B0)", 1.547)
    w = ROOT.RooRealVar("w", "flavour mistag rate", 0.36)

    # using fully-mapped programmatic Dbin index:
    K_var = [ROOT.RooRealVar(f"K{i}", f"K[{i}]", 0, 1) for i in range(2 * N)]
    # using half-mapped programmatic Dbin index:
    C_var = [ROOT.RooRealVar(f"C{i}", f"C[{i}]", -1, 1) for i in range(N)]
    # using half-mapped programmatic Dbin index:
    S_var = [ROOT.RooRealVar(f"S{i}", f"S[{i}]", -1, 1) for i in range(N)]
    # using fully-mapped programmatic Bbin index:
    k_var = [ROOT.RooRealVar(f"k{i}", f"k[{i}]", 0, 1) for i in range(2 * M)]
    # using half-mapped programmatic Bbin index:
    c_var = [ROOT.RooRealVar(f"c{i}", f"c[{i}]", -1, 1) for i in range(M)]
    # using half-mapped programmatic Bbin index:
    s_var = [ROOT.RooRealVar(f"s{i}", f"s[{i}]", -1, 1) for i in range(M)]
    cos2beta_var = ROOT.RooRealVar("cos2beta", "cos2beta", 0, 3)
    sin2beta_var = ROOT.RooRealVar("sin2beta", "sin2beta", 0, 3)

    all_vars = (
        K_var
        + C_var
        + S_var
        + k_var
        + c_var
        + s_var
        + [cos2beta_var, sin2beta_var, index_Dbin, index_Bbin]
    )

    # Use delta function resolution model
    tm = ROOT.RooTruthModel(f"tm", "truth model", dt)

    Adir = ROOT.RooFormulaVar(
        f"D",
        "Dij",
        f"({get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)}"
        f" - {get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('k', '-j', M)}) / 2",
        all_vars,
    )
    Amix = ROOT.RooFormulaVar(
        f"F",
        "Fij",
        f"-sqrt({get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)} * {get_physically_indexed_variable('k', '-j', M)})"
        f" * (({get_physically_indexed_variable('C', 'i', N)} * {get_physically_indexed_variable('s', 'j', M)} - {get_physically_indexed_variable('S', 'i', N)} * {get_physically_indexed_variable('c', 'j', M)}) * cos2beta"
        f" - ({get_physically_indexed_variable('C', 'i', N)} * {get_physically_indexed_variable('c', 'j', M)} + {get_physically_indexed_variable('S', 'i', N)} * {get_physically_indexed_variable('s', 'j', M)}) * sin2beta)",
        all_vars,
    )

    # Derived input parameters for pdf
    DG = ROOT.RooFit.RooConst(0)

    # Construct coefficient functions for sin,cos, modulations of decay
    # distribution
    fcosh = ROOT.RooFormulaVar(
        "U",
        "Uij",
        f"({get_physically_indexed_variable('K', '-i', N)} * {get_physically_indexed_variable('k', 'j', M)}"
        f" + {get_physically_indexed_variable('K', 'i', N)} * {get_physically_indexed_variable('k', '-j', M)}) / 2",
        all_vars,
    )
    fsinh = ROOT.RooFit.RooConst(1)
    fcos = ROOT.RooFormulaVar(f"fcos", "fcos", "@0*@1*(1-2*@2)", [Adir, tagFlav, w])
    fsin = ROOT.RooFormulaVar(f"fsin", "fsin", "@0*@1*(1-2*@2)", [Amix, tagFlav, w])

    # Construct generic B decay pdf using above user coefficients
    bcpg0 = ROOT.RooBDecay(
        f"bcpg0",
        "bcpg0",
        dt,
        tau,
        DG,
        fcosh,
        fsinh,
        fcos,
        fsin,
        dm,
        tm,
        type="SingleSided",
    )

    workspace.Import(bcpg0)

    return workspace


M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)
if snakemake.wildcards.model_name == "model_beta":
    workspace, _ = get_workspace_beta(M, N)
elif snakemake.wildcards.model_name == "model_cos2betasin2beta":
    workspace = get_workspace_cos2betasin2beta(M, N)
elif snakemake.wildcards.model_name == "model_combined":
    workspace = ROOT.RooWorkspace("w", "workspace")
    _ws, _var = get_workspace_beta(M, N, workspace, "_KSpipi")
    __ws, __var = get_workspace_beta(M, 0, workspace, "_hh")
else:
    raise NotImplementedError("model undefined")

C, S, K, c, s, k, beta = get_cp_parameters(
    snakemake.input.b2dpipi_parameters_filepath, M, N
)

for i in range(2 * N):
    workspace.var(f"K{i}").setVal(K[i])
    workspace.var(f"K{i}").setConstant(True)
for i in range(2 * M):
    workspace.var(f"k{i}").setVal(k[i])
    workspace.var(f"k{i}").setConstant(True)

workspace.writeToFile(snakemake.output.model_filepath)


# -------- calculate and print debug info --------
print(f"C: {C}")
print(f"S: {S}")
print(f"K: {K}")
print(f"c: {c}")
print(f"s: {s}")
print(f"k: {k}")
print(f"beta: {beta}")
workspace.Print("V")

for i in range(N):
    workspace.var(f"C{i}").setVal(C[i])
    workspace.var(f"S{i}").setVal(S[i])
for i in range(M):
    workspace.var(f"c{i}").setVal(c[i])
    workspace.var(f"s{i}").setVal(s[i])
if snakemake.wildcards.model_name != "model_cos2betasin2beta":
    workspace.var("beta").setVal(beta)
else:
    workspace.var("cos2beta").setVal(ROOT.TMath.Cos(2 * beta))
    workspace.var("sin2beta").setVal(ROOT.TMath.Sin(2 * beta))


def convert_physical_index_to_fully_mapped_programmatic_index(
    physical_index, half_limit
):
    return (
        physical_index + half_limit
        if physical_index < 0
        else physical_index + half_limit - 1
    )


def convert_physical_index_to_half_mapped_programmatic_index(physical_index):
    return -physical_index - 1 if physical_index < 0 else physical_index - 1


C_long = [C[-i - 1] for i in range(-N, 0)] + C
S_long = [-S[-i - 1] for i in range(-N, 0)] + S
c_long = [c[-i - 1] for i in range(-M, 0)] + c
s_long = [-s[-i - 1] for i in range(-M, 0)] + s
sum_of_Uij = 0
sum_of_Dij = 0
sum_of_Fij = 0
sum_of_Uij_true = 0
sum_of_Dij_true = 0
sum_of_Fij_true = 0
for i in list(range(-N, 0)) + list(range(1, N + 1)):
    for j in list(range(-M, 0)) + list(range(1, M + 1)):
        workspace.cat("i").setIndex(i)
        workspace.cat("j").setIndex(j)
        if snakemake.wildcards.model_name == "model_combined":
            Uij = workspace.obj("U_KSpipi").evaluate()
            Dij = workspace.obj("D_KSpipi").evaluate()
            Fij = workspace.obj("F_KSpipi").evaluate()
        else:
            Uij = workspace.obj("U").evaluate()
            Dij = workspace.obj("D").evaluate()
            Fij = workspace.obj("F").evaluate()
        Uij_true = (
            K[convert_physical_index_to_fully_mapped_programmatic_index(-i, N)]
            * k[convert_physical_index_to_fully_mapped_programmatic_index(j, M)]
            + K[convert_physical_index_to_fully_mapped_programmatic_index(i, N)]
            * k[convert_physical_index_to_fully_mapped_programmatic_index(-j, M)]
        ) / 2
        Dij_true = (
            K[convert_physical_index_to_fully_mapped_programmatic_index(-i, N)]
            * k[convert_physical_index_to_fully_mapped_programmatic_index(j, M)]
            - K[convert_physical_index_to_fully_mapped_programmatic_index(i, N)]
            * k[convert_physical_index_to_fully_mapped_programmatic_index(-j, M)]
        ) / 2
        Fij_true = -np.sqrt(
            K[convert_physical_index_to_fully_mapped_programmatic_index(i, N)]
            * K[convert_physical_index_to_fully_mapped_programmatic_index(-i, N)]
            * k[convert_physical_index_to_fully_mapped_programmatic_index(j, M)]
            * k[convert_physical_index_to_fully_mapped_programmatic_index(-j, M)]
        ) * (
            (
                C_long[convert_physical_index_to_fully_mapped_programmatic_index(i, N)]
                * s_long[
                    convert_physical_index_to_fully_mapped_programmatic_index(j, M)
                ]
                - S_long[
                    convert_physical_index_to_fully_mapped_programmatic_index(i, N)
                ]
                * c_long[
                    convert_physical_index_to_fully_mapped_programmatic_index(j, M)
                ]
            )
            * np.cos(2 * beta)
            - (
                C_long[convert_physical_index_to_fully_mapped_programmatic_index(i, N)]
                * c_long[
                    convert_physical_index_to_fully_mapped_programmatic_index(j, M)
                ]
                + S_long[
                    convert_physical_index_to_fully_mapped_programmatic_index(i, N)
                ]
                * s_long[
                    convert_physical_index_to_fully_mapped_programmatic_index(j, M)
                ]
            )
            * np.sin(2 * beta)
        )
        print(
            f"U_{i}_{j} vs U_{i}_{j}_true: {Uij:.10f} vs {Uij_true:.10f} diff {Uij - Uij_true:.10f}"
        )
        print(
            f"D_{i}_{j} vs D_{i}_{j}_true: {Dij:.10f} vs {Dij_true:.10f} diff {Dij - Dij_true:.10f}"
        )
        print(
            f"F_{i}_{j} vs F_{i}_{j}_true: {Fij:.10f} vs {Fij_true:.10f} diff {Fij - Fij_true:.10f}"
        )
        sum_of_Uij += Uij
        sum_of_Dij += Dij
        sum_of_Fij += Fij
        sum_of_Uij_true += Uij_true
        sum_of_Dij_true += Dij_true
        sum_of_Fij_true += Fij_true
print(f"sum of Uij: {sum_of_Uij}")
print(f"sum of Dij: {sum_of_Dij}")
print(f"sum of Fij: {sum_of_Fij}")
print(f"sum of Uij_true: {sum_of_Uij_true}")
print(f"sum of Dij_true: {sum_of_Dij_true}")
print(f"sum of Fij_true: {sum_of_Fij_true}")
# ----------------

tee.stop()
