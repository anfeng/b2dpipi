import os

import pandas as pd
import ROOT
from utils import Tee, get_cp_parameters, plot_workspace

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)

model_file = ROOT.TFile(snakemake.input.model_filepath)
workspace = model_file.Get("w")

toy_data = ROOT.TFile(snakemake.input.toy_filepath[0]).Get("bcpgData")
for toy_filepath in snakemake.input.toy_filepath[1:]:
    toy_data.append(ROOT.TFile(toy_filepath).Get("bcpgData"))
toy_data = ROOT.RooDataSet.from_pandas(
    pd.DataFrame(toy_data.to_numpy())[: int(snakemake.wildcards.event_num)],
    toy_data.get(),
)
print(toy_data.numEntries())

fitresult_file = ROOT.TFile(snakemake.input.fitresult_filepath)
fitresult = fitresult_file.Get("fitresult")

C, S, K, c, s, k, beta = get_cp_parameters(
    snakemake.input.b2dpipi_parameters_filepath, M, N
)
if snakemake.wildcards.c_s_status == "csfixed":
    for i in range(M):
        workspace.var(f"c{i}").setVal(c[i])
        workspace.var(f"c{i}").setConstant(True)
        workspace.var(f"s{i}").setVal(s[i])
        workspace.var(f"s{i}").setConstant(True)
else:
    for i in range(M):
        workspace.var(f"c{i}").setVal(fitresult.floatParsFinal().find(f"c{i}").getVal())
        workspace.var(f"s{i}").setVal(fitresult.floatParsFinal().find(f"s{i}").getVal())
if snakemake.wildcards.C_S_status == "CSfixed":
    for i in range(N):
        workspace.var(f"C{i}").setVal(C[i])
        workspace.var(f"C{i}").setConstant(True)
        workspace.var(f"S{i}").setVal(S[i])
        workspace.var(f"S{i}").setConstant(True)
else:
    for i in range(N):
        workspace.var(f"C{i}").setVal(fitresult.floatParsFinal().find(f"C{i}").getVal())
        workspace.var(f"S{i}").setVal(fitresult.floatParsFinal().find(f"S{i}").getVal())
workspace.var("beta").setVal(fitresult.floatParsFinal().find("beta").getVal())

os.makedirs(f"{snakemake.params.figure_filepath_prefix}/fitresult", exist_ok=True)
plot_workspace(
    workspace,
    toy_data,
    M,
    N,
    f"{snakemake.params.figure_filepath_prefix}/fitresult/fitresult",
    snakemake.params.figure_filepath_suffix,
)

outputdir_nll = f"{snakemake.params.figure_filepath_prefix}/nll/{{fig_name}}{snakemake.params.figure_filepath_suffix}"
os.makedirs(f"{snakemake.params.figure_filepath_prefix}/nll", exist_ok=True)

nll = workspace.pdf("bcpg0").createNLL(
    toy_data, Extended=True, NumCPU=snakemake.threads
)

beta = workspace.var("beta")
frame1 = beta.frame(Bins=20, Range=(0.01, 0.5), Title="LL in beta")
nll.plotOn(frame1, ShiftToZero=True)
# pll_frac = nll.createProfile({beta})
# pll_frac.plotOn(frame1, LineColor="r")
canvas = ROOT.TCanvas()
frame1.Draw()
canvas.SaveAs(outputdir_nll.format(fig_name="nll_beta"))

for i in range(M):
    frame = workspace.var(f"c{i}").frame(Bins=20, Range=(-2, 2), Title=f"LL in c{i}")
    nll.plotOn(frame, ShiftToZero=True)
    canvas = ROOT.TCanvas()
    frame.Draw()
    canvas.SaveAs(outputdir_nll.format(fig_name=f"nll_c{i}"))

    frame = workspace.var(f"s{i}").frame(Bins=20, Range=(-2, 2), Title=f"LL in s{i}")
    nll.plotOn(frame, ShiftToZero=True)
    canvas = ROOT.TCanvas()
    frame.Draw()
    canvas.SaveAs(outputdir_nll.format(fig_name=f"nll_s{i}"))

for i in range(N):
    frame = workspace.var(f"C{i}").frame(Bins=20, Range=(-2, 2), Title=f"LL in C{i}")
    nll.plotOn(frame, ShiftToZero=True)
    canvas = ROOT.TCanvas()
    frame.Draw()
    canvas.SaveAs(outputdir_nll.format(fig_name=f"nll_C{i}"))

    frame = workspace.var(f"S{i}").frame(Bins=20, Range=(-2, 2), Title=f"LL in S{i}")
    nll.plotOn(frame, ShiftToZero=True)
    canvas = ROOT.TCanvas()
    frame.Draw()
    canvas.SaveAs(outputdir_nll.format(fig_name=f"nll_S{i}"))

beta_fitted = fitresult.floatParsFinal().find(f"beta").getVal()
beta_uncertainty = fitresult.floatParsFinal().find(f"beta").getError()
print(f"beta: {beta_fitted} +- {beta_uncertainty}")
print(
    f"beta (degree): {beta_fitted / ROOT.TMath.Pi() * 180} +- {beta_uncertainty / ROOT.TMath.Pi() * 180}"
)
print(
    f"sin(2 * beta): {ROOT.TMath.Sin(2 * beta_fitted)} +- {2 * ROOT.TMath.Cos(2 * beta_fitted) * beta_uncertainty}"
)
print(
    f"cos(2 * beta): {ROOT.TMath.Cos(2 * beta_fitted)} +- {2 * ROOT.TMath.Sin(2 * beta_fitted) * beta_uncertainty}"
)

fitresult_file.Close()
model_file.Close()

tee.stop()
