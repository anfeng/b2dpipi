import math

import matplotlib.patches
import matplotlib.pyplot as plt
import numpy as np
import ROOT
import yaml
from utils import Tee

ROOT.gROOT.SetBatch(True)

tee = Tee(output_filepath=snakemake.log[0])
tee.start()


def copy_th2d_zeroing(src_hist):
    dst_hist = ROOT.TH2D()
    src_hist.Copy(dst_hist)
    for i in range(src_hist.GetXaxis().GetNbins() + 2):
        for j in range(src_hist.GetYaxis().GetNbins() + 2):
            dst_hist.SetBinContent(i, j, 0)
    return dst_hist


def sum_bins(hist, bins):
    return sum([hist.GetBinContent(bin[0], bin[1]) for bin in bins])


def count_bins(hist, bin_content_low, bin_content_high, endpoint=False):
    count = 0
    for i in range(1, hist.GetXaxis().GetNbins() + 1):
        for j in range(1, hist.GetYaxis().GetNbins() + 1):
            bin_content = hist.GetBinContent(i, j)
            if endpoint:
                if bin_content >= bin_content_low and bin_content <= bin_content_high:
                    count += 1
            else:
                if bin_content >= bin_content_low and bin_content < bin_content_high:
                    count += 1
    return count


def adaptive_z_binning(hist, z_bin_number, num=1000):
    total_bin_number = hist.GetXaxis().GetNbins() * hist.GetYaxis().GetNbins()
    bin_number_per_z_bin = total_bin_number / z_bin_number
    z_bin_edges = [hist.GetMinimum()]
    current_index = 0

    excluded_bin_number = count_bins(
        hist,
        hist.GetMinimum(),
        hist.GetMinimum() + (hist.GetMaximum() - hist.GetMinimum()) / (num - 1),
    )
    if excluded_bin_number > bin_number_per_z_bin:
        bin_number_per_z_bin = (total_bin_number - excluded_bin_number) / (
            z_bin_number - 1
        )
        z_bin_edges.append(
            hist.GetMinimum() + (hist.GetMaximum() - hist.GetMinimum()) / (num - 1)
        )
        current_index += 1

    for z_bin_edge in np.linspace(hist.GetMinimum(), hist.GetMaximum(), num):
        if (
            count_bins(hist, z_bin_edges[current_index], z_bin_edge)
            >= bin_number_per_z_bin
        ):
            z_bin_edges.append(z_bin_edge)
            current_index += 1
    if current_index < z_bin_number:
        z_bin_edges.append(hist.GetMaximum())

    return z_bin_edges


def symmetric_rebinning_by_z_bins(hist, z_bin_edges):
    new_bins_up = [[] for _ in range(len(z_bin_edges) - 1)]
    new_bins_down = [[] for _ in range(len(z_bin_edges) - 1)]
    for i in range(1, hist.GetXaxis().GetNbins() + 1):
        for j in range(i + 1, hist.GetYaxis().GetNbins() + 1):
            bin_content = hist.GetBinContent(i, j)
            for k in range(1, len(z_bin_edges)):
                if (bin_content > z_bin_edges[k - 1]) and (
                    bin_content <= z_bin_edges[k]
                ):
                    new_bins_up[k - 1].append([i, j])
                    new_bins_down[k - 1].append([j, i])
                    break
    return new_bins_up, new_bins_down


def plot_cs_parameters(c, s, output_filepath, xlabel="c", ylabel="s"):
    plt.figure()
    figure, axes = plt.subplots()
    plt.errorbar(c, s, fmt="bo")
    axes.add_artist(
        matplotlib.patches.Circle((0, 0), 1, fill=False, linestyle="--", color="r")
    )
    plt.grid(which="both")
    for i in range(len(c)):
        plt.annotate(
            f"{i + 1}", (c[i], s[i]), xytext=(0.5, 0.5), textcoords="offset fontsize"
        )
    plt.xlim(-1.2, 1.2)
    plt.ylim(-1.2, 1.2)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.minorticks_on()
    # plt.tick_params(axis='both', which='major', width=0.5)
    # plt.tick_params(axis='both', which='minor', width=0.1)
    # plt.xticks(np.arange(-1.2, 1.2, 0.1))
    # plt.yticks(np.arange(-1.2, 1.2, 0.1))
    axes.set_aspect("equal")
    plt.savefig(output_filepath)


def plot_k_parameters(
    k, km, output_filepath, xlabel="absolute value of bin index", ylabel="k", index="j"
):
    plt.figure()
    plt.errorbar(np.arange(1, len(k) + 1), k, fmt="bo", label=f"{index} > 0")
    plt.errorbar(np.arange(1, len(km) + 1), km, fmt="ro", label=f"{index} < 0")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.savefig(output_filepath)


def rebin_th2(hist, config):
    hist_new = ROOT.TH2D(*config)
    for index_x in range(1, hist_new.GetNbinsX() + 1):
        for index_y in range(1, hist_new.GetNbinsY() + 1):
            hist_new.SetBinContent(index_x, index_y, 0)

    for index_x in range(1, hist.GetNbinsX() + 1):
        for index_y in range(1, hist.GetNbinsY() + 1):
            coord_x = hist.GetXaxis().GetBinCenter(index_x)
            coord_y = hist.GetYaxis().GetBinCenter(index_y)
            hist_new.AddBinContent(
                hist_new.FindBin(coord_x, coord_y), hist.GetBinContent(index_x, index_y)
            )

    return hist_new


# ---------- read amplitudes ----------
f = ROOT.TFile(snakemake.input[0])

hist_a_re = f.Get("hist_a_re")
hist_a_im = f.Get("hist_a_im")
hist_abar_re = f.Get("hist_abar_re")
hist_abar_im = f.Get("hist_abar_im")

# ---------- rebin amplitudes ----------
binning_config = (
    "hist",
    "hist",
    hist_a_re.GetNbinsX(),
    2.1**2,
    hist_a_re.GetXaxis().GetBinLowEdge(hist_a_re.GetNbinsX() + 1),
    hist_a_re.GetNbinsY(),
    2.1**2,
    hist_a_re.GetYaxis().GetBinLowEdge(hist_a_re.GetNbinsY() + 1),
)
hist_a_re = rebin_th2(hist_a_re, binning_config)
hist_a_im = rebin_th2(hist_a_im, binning_config)
hist_abar_re = rebin_th2(hist_abar_re, binning_config)
hist_abar_im = rebin_th2(hist_abar_im, binning_config)

# ---------- generate histograms ----------
hist_a_sq = copy_th2d_zeroing(hist_a_re)
hist_a_sqji = copy_th2d_zeroing(hist_a_re)
hist_abar_sq = copy_th2d_zeroing(hist_a_re)
hist_arg_diff = copy_th2d_zeroing(hist_a_re)
hist_abs_arg_diff = copy_th2d_zeroing(hist_a_re)
hist_a_reij_reji = copy_th2d_zeroing(hist_a_re)
hist_a_imij_imji = copy_th2d_zeroing(hist_a_re)
hist_a_reij_imji = copy_th2d_zeroing(hist_a_re)
hist_a_reji_imij = copy_th2d_zeroing(hist_a_re)
for i in range(1, hist_a_re.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_a_re.GetYaxis().GetNbins() + 1):
        hist_a_sq.SetBinContent(
            i,
            j,
            hist_a_re.GetBinContent(i, j) ** 2 + hist_a_im.GetBinContent(i, j) ** 2,
        )
        hist_a_sqji.SetBinContent(
            i,
            j,
            hist_a_re.GetBinContent(j, i) ** 2 + hist_a_im.GetBinContent(j, i) ** 2,
        )
        hist_abar_sq.SetBinContent(
            i,
            j,
            hist_abar_re.GetBinContent(i, j) ** 2
            + hist_abar_im.GetBinContent(i, j) ** 2,
        )
        hist_arg_diff.SetBinContent(
            i,
            j,
            (
                math.atan2(
                    hist_abar_im.GetBinContent(i, j), hist_abar_re.GetBinContent(i, j)
                )
                - math.atan2(
                    hist_a_im.GetBinContent(i, j), hist_a_re.GetBinContent(i, j)
                )
            )
            % (2 * np.pi),
        )
        hist_abs_arg_diff.SetBinContent(
            i,
            j,
            abs(
                math.atan2(hist_a_im.GetBinContent(i, j), hist_a_re.GetBinContent(i, j))
                - math.atan2(
                    hist_abar_im.GetBinContent(i, j), hist_abar_re.GetBinContent(i, j)
                )
            ),
        )
        hist_a_reij_reji.SetBinContent(
            i, j, hist_a_re.GetBinContent(i, j) * hist_a_re.GetBinContent(j, i)
        )
        hist_a_imij_imji.SetBinContent(
            i, j, hist_a_im.GetBinContent(i, j) * hist_a_im.GetBinContent(j, i)
        )
        hist_a_reij_imji.SetBinContent(
            i, j, hist_a_re.GetBinContent(i, j) * hist_a_im.GetBinContent(j, i)
        )
        hist_a_reji_imij.SetBinContent(
            i, j, hist_a_re.GetBinContent(j, i) * hist_a_im.GetBinContent(i, j)
        )

c = ROOT.TCanvas()
hist_a_sq.SetStats(False)
hist_a_sq.Draw("colz")
# hist_a_sq.SetMinimum(hist_a_sq.GetMinimum(0.))
# c.SetLogz()
c.SaveAs(snakemake.output.hist_a_sq_filepath)

c = ROOT.TCanvas()
hist_abar_sq.SetStats(False)
hist_abar_sq.Draw("colz")
c.SaveAs(snakemake.output.hist_abar_sq_filepath)

c = ROOT.TCanvas()
hist_arg_diff.SetStats(False)
hist_arg_diff.SetContour(16)
hist_arg_diff.Draw("colz")
c.SaveAs(snakemake.output.hist_arg_diff_filepath)

c = ROOT.TCanvas()
hist_abs_arg_diff.SetStats(False)
hist_abs_arg_diff.SetContour(16)
hist_abs_arg_diff.Draw("colz")
c.SaveAs(snakemake.output.hist_abs_arg_diff_filepath)

# ---------- check pippim_masses ----------
m_B = 5.27966
m_D = 1.86484
m_pip = 0.13957
m_pim = 0.13957
Dpip_masses_squared = np.array(
    [
        hist_a_sq.GetXaxis().GetBinLowEdge(i)
        for i in range(1, hist_a_sq.GetXaxis().GetNbins() + 2)
    ]
)
Dpim_masses_squared = np.array(
    [
        hist_a_sq.GetYaxis().GetBinLowEdge(i)
        for i in range(1, hist_a_sq.GetYaxis().GetNbins() + 2)
    ]
)
pippim_masses_squared = (
    m_B**2 + m_D**2 + m_pip**2 + m_pim**2 - Dpip_masses_squared - Dpim_masses_squared
)
print()
print("---------- check pippim_masses ----------")
print(f"Dpip_masses_squared: {Dpip_masses_squared}")
print(f"Dpim_masses_squared: {Dpim_masses_squared}")
print(f"pippim_masses_squared: {pippim_masses_squared}")
print()

# ---------- generate hist_a_sq_Dpip_pippim ----------
hist_a_sq_Dpip_pippim = ROOT.TH2D(
    "hist_a_sq_Dpip_pippim",
    "hist_a_sq_Dpip_pippim;m^{2}(#pi^{+}#pi^{-})(GeV^{2}/c^{4});m^{2}(#bar{D}^{0}#pi^{+})(GeV^{2}/c^{4})",
    100,
    0,
    12,
    100,
    3,
    27,
)
for i in range(1, hist_a_sq_Dpip_pippim.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_a_sq_Dpip_pippim.GetYaxis().GetNbins() + 1):
        pippim_mass_squared = hist_a_sq_Dpip_pippim.GetXaxis().GetBinCenter(i)
        Dpip_mass_squared = hist_a_sq_Dpip_pippim.GetYaxis().GetBinCenter(j)
        Dpim_mass_squared = (
            m_B**2
            + m_D**2
            + m_pip**2
            + m_pim**2
            - Dpip_mass_squared
            - pippim_mass_squared
        )
        if (
            (Dpim_mass_squared > hist_a_sq.GetYaxis().GetXmin())
            and (Dpim_mass_squared < hist_a_sq.GetYaxis().GetXmax())
            and (Dpip_mass_squared > hist_a_sq.GetXaxis().GetXmin())
            and (Dpip_mass_squared < hist_a_sq.GetXaxis().GetXmax())
        ):
            hist_a_sq_Dpip_pippim.SetBinContent(
                i, j, hist_a_sq.Interpolate(Dpip_mass_squared, Dpim_mass_squared)
            )
c = ROOT.TCanvas()
hist_a_sq_Dpip_pippim.SetStats(False)
hist_a_sq_Dpip_pippim.Draw("colz")
hist_a_sq_Dpip_pippim.SetMinimum(hist_a_sq_Dpip_pippim.GetMinimum(0.0))
c.SetLogz()
c.SaveAs(snakemake.output.hist_a_sq_Dpip_pippim_filepath)

# ---------- generate hist_a_sq_Dpip_pippim_pippimprojection ----------
hist_a_sq_Dpip_pippim_pippimprojection = ROOT.TH1D(
    "hist_a_sq_Dpip_pippim_pippimprojection",
    "hist_a_sq_Dpip_pippim_pippimprojection;m(#pi^{+}#pi^{-})(GeV/c^{2})",
    50,
    0,
    3.5,
)
for i in range(1, hist_a_sq_Dpip_pippim_pippimprojection.GetXaxis().GetNbins() + 1):
    hist_a_sq_Dpip_pippim_pippimprojection.SetBinContent(
        i,
        hist_a_sq_Dpip_pippim.ProjectionX().Interpolate(
            hist_a_sq_Dpip_pippim_pippimprojection.GetXaxis().GetBinCenter(i) ** 2
        )
        / hist_a_sq_Dpip_pippim.ProjectionX().Integral("width")
        * (2 * hist_a_sq_Dpip_pippim_pippimprojection.GetXaxis().GetBinCenter(i))
        * hist_a_sq_Dpip_pippim_pippimprojection.GetXaxis().GetBinWidth(1),
    )
print()
print("---------- generate hist_a_sq_Dpip_pippim_pippimprojection ----------")
print(
    f"integral of hist_a_sq_Dpip_pippim_pippimprojection_pippimsquared: {hist_a_sq_Dpip_pippim.ProjectionX().Integral()}"
)
print(
    f"integral of hist_a_sq_Dpip_pippim_pippimprojection: {hist_a_sq_Dpip_pippim_pippimprojection.Integral()}"
)
print()
c = ROOT.TCanvas()
hist_a_sq_Dpip_pippim_pippimprojection.SetStats(False)
hist_a_sq_Dpip_pippim_pippimprojection.Draw()
c.SaveAs(snakemake.output.hist_a_sq_Dpip_pippim_pippimprojection_filepath)

# ---------- generate hist_a_sq_Dpip_pippim_Dpipprojection ----------
hist_a_sq_Dpip_pippim_Dpipprojection = ROOT.TH1D(
    "hist_a_sq_Dpip_pippim_Dpipprojection",
    "hist_a_sq_Dpip_pippim_Dpipprojection;m(#bar{D}^{0}#pi^{+})(GeV/c^{2})",
    50,
    1.8,
    5.3,
)
for i in range(1, hist_a_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetNbins() + 1):
    hist_a_sq_Dpip_pippim_Dpipprojection.SetBinContent(
        i,
        hist_a_sq_Dpip_pippim.ProjectionY().Interpolate(
            hist_a_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetBinCenter(i) ** 2
        )
        / hist_a_sq_Dpip_pippim.ProjectionY().Integral("width")
        * (2 * hist_a_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetBinCenter(i))
        * hist_a_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetBinWidth(1),
    )
print()
print("---------- generate hist_a_sq_Dpip_pippim_Dpipprojection ----------")
print(
    f"integral of hist_a_sq_Dpip_pippim_Dpipprojection_Dpisquared: {hist_a_sq_Dpip_pippim.ProjectionX().Integral()}"
)
print(
    f"integral of hist_a_sq_Dpip_pippim_Dpipprojection: {hist_a_sq_Dpip_pippim_Dpipprojection.Integral()}"
)
print()
c = ROOT.TCanvas()
hist_a_sq_Dpip_pippim_Dpipprojection.SetStats(False)
hist_a_sq_Dpip_pippim_Dpipprojection.Draw()
c.SaveAs(snakemake.output.hist_a_sq_Dpip_pippim_Dpipprojection_filepath)

# ---------- generate hist_a_sq_Dpim_pippim ----------
hist_a_sq_Dpim_pippim = ROOT.TH2D(
    "hist_a_sq_Dpim_pippim",
    "hist_a_sq_Dpim_pippim;m^{2}(#pi^{+}#pi^{-})(GeV^{2}/c^{4});m^{2}(#bar{D}^{0}#pi^{-})(GeV^{2}/c^{4})",
    100,
    0,
    12,
    100,
    3,
    27,
)
for i in range(1, hist_a_sq_Dpim_pippim.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_a_sq_Dpim_pippim.GetYaxis().GetNbins() + 1):
        pippim_mass_squared = hist_a_sq_Dpim_pippim.GetXaxis().GetBinCenter(i)
        Dpim_mass_squared = hist_a_sq_Dpim_pippim.GetYaxis().GetBinCenter(j)
        Dpip_mass_squared = (
            m_B**2
            + m_D**2
            + m_pip**2
            + m_pim**2
            - Dpim_mass_squared
            - pippim_mass_squared
        )
        if (
            (Dpim_mass_squared > hist_a_sq.GetYaxis().GetXmin())
            and (Dpim_mass_squared < hist_a_sq.GetYaxis().GetXmax())
            and (Dpip_mass_squared > hist_a_sq.GetXaxis().GetXmin())
            and (Dpip_mass_squared < hist_a_sq.GetXaxis().GetXmax())
        ):
            hist_a_sq_Dpim_pippim.SetBinContent(
                i, j, hist_a_sq.Interpolate(Dpip_mass_squared, Dpim_mass_squared)
            )
c = ROOT.TCanvas()
hist_a_sq_Dpim_pippim.SetStats(False)
hist_a_sq_Dpim_pippim.Draw("colz")
hist_a_sq_Dpim_pippim.SetMinimum(hist_a_sq_Dpim_pippim.GetMinimum(0.0))
c.SetLogz()
c.SaveAs(snakemake.output.hist_a_sq_Dpim_pippim_filepath)

# ---------- generate hist_a_sq_Dpim_pippim_Dpimprojection ----------
hist_a_sq_Dpim_pippim_Dpimprojection = ROOT.TH1D(
    "hist_a_sq_Dpim_pippim_Dpimprojection",
    "hist_a_sq_Dpim_pippim_Dpimprojection;m(#bar{D}^{0}#pi^{-})(GeV/c^{2})",
    50,
    1.8,
    5.3,
)
for i in range(1, hist_a_sq_Dpim_pippim_Dpimprojection.GetXaxis().GetNbins() + 1):
    hist_a_sq_Dpim_pippim_Dpimprojection.SetBinContent(
        i,
        hist_a_sq_Dpim_pippim.ProjectionY().Interpolate(
            hist_a_sq_Dpim_pippim_Dpimprojection.GetXaxis().GetBinCenter(i) ** 2
        )
        / hist_a_sq_Dpim_pippim.ProjectionY().Integral("width")
        * (2 * hist_a_sq_Dpim_pippim_Dpimprojection.GetXaxis().GetBinCenter(i))
        * hist_a_sq_Dpim_pippim_Dpimprojection.GetXaxis().GetBinWidth(1),
    )
print()
print("---------- generate hist_a_sq_Dpim_pippim_Dpimprojection ----------")
print(
    f"integral of hist_a_sq_Dpim_pippim_Dpimprojection_Dpisquared: {hist_a_sq_Dpim_pippim.ProjectionX().Integral()}"
)
print(
    f"integral of hist_a_sq_Dpim_pippim_Dpimprojection: {hist_a_sq_Dpim_pippim_Dpimprojection.Integral()}"
)
print()
c = ROOT.TCanvas()
hist_a_sq_Dpim_pippim_Dpimprojection.SetStats(False)
hist_a_sq_Dpim_pippim_Dpimprojection.Draw()
c.SaveAs(snakemake.output.hist_a_sq_Dpim_pippim_Dpimprojection_filepath)

# ---------- generate hist_abs_arg_diff_diff ----------
hist_abs_arg_diff_diff = copy_th2d_zeroing(hist_a_re)
for i in range(1, hist_a_re.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_a_re.GetYaxis().GetNbins() + 1):
        hist_abs_arg_diff_diff.SetBinContent(
            i,
            j,
            hist_abs_arg_diff.GetBinContent(i, j)
            - hist_abs_arg_diff.GetBinContent(j, i),
        )
print()
print("---------- generate hist_abs_arg_diff_diff ----------")
print(f"abs_arg_diff_diff_max: {hist_abs_arg_diff_diff.GetMaximum()}")
print()
c = ROOT.TCanvas()
hist_abs_arg_diff_diff.SetStats(False)
hist_abs_arg_diff_diff.Draw("colz")
c.SaveAs(snakemake.output.hist_abs_arg_diff_diff_filepath)

# ---------- generate hist_abar_sq_Dpip_pippim ----------
hist_abar_sq_Dpip_pippim = ROOT.TH2D(
    "hist_abar_sq_Dpip_pippim",
    "hist_abar_sq_Dpip_pippim;m^{2}(#pi^{+}#pi^{-})(GeV^{2}/c^{4});m^{2}(D^{0}#pi^{+})(GeV^{2}/c^{4})",
    100,
    0,
    12,
    100,
    3,
    27,
)
for i in range(1, hist_abar_sq_Dpip_pippim.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_abar_sq_Dpip_pippim.GetYaxis().GetNbins() + 1):
        pippim_mass_squared = hist_abar_sq_Dpip_pippim.GetXaxis().GetBinCenter(i)
        Dpip_mass_squared = hist_abar_sq_Dpip_pippim.GetYaxis().GetBinCenter(j)
        Dpim_mass_squared = (
            m_B**2
            + m_D**2
            + m_pip**2
            + m_pim**2
            - Dpip_mass_squared
            - pippim_mass_squared
        )
        if (
            (Dpim_mass_squared > hist_abar_sq.GetYaxis().GetXmin())
            and (Dpim_mass_squared < hist_abar_sq.GetYaxis().GetXmax())
            and (Dpip_mass_squared > hist_abar_sq.GetXaxis().GetXmin())
            and (Dpip_mass_squared < hist_abar_sq.GetXaxis().GetXmax())
        ):
            hist_abar_sq_Dpip_pippim.SetBinContent(
                i, j, hist_abar_sq.Interpolate(Dpip_mass_squared, Dpim_mass_squared)
            )
c = ROOT.TCanvas()
hist_abar_sq_Dpip_pippim.SetStats(False)
hist_abar_sq_Dpip_pippim.SetTitle("")
hist_abar_sq_Dpip_pippim.Draw("colz")
hist_abar_sq_Dpip_pippim.SetMinimum(hist_abar_sq_Dpip_pippim.GetMinimum(0.0))
c.SetLogz()
c.SaveAs(snakemake.output.hist_abar_sq_Dpip_pippim_filepath)

# ---------- generate hist_abar_sq_Dpip_pippim_pippimprojection ----------
hist_abar_sq_Dpip_pippim_pippimprojection = ROOT.TH1D(
    "hist_abar_sq_Dpip_pippim_pippimprojection",
    "hist_abar_sq_Dpip_pippim_pippimprojection;m(#pi^{+}#pi^{-})(GeV/c^{2})",
    50,
    0,
    3.5,
)
for i in range(1, hist_abar_sq_Dpip_pippim_pippimprojection.GetXaxis().GetNbins() + 1):
    hist_abar_sq_Dpip_pippim_pippimprojection.SetBinContent(
        i,
        hist_abar_sq_Dpip_pippim.ProjectionX().Interpolate(
            hist_abar_sq_Dpip_pippim_pippimprojection.GetXaxis().GetBinCenter(i) ** 2
        )
        / hist_abar_sq_Dpip_pippim.ProjectionX().Integral("width")
        * (2 * hist_abar_sq_Dpip_pippim_pippimprojection.GetXaxis().GetBinCenter(i))
        * hist_abar_sq_Dpip_pippim_pippimprojection.GetXaxis().GetBinWidth(1),
    )
print()
print("---------- generate hist_abar_sq_Dpip_pippim_pippimprojection ----------")
print(
    f"integral of hist_abar_sq_Dpip_pippim_pippimprojection_pippimsquared: {hist_abar_sq_Dpip_pippim.ProjectionX().Integral()}"
)
print(
    f"integral of hist_abar_sq_Dpip_pippim_pippimprojection: {hist_abar_sq_Dpip_pippim_pippimprojection.Integral()}"
)
print()
c = ROOT.TCanvas()
hist_abar_sq_Dpip_pippim_pippimprojection.SetStats(False)
hist_abar_sq_Dpip_pippim_pippimprojection.Draw()
c.SaveAs(snakemake.output.hist_abar_sq_Dpip_pippim_pippimprojection_filepath)

# ---------- generate hist_abar_sq_Dpip_pippim_Dpipprojection ----------
hist_abar_sq_Dpip_pippim_Dpipprojection = ROOT.TH1D(
    "hist_abar_sq_Dpip_pippim_Dpipprojection",
    "hist_abar_sq_Dpip_pippim_Dpipprojection;m(D^{0}#pi^{+})(GeV/c^{2})",
    50,
    1.8,
    5.3,
)
for i in range(1, hist_abar_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetNbins() + 1):
    hist_abar_sq_Dpip_pippim_Dpipprojection.SetBinContent(
        i,
        hist_abar_sq_Dpip_pippim.ProjectionY().Interpolate(
            hist_abar_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetBinCenter(i) ** 2
        )
        / hist_abar_sq_Dpip_pippim.ProjectionY().Integral("width")
        * (2 * hist_abar_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetBinCenter(i))
        * hist_abar_sq_Dpip_pippim_Dpipprojection.GetXaxis().GetBinWidth(1),
    )
print()
print("---------- generate hist_abar_sq_Dpip_pippim_Dpipprojection ----------")
print(
    f"integral of hist_abar_sq_Dpip_pippim_Dpipprojection_Dpisquared: {hist_abar_sq_Dpip_pippim.ProjectionX().Integral()}"
)
print(
    f"integral of hist_abar_sq_Dpip_pippim_Dpipprojection: {hist_abar_sq_Dpip_pippim_Dpipprojection.Integral()}"
)
print()
c = ROOT.TCanvas()
hist_abar_sq_Dpip_pippim_Dpipprojection.SetStats(False)
hist_abar_sq_Dpip_pippim_Dpipprojection.Draw()
c.SaveAs(snakemake.output.hist_abar_sq_Dpip_pippim_Dpipprojection_filepath)

# ---------- generate hist_abar_sq_Dpip_pippim_withpippimcut ----------
hist_abar_sq_Dpip_pippim_withpippimcut = copy_th2d_zeroing(hist_abar_sq_Dpip_pippim)
for i in range(1, hist_abar_sq_Dpip_pippim_withpippimcut.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_abar_sq_Dpip_pippim_withpippimcut.GetYaxis().GetNbins() + 1):
        if hist_abar_sq_Dpip_pippim_withpippimcut.GetXaxis().GetBinCenter(i) > 1.6**2:
            hist_abar_sq_Dpip_pippim_withpippimcut.SetBinContent(
                i, j, hist_abar_sq_Dpip_pippim.GetBinContent(i, j)
            )
c = ROOT.TCanvas()
hist_abar_sq_Dpip_pippim_withpippimcut.SetStats(False)
hist_abar_sq_Dpip_pippim_withpippimcut.Draw("colz")
hist_abar_sq_Dpip_pippim_withpippimcut.SetMinimum(
    hist_abar_sq_Dpip_pippim_withpippimcut.GetMinimum(0.0)
)
c.SetLogz()
c.SaveAs(snakemake.output.hist_abar_sq_Dpip_pippim_withpippimcut_filepath)

# ---------- generate hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection ----------
hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection = ROOT.TH1D(
    "hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection",
    "hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection;m(D^{0}#pi^{+})(GeV/c^{2})",
    100,
    2,
    3,
)
for i in range(
    1, hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.GetXaxis().GetNbins() + 1
):
    hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.SetBinContent(
        i,
        hist_abar_sq_Dpip_pippim_withpippimcut.ProjectionY().Interpolate(
            hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.GetXaxis().GetBinCenter(
                i
            )
            ** 2
        )
        / hist_abar_sq_Dpip_pippim_withpippimcut.ProjectionY().Integral("width")
        * (
            2
            * hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.GetXaxis().GetBinCenter(
                i
            )
        )
        * hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.GetXaxis().GetBinWidth(
            1
        ),
    )
print()
print(
    "---------- generate hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection ----------"
)
print(
    f"integral of hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection_Dpisquared: {hist_abar_sq_Dpip_pippim_withpippimcut.ProjectionY().Integral()}"
)
print(
    f"integral of hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection: {hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.Integral()}"
)
print()
c = ROOT.TCanvas()
hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.SetStats(False)
hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.SetTitle("")
hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection.Draw()
c.SaveAs(
    snakemake.output.hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection_filepath
)

# ---------- generate hist_abar_sq_Dpip_pippim_withDpipcut ----------
hist_abar_sq_Dpip_pippim_withDpipcut = copy_th2d_zeroing(hist_abar_sq_Dpip_pippim)
for i in range(1, hist_abar_sq_Dpip_pippim_withDpipcut.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_abar_sq_Dpip_pippim_withDpipcut.GetYaxis().GetNbins() + 1):
        if hist_abar_sq_Dpip_pippim_withDpipcut.GetYaxis().GetBinCenter(j) > 3**2:
            hist_abar_sq_Dpip_pippim_withDpipcut.SetBinContent(
                i, j, hist_abar_sq_Dpip_pippim.GetBinContent(i, j)
            )
c = ROOT.TCanvas()
hist_abar_sq_Dpip_pippim_withDpipcut.SetStats(False)
hist_abar_sq_Dpip_pippim_withDpipcut.Draw("colz")
hist_abar_sq_Dpip_pippim_withDpipcut.SetMinimum(
    hist_abar_sq_Dpip_pippim_withDpipcut.GetMinimum(0.0)
)
c.SetLogz()
c.SaveAs(snakemake.output.hist_abar_sq_Dpip_pippim_withDpipcut_filepath)

# ---------- generate hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection ----------
hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection = ROOT.TH1D(
    "hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection",
    "hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection;m(#pi^{+}#pi^{-})(GeV/c^{2})",
    100,
    0.2,
    1.6,
)
for i in range(
    1, hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.GetXaxis().GetNbins() + 1
):
    hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.SetBinContent(
        i,
        hist_abar_sq_Dpip_pippim_withDpipcut.ProjectionX().Interpolate(
            hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.GetXaxis().GetBinCenter(
                i
            )
            ** 2
        )
        / hist_abar_sq_Dpip_pippim_withDpipcut.ProjectionX().Integral("width")
        * (
            2
            * hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.GetXaxis().GetBinCenter(
                i
            )
        )
        * hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.GetXaxis().GetBinWidth(
            1
        ),
    )
print()
print(
    "---------- generate hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection ----------"
)
print(
    f"integral of hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection_Dpisquared: {hist_abar_sq_Dpip_pippim_withDpipcut.ProjectionX().Integral()}"
)
print(
    f"integral of hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection: {hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.Integral()}"
)
print()
c = ROOT.TCanvas()
hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.SetStats(False)
hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.SetTitle("")
hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection.Draw()
c.SaveAs(
    snakemake.output.hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection_filepath
)

# ---------- calculate arg diff bin edges ----------
print()
print("---------- calculate arg diff bin edges ----------")
print(f"abs_arg_diff_min: {hist_abs_arg_diff.GetMinimum()}")
print(f"abs_arg_diff_max: {hist_abs_arg_diff.GetMaximum()}")
arg_diff_bin_edges = np.linspace(0, 2 * np.pi, int(snakemake.wildcards.M) + 1)
# arg_diff_bin_edges = adaptive_z_binning(hist_abs_arg_diff, int(snakemake.wildcards.M))
print(
    f"arg_diff_bin_edges: 2pi * {[arg_diff_bin_edge / (2 * np.pi) for arg_diff_bin_edge in arg_diff_bin_edges]}"
)
print()

# ---------- binning ----------
print()
print("---------- binning ----------")
dalitz_plot_bins_up, dalitz_plot_bins_down = symmetric_rebinning_by_z_bins(
    hist_arg_diff, arg_diff_bin_edges
)
numbers_of_subbins_of_dalitz_plot_bin_up = [
    len(dalitz_plot_bin_up) for dalitz_plot_bin_up in dalitz_plot_bins_up
]
print(
    f"numbers of sub-bins of dalitz_plot_bin_up: {numbers_of_subbins_of_dalitz_plot_bin_up}"
)
print(
    f"total number of sub-bins of dalitz_plot_bin_up: {sum(numbers_of_subbins_of_dalitz_plot_bin_up)}"
)
numbers_of_subbins_of_dalitz_plot_bin_down = [
    len(dalitz_plot_bin_down) for dalitz_plot_bin_down in dalitz_plot_bins_down
]
print(
    f"numbers of sub-bins of dalitz_plot_bin_down: {numbers_of_subbins_of_dalitz_plot_bin_down}"
)
print(
    f"total number of sub-bins of dalitz_plot_bin_down: {sum(numbers_of_subbins_of_dalitz_plot_bin_down)}"
)
arg_diff_of_one_bin = [
    (
        (
            math.atan2(
                hist_abar_im.GetBinContent(i, j), hist_abar_re.GetBinContent(i, j)
            )
            - math.atan2(hist_a_im.GetBinContent(i, j), hist_a_re.GetBinContent(i, j))
        )
        % (2 * np.pi)
    )
    / (2 * np.pi)
    for i, j in dalitz_plot_bins_up[1]
]
print(f"arg_diff_of_one_bin_min: 2pi * {min(arg_diff_of_one_bin)}")
print(f"arg_diff_of_one_bin_max: 2pi * {max(arg_diff_of_one_bin)}")
print()

# ---------- generate hist_binning ----------
hist_binning = copy_th2d_zeroing(hist_a_re)
# for i in range(1, hist_a_re.GetXaxis().GetNbins() + 1):
#     for j in range(1, hist_a_re.GetYaxis().GetNbins() + 1):
#         hist_binning.SetBinContent(i, j, 0)
for i, dalitz_plot_bin in enumerate(dalitz_plot_bins_up):
    for bin in dalitz_plot_bin:
        hist_binning.SetBinContent(bin[0], bin[1], i + 1)
for i, dalitz_plot_bin in enumerate(dalitz_plot_bins_down):
    for bin in dalitz_plot_bin:
        hist_binning.SetBinContent(bin[0], bin[1], i + 1)
c = ROOT.TCanvas()
hist_binning.SetStats(False)
ROOT.gStyle.SetPalette(8, np.array([867, 1, 608, 2, 8, 390, 4, 5], dtype=np.int32))
hist_binning.SetContour(9, np.array([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5]))
hist_binning.Draw("colz")
c.SaveAs(snakemake.output.hist_binning_filepath)

f_out = ROOT.TFile(snakemake.output["hist_binning_rootfilepath"], "recreate")
hist_binning.Write()
f_out.Close()

# ---------- generate hist_binning_Dpip_pippim ----------
hist_binning_Dpip_pippim = ROOT.TH2D(
    "hist_binning_Dpip_pippim",
    "hist_binning_Dpip_pippim;m^{2}(#pi^{+}#pi^{-})(GeV^{2}/c^{4});m^{2}(D#pi^{+})(GeV^{2}/c^{4})",
    100,
    0,
    12,
    100,
    3,
    27,
)
for i in range(1, hist_binning_Dpip_pippim.GetXaxis().GetNbins() + 1):
    for j in range(1, hist_binning_Dpip_pippim.GetYaxis().GetNbins() + 1):
        if (i % 10 == 0) and (j % 10 == 0):
            print(f"filling bin {i=} {j=}")
        pippim_mass_squared = hist_binning_Dpip_pippim.GetXaxis().GetBinCenter(i)
        Dpip_mass_squared = hist_binning_Dpip_pippim.GetYaxis().GetBinCenter(j)
        Dpim_mass_squared = (
            m_B**2
            + m_D**2
            + m_pip**2
            + m_pim**2
            - Dpip_mass_squared
            - pippim_mass_squared
        )
        if Dpim_mass_squared >= 0:
            if Dpim_mass_squared >= Dpip_mass_squared:
                dalitz_plot_bins_for_search = dalitz_plot_bins_up
            else:
                dalitz_plot_bins_for_search = dalitz_plot_bins_down
            found = False
            for k, dalitz_plot_bin in enumerate(dalitz_plot_bins_for_search):
                for bin in dalitz_plot_bin:
                    if hist_a_re.GetBin(bin[0], bin[1]) == hist_a_re.FindBin(
                        Dpip_mass_squared, Dpim_mass_squared
                    ):
                        hist_binning_Dpip_pippim.SetBinContent(i, j, k + 1)
                        found = True
                        break
                if found:
                    break
c = ROOT.TCanvas()
hist_binning_Dpip_pippim.SetStats(False)
hist_binning_Dpip_pippim.SetContour(
    9, np.array([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5])
)
hist_binning_Dpip_pippim.Draw("colz")
c.SaveAs(snakemake.output.hist_binning_Dpip_pippim_filepath)

# ---------- calculate CP parameters and plot ----------
ki = [
    sum_bins(hist_a_sq, dalitz_plot_bin_up) / hist_a_sq.Integral()
    for dalitz_plot_bin_up in dalitz_plot_bins_up
]
kmi = [
    sum_bins(hist_a_sq, dalitz_plot_bin_down) / hist_a_sq.Integral()
    for dalitz_plot_bin_down in dalitz_plot_bins_down
]
ci = [
    (
        sum_bins(hist_a_reij_reji, dalitz_plot_bin_up)
        + sum_bins(hist_a_imij_imji, dalitz_plot_bin_up)
    )
    / (
        np.sqrt(sum_bins(hist_a_sq, dalitz_plot_bin_up))
        * np.sqrt(sum_bins(hist_a_sqji, dalitz_plot_bin_up))
    )
    for dalitz_plot_bin_up in dalitz_plot_bins_up
]
si = [
    (
        sum_bins(hist_a_reij_imji, dalitz_plot_bin_up)
        - sum_bins(hist_a_reji_imij, dalitz_plot_bin_up)
    )
    / (
        np.sqrt(sum_bins(hist_a_sq, dalitz_plot_bin_up))
        * np.sqrt(sum_bins(hist_a_sqji, dalitz_plot_bin_up))
    )
    for dalitz_plot_bin_up in dalitz_plot_bins_up
]
cmi = [
    (
        sum_bins(hist_a_reij_reji, dalitz_plot_bin_down)
        + sum_bins(hist_a_imij_imji, dalitz_plot_bin_down)
    )
    / (
        np.sqrt(sum_bins(hist_a_sq, dalitz_plot_bin_down))
        * np.sqrt(sum_bins(hist_a_sqji, dalitz_plot_bin_down))
    )
    for dalitz_plot_bin_down in dalitz_plot_bins_down
]
smi = [
    (
        sum_bins(hist_a_reij_imji, dalitz_plot_bin_down)
        - sum_bins(hist_a_reji_imij, dalitz_plot_bin_down)
    )
    / (
        np.sqrt(sum_bins(hist_a_sq, dalitz_plot_bin_down))
        * np.sqrt(sum_bins(hist_a_sqji, dalitz_plot_bin_down))
    )
    for dalitz_plot_bin_down in dalitz_plot_bins_down
]

print(f"ki: {ki}")
print(f"kmi: {kmi}")
print(f"ci: {ci}")
print(f"cmi: {cmi}")
print(f"si: {si}")
print(f"smi: {smi}")

plot_k_parameters(ki, kmi, snakemake.output.fig_k_filepath, ylabel="k")
plot_cs_parameters(ci, si, snakemake.output.fig_cs_filepath, xlabel="c", ylabel="s")

Ci = [
    -0.0154534380,
    0.8478508543,
    0.1892644422,
    -0.9137806340,
    -0.1550303715,
    0.3648258385,
    0.8644270022,
    0.8565834993,
]
Si = [
    -0.8104234346,
    -0.1331743422,
    -0.8703767427,
    -0.0799645449,
    0.8545587019,
    0.7922451754,
    0.2067869318,
    -0.3323938070,
]
Ki = [
    0.0203365780,
    0.0040000361,
    0.0032598069,
    0.0637987973,
    0.0312136149,
    0.0038962435,
    0.0493697765,
    0.0634571919,
]
Kmi = [
    0.0960459496,
    0.1463195996,
    0.1433025377,
    0.1085394555,
    0.0540369696,
    0.0767518358,
    0.1145536063,
    0.0211180007,
]
plot_k_parameters(Ki, Kmi, snakemake.output.fig_K_filepath, ylabel="K", index="i")
plot_cs_parameters(Ci, Si, snakemake.output.fig_CS_filepath, xlabel="C", ylabel="S")

with open(snakemake.output.b2dpipi_parameters_filepath, "w") as output_file:
    yaml.dump(
        {"ki": ki, "kmi": kmi, "ci": list(map(float, ci)), "si": list(map(float, si))},
        output_file,
        default_flow_style=False,
    )

f.Close()

tee.stop()
