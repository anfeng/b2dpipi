import os
import pickle

import tensorflow as tf
import zfit
import zfit.z.numpy as znp
from utils import Tee, get_cp_parameters
from zfit import z


def get_model_beta(M, N):
    class MyBinnedBDecay(zfit.pdf.ZPDF):
        _PARAMS = ["dm", "dgamma", "tau", "mistag_rate", "beta"]
        _PARAMS += [f"{variable}{i}" for variable in ["c", "s"] for i in range(M)]
        _PARAMS += [f"k{i}" for i in range(2 * M)]
        _PARAMS += [f"{variable}{i}" for variable in ["C", "S"] for i in range(N)]
        _PARAMS += [f"K{i}" for i in range(2 * N)]

        def __init__(self, **params):
            dt = zfit.Space(obs="dt", limits=(0, 10))
            flavor = zfit.Space(obs="flavor", limits=(-1.5, -0.5)) + zfit.Space(
                obs="flavor", limits=(0.5, 1.5)
            )
            index_i = zfit.Space(obs="index_i", limits=(-N - 0.5, -0.5)) + zfit.Space(
                obs="index_i", limits=(0.5, N + 0.5)
            )
            index_j = zfit.Space(obs="index_j", limits=(-M - 0.5, -0.5)) + zfit.Space(
                obs="index_j", limits=(0.5, M + 0.5)
            )

            if not params:
                dm = zfit.Parameter("dm", 0.472, floating=False)
                dgamma = zfit.Parameter("dgamma", 0, floating=False)
                tau = zfit.Parameter("tau", 1.547, floating=False)
                mistag_rate = zfit.Parameter("mistag_rate", 0.36, floating=False)

                k_params = {
                    f"k{i}": zfit.Parameter(f"k{i}", 0, -1, 1, floating=False)
                    for i in range(2 * M)
                }
                cs_params = {
                    f"{variable}{i}": zfit.Parameter(f"{variable}{i}", 0, -5, 5)
                    for variable in ["c", "s"]
                    for i in range(M)
                }
                CS_params = {
                    f"{variable}{i}": zfit.Parameter(f"{variable}{i}", 0, -5, 5)
                    for variable in ["C", "S"]
                    for i in range(N)
                }
                K_params = {
                    f"K{i}": zfit.Parameter(f"K{i}", 0, -1, 1, floating=False)
                    for i in range(2 * N)
                }
                beta_param = zfit.Parameter("beta", znp.pi / 4, 0, znp.pi / 2)

                return super().__init__(
                    obs=dt * flavor * index_i * index_j,
                    dm=dm,
                    dgamma=dgamma,
                    tau=tau,
                    mistag_rate=mistag_rate,
                    beta=beta_param,
                    **k_params,
                    **K_params,
                    **cs_params,
                    **CS_params,
                )
            else:
                return super().__init__(obs=dt * flavor * index_i * index_j, **params)

        def get_indexed_variable(
            self,
            variable_name: str,
            index_expression: tf.Tensor,
            upper_limit: int,
            index: int = 0,
        ):
            # returns a Tensor yielding self.params[{variable_name}{index_expression}]
            # , where variable_name is a variable and index_expression, whose value ranges from 0 (included) to upper_limit (excluded), is an expression of some variables
            # , e.g. get_indexed_variable('K', i + 1, 10) yields K1 if the value of i + 1 is 1
            if index < upper_limit:
                return tf.where(
                    index_expression == index,
                    self.params[f"{variable_name}{index}"],
                    self.get_indexed_variable(
                        variable_name, index_expression, upper_limit, index + 1
                    ),
                )
            else:
                return tf.constant(0.0, dtype=tf.float64)

        def get_physically_indexed_variable(
            self, variable_name, index_expression, half_limit
        ):
            if variable_name == "K" or variable_name == "k":
                # convert physical index to fully-mapped programmatic index:
                return self.get_indexed_variable(
                    variable_name,
                    tf.where(
                        index_expression < 0,
                        index_expression + half_limit,
                        index_expression + half_limit - 1,
                    ),
                    2 * half_limit,
                )
            elif variable_name == "C" or variable_name == "c":
                # convert physical index to half-mapped programmatic index:
                return self.get_indexed_variable(
                    variable_name,
                    tf.where(
                        index_expression < 0,
                        -index_expression - 1,
                        index_expression - 1,
                    ),
                    half_limit,
                )
            elif variable_name == "S" or variable_name == "s":
                # convert physical index to half-mapped programmatic index:
                return tf.where(
                    index_expression < 0,
                    -self.get_indexed_variable(
                        variable_name, -index_expression - 1, half_limit
                    ),
                    self.get_indexed_variable(
                        variable_name, index_expression - 1, half_limit
                    ),
                )
            else:
                raise NotImplementedError(f"Unsupported variable name {variable_name}")

        def _unnormalized_pdf(self, x):
            dt, flavor, index_i, index_j = z.unstack_x(x)

            flavor = tf.round(flavor)
            index_i = tf.cast(tf.round(index_i), tf.int32)
            index_j = tf.cast(tf.round(index_j), tf.int32)

            dm = self.params["dm"]
            dgamma = self.params["dgamma"]
            tau = self.params["tau"]
            mistag_rate = self.params["mistag_rate"]
            beta = self.params["beta"]

            f0 = (
                self.get_physically_indexed_variable("K", -index_i, N)
                * self.get_physically_indexed_variable("k", index_j, M)
                + self.get_physically_indexed_variable("K", index_i, N)
                * self.get_physically_indexed_variable("k", -index_j, M)
            ) / 2
            f1 = 1
            f2 = (
                (1 - 2 * mistag_rate)
                * flavor
                * (
                    self.get_physically_indexed_variable("K", -index_i, N)
                    * self.get_physically_indexed_variable("k", index_j, M)
                    - self.get_physically_indexed_variable("K", index_i, N)
                    * self.get_physically_indexed_variable("k", -index_j, M)
                )
                / 2
            )
            f3 = (
                (1 - 2 * mistag_rate)
                * flavor
                * (
                    -tf.sqrt(
                        self.get_physically_indexed_variable("K", index_i, N)
                        * self.get_physically_indexed_variable("K", -index_i, N)
                        * self.get_physically_indexed_variable("k", index_j, M)
                        * self.get_physically_indexed_variable("k", -index_j, M)
                    )
                    * (
                        (
                            self.get_physically_indexed_variable("C", index_i, N)
                            * self.get_physically_indexed_variable("s", index_j, M)
                            - self.get_physically_indexed_variable("S", index_i, N)
                            * self.get_physically_indexed_variable("c", index_j, M)
                        )
                        * tf.cos(2 * beta)
                        - (
                            self.get_physically_indexed_variable("C", index_i, N)
                            * self.get_physically_indexed_variable("c", index_j, M)
                            + self.get_physically_indexed_variable("S", index_i, N)
                            * self.get_physically_indexed_variable("s", index_j, M)
                        )
                        * tf.sin(2 * beta)
                    )
                )
            )

            f = tf.exp(-dt / tau) * (
                f0 * tf.cosh(dgamma * dt / 2.0)
                + f1 * tf.sinh(dgamma * dt / 2.0)
                + f2 * tf.cos(dm * dt)
                + f3 * tf.sin(dm * dt)
            )
            return tf.where(
                f > 0,
                f,
                tf.math.nextafter(
                    tf.constant(0, tf.float64), tf.constant(1, tf.float64)
                ),
            )
            # smallest_float64 = tf.math.nextafter(tf.constant(0, tf.float64), tf.constant(1, tf.float64))
            # if f < 0:
            #     tf.print(f'ERROR: PDF value less than zero. Returning smallest positive float64 {smallest_float64}')
            #     return smallest_float64
            # else:
            #     return f

        def configure_parameters(self, snakemake_object):
            C, S, K, c, s, k, beta = get_cp_parameters(
                snakemake_object.input.b2dpipi_parameters_filepath, M, N
            )

            for i in range(2 * N):
                self.params[f"K{i}"].set_value(K[i])
                self.params[f"K{i}"].floating = False
            for i in range(2 * M):
                self.params[f"k{i}"].set_value(k[i])
                self.params[f"k{i}"].floating = False
            for i in range(N):
                self.params[f"C{i}"].set_value(C[i])
                self.params[f"S{i}"].set_value(S[i])
            for i in range(M):
                self.params[f"c{i}"].set_value(c[i])
                self.params[f"s{i}"].set_value(s[i])

            c_s_status = snakemake_object.wildcards.c_s_status
            if c_s_status[-5:] == "float":
                floating = True
            else:
                floating = False
            if c_s_status[:2] == "cs":
                for i in range(M):
                    self.params[f"c{i}"].floating = floating
                    self.params[f"s{i}"].floating = floating
            else:
                for i in range(M):
                    if f"c{i}" in c_s_status[:-5]:
                        self.params[f"c{i}"].floating = floating
                    else:
                        self.params[f"c{i}"].floating = not floating
                    if f"s{i}" in c_s_status[:-5]:
                        self.params[f"s{i}"].floating = floating
                    else:
                        self.params[f"s{i}"].floating = not floating

            C_S_status = snakemake_object.wildcards.C_S_status
            if C_S_status[-5:] == "float":
                floating = True
            else:
                floating = False
            if C_S_status[:2] == "CS":
                for i in range(M):
                    self.params[f"C{i}"].floating = floating
                    self.params[f"S{i}"].floating = floating
            else:
                for i in range(M):
                    if f"C{i}" in C_S_status[:-5]:
                        self.params[f"C{i}"].floating = floating
                    else:
                        self.params[f"C{i}"].floating = not floating
                    if f"S{i}" in C_S_status[:-5]:
                        self.params[f"S{i}"].floating = floating
                    else:
                        self.params[f"S{i}"].floating = not floating

            self.params["beta"].set_value(beta)
            if snakemake_object.wildcards.beta_status == "betafixed":
                self.params["beta"].floating = False
            else:
                self.params["beta"].floating = True

    return MyBinnedBDecay


def get_model_beta_cpspecific(M):
    class MyBinnedBDecayCPSpecific(zfit.pdf.ZPDF):
        _PARAMS = ["dm", "dgamma", "tau", "mistag_rate", "beta"]
        _PARAMS += [f"{variable}{i}" for variable in ["c", "s"] for i in range(M)]
        _PARAMS += [f"k{i}" for i in range(2 * M)]

        def __init__(self, **params):
            dt = zfit.Space(obs="dt", limits=(0, 10))
            flavor = zfit.Space(obs="flavor", limits=(-1.5, -0.5)) + zfit.Space(
                obs="flavor", limits=(0.5, 1.5)
            )
            index_j = zfit.Space(obs="index_j", limits=(-M - 0.5, -0.5)) + zfit.Space(
                obs="index_j", limits=(0.5, M + 0.5)
            )

            if not params:
                dm = zfit.Parameter("dm", 0.472, floating=False)
                dgamma = zfit.Parameter("dgamma", 0, floating=False)
                tau = zfit.Parameter("tau", 1.547, floating=False)
                mistag_rate = zfit.Parameter("mistag_rate", 0.36, floating=False)

                k_params = {
                    f"k{i}": zfit.Parameter(f"k{i}", 0, -1, 1, floating=False)
                    for i in range(2 * M)
                }
                cs_params = {
                    f"{variable}{i}": zfit.Parameter(f"{variable}{i}", 0, -5, 5)
                    for variable in ["c", "s"]
                    for i in range(M)
                }
                beta_param = zfit.Parameter("beta", znp.pi / 4, 0, znp.pi / 2)

                return super().__init__(
                    obs=dt * flavor * index_j,
                    dm=dm,
                    dgamma=dgamma,
                    tau=tau,
                    mistag_rate=mistag_rate,
                    beta=beta_param,
                    **k_params,
                    **cs_params,
                )
            else:
                return super().__init__(obs=dt * flavor * index_j, **params)

        def get_indexed_variable(
            self,
            variable_name: str,
            index_expression: tf.Tensor,
            upper_limit: int,
            index: int = 0,
        ):
            # returns a Tensor yielding self.params[{variable_name}{index_expression}]
            # , where variable_name is a variable and index_expression, whose value ranges from 0 (included) to upper_limit (excluded), is an expression of some variables
            # , e.g. get_indexed_variable('K', i + 1, 10) yields K1 if the value of i + 1 is 1
            if index < upper_limit:
                return tf.where(
                    index_expression == index,
                    self.params[f"{variable_name}{index}"],
                    self.get_indexed_variable(
                        variable_name, index_expression, upper_limit, index + 1
                    ),
                )
            else:
                return tf.constant(0.0, dtype=tf.float64)

        def get_physically_indexed_variable(
            self, variable_name, index_expression, half_limit
        ):
            if variable_name == "K" or variable_name == "k":
                # convert physical index to fully-mapped programmatic index:
                return self.get_indexed_variable(
                    variable_name,
                    tf.where(
                        index_expression < 0,
                        index_expression + half_limit,
                        index_expression + half_limit - 1,
                    ),
                    2 * half_limit,
                )
            elif variable_name == "C" or variable_name == "c":
                # convert physical index to half-mapped programmatic index:
                return self.get_indexed_variable(
                    variable_name,
                    tf.where(
                        index_expression < 0,
                        -index_expression - 1,
                        index_expression - 1,
                    ),
                    half_limit,
                )
            elif variable_name == "S" or variable_name == "s":
                # convert physical index to half-mapped programmatic index:
                return tf.where(
                    index_expression < 0,
                    -self.get_indexed_variable(
                        variable_name, -index_expression - 1, half_limit
                    ),
                    self.get_indexed_variable(
                        variable_name, index_expression - 1, half_limit
                    ),
                )
            else:
                raise NotImplementedError(f"Unsupported variable name {variable_name}")

        def _unnormalized_pdf(self, x):
            dt, flavor, index_j = z.unstack_x(x)

            flavor = tf.round(flavor)
            index_j = tf.cast(tf.round(index_j), tf.int32)

            dm = self.params["dm"]
            dgamma = self.params["dgamma"]
            tau = self.params["tau"]
            mistag_rate = self.params["mistag_rate"]
            beta = self.params["beta"]

            f0 = (
                self.get_physically_indexed_variable("k", index_j, M)
                + self.get_physically_indexed_variable("k", -index_j, M)
            ) / 2
            f1 = 1
            f2 = (
                (1 - 2 * mistag_rate)
                * flavor
                * (
                    self.get_physically_indexed_variable("k", index_j, M)
                    - self.get_physically_indexed_variable("k", -index_j, M)
                )
                / 2
            )
            f3 = (
                (1 - 2 * mistag_rate)
                * flavor
                * (
                    -tf.sqrt(
                        self.get_physically_indexed_variable("k", index_j, M)
                        * self.get_physically_indexed_variable("k", -index_j, M)
                    )
                    * (
                        self.get_physically_indexed_variable("s", index_j, M)
                        * tf.cos(2 * beta)
                        - self.get_physically_indexed_variable("c", index_j, M)
                        * tf.sin(2 * beta)
                    )
                )
            )

            f = tf.exp(-dt / tau) * (
                f0 * tf.cosh(dgamma * dt / 2.0)
                + f1 * tf.sinh(dgamma * dt / 2.0)
                + f2 * tf.cos(dm * dt)
                + f3 * tf.sin(dm * dt)
            )
            return tf.where(
                f > 0,
                f,
                tf.math.nextafter(
                    tf.constant(0, tf.float64), tf.constant(1, tf.float64)
                ),
            )

        def configure_parameters(self, snakemake_object):
            C, S, K, c, s, k, beta = get_cp_parameters(
                snakemake_object.input.b2dpipi_parameters_filepath, M, 1
            )

            for i in range(2 * M):
                self.params[f"k{i}"].set_value(k[i])
                self.params[f"k{i}"].floating = False
            for i in range(M):
                self.params[f"c{i}"].set_value(c[i])
                self.params[f"s{i}"].set_value(s[i])

            c_s_status = snakemake_object.wildcards.c_s_status
            if c_s_status[-5:] == "float":
                floating = True
            else:
                floating = False
            if c_s_status[:2] == "cs":
                for i in range(M):
                    self.params[f"c{i}"].floating = floating
                    self.params[f"s{i}"].floating = floating
            else:
                for i in range(M):
                    if f"c{i}" in c_s_status[:-5]:
                        self.params[f"c{i}"].floating = floating
                    else:
                        self.params[f"c{i}"].floating = not floating
                    if f"s{i}" in c_s_status[:-5]:
                        self.params[f"s{i}"].floating = floating
                    else:
                        self.params[f"s{i}"].floating = not floating

            self.params["beta"].set_value(beta)
            self.params["beta"].floating = True

    return MyBinnedBDecayCPSpecific


if __name__ == "__main__":
    tee = Tee(output_filepath=snakemake.log[0])
    tee.start()

    if "CUDA_VISIBLE_DEVICES" in os.environ:
        print("CUDA_VISIBLE_DEVICES is set")
        print(f'CUDA_VISIBLE_DEVICES: {os.environ["CUDA_VISIBLE_DEVICES"]}')
    else:
        print("CUDA_VISIBLE_DEVICES is not set")
        os.environ["CUDA_VISIBLE_DEVICES"] = f"{int(snakemake.wildcards.run_index) % 4}"
        print(f'setting CUDA_VISIBLE_DEVICES to {os.environ["CUDA_VISIBLE_DEVICES"]}')
    os.environ["TF_FORCE_GPU_ALLOW_GROWTH"] = "true"
    zfit.run.set_n_cpu(strict=True)

    print(f"visible devices: {tf.config.list_physical_devices()}")

    zfit.settings.set_verbosity(6)
    zfit.settings.set_seed(int(snakemake.wildcards.run_index))

    M = int(snakemake.wildcards.M)
    N = int(snakemake.wildcards.N)

    C, S, K, c, s, k, beta = get_cp_parameters(
        snakemake.input.b2dpipi_parameters_filepath, M, N
    )

    if (snakemake.wildcards.model_name == "model_beta") or (
        snakemake.wildcards.model_name == "model_combined"
    ):
        mybdecay = get_model_beta(M, N)()
        mybdecay.configure_parameters(snakemake)
    elif snakemake.wildcards.model_name == "model_beta_cpspecific":
        mybdecay = get_model_beta_cpspecific(M)()
        mybdecay.configure_parameters(snakemake)
    # elif snakemake.wildcards.model_name == 'model_cos2betasin2beta':
    #     mybdecay = get_model_cos2betasin2beta(M, N)
    else:
        raise NotImplementedError("model undefined")

    sampler = mybdecay.create_sampler(
        n=int(snakemake.wildcards.event_num), fixed_params=True
    )
    nll = zfit.loss.UnbinnedNLL(model=mybdecay, data=sampler)

    minimizer = zfit.minimize.Minuit(verbosity=9)
    toys = []
    results = []
    if snakemake.wildcards.model_name == "model_combined":
        mybdecay_cpspecific = get_model_beta_cpspecific(M)(
            **{
                key: val
                for key, val in mybdecay.params.items()
                if key[0] not in ["K", "C", "S"]
            }
        )
        mybdecay_cpspecific.configure_parameters(snakemake)
        sampler_cpspecific = mybdecay_cpspecific.create_sampler(
            n=int(1.6 * int(snakemake.wildcards.event_num)), fixed_params=True
        )
        nll_cpspecific = zfit.loss.UnbinnedNLL(
            model=mybdecay_cpspecific, data=sampler_cpspecific
        )

        for i in range(int(snakemake.params.toys_per_run)):
            print(f"\n\n---------- processing toy {i} ----------\n")

            print("\n\n---------- begin of sampling ----------\n")
            sampler.resample()
            sampler_cpspecific.resample()
            print("\n\n---------- end of sampling ----------\n")

            toys.append([sampler.to_pandas(), sampler_cpspecific.to_pandas()])

            print("\n\n---------- begin of minimization ----------\n")
            result = minimizer.minimize(nll + nll_cpspecific)
            print("\n\n---------- end of minimization ----------\n")

            print("\n\n---------- begin of hesse ----------\n")
            result.hesse()
            print("\n\n---------- end of hesse ----------\n")

            result.freeze()
            results.append(result)
            # results.append({'status': result.status,
            #                 'edm': result.edm,
            #                 'minNll': result.fminfull,
            #                 'params': result.params})
    else:
        for i in range(int(snakemake.params.toys_per_run)):
            print(f"\n\n---------- processing toy {i} ----------\n")

            print("\n\n---------- begin of sampling ----------\n")
            sampler.resample()
            print("\n\n---------- end of sampling ----------\n")

            toys.append(sampler.to_pandas())

            print("\n\n---------- begin of minimization ----------\n")
            result = minimizer.minimize(nll)
            print("\n\n---------- end of minimization ----------\n")

            print("\n\n---------- begin of hesse ----------\n")
            result.hesse()
            print("\n\n---------- end of hesse ----------\n")

            result.freeze()
            results.append(result)
            # results.append({'status': result.status,
            #                 'edm': result.edm,
            #                 'minNll': result.fminfull,
            #                 'params': result.params})

    with open(snakemake.output.toy_picklefilepath, "wb") as f:
        pickle.dump(toys, f)
    with open(snakemake.output.fitresult_picklefilepath, "wb") as f:
        pickle.dump(results, f)

    tee.stop()
