import pickle

import ROOT
import yaml
from utils import Tee

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)

if snakemake.wildcards.fitting_lib == "zfit":

    with open(snakemake.input.fitresult_filepath, "rb") as fitresult_file:
        fitresults = pickle.load(fitresult_file)

    results = []
    for fitresult in fitresults:

        # -------- retrieve beta results --------

        if (
            (snakemake.wildcards.model_name == "model_beta")
            or (snakemake.wildcards.model_name == "model_beta_cpspecific")
            or (snakemake.wildcards.model_name == "model_combined")
        ) and (snakemake.wildcards.beta_status != "betafixed"):
            beta_fitted = fitresult.params["beta"]["value"]
            beta_uncertainty = float(fitresult.params["beta"]["hesse"]["error"])
            beta_uncertainty_low = -beta_uncertainty
            beta_uncertainty_high = beta_uncertainty
            result = {
                "beta_fitted": beta_fitted,
                "beta_uncertainty": beta_uncertainty,
                "beta_fitted (degree)": beta_fitted / ROOT.TMath.Pi() * 180,
                "beta_uncertainty (degree)": beta_uncertainty / ROOT.TMath.Pi() * 180,
                "beta_uncertainty_low (degree)": beta_uncertainty_low
                / ROOT.TMath.Pi()
                * 180,
                "beta_uncertainty_high (degree)": beta_uncertainty_high
                / ROOT.TMath.Pi()
                * 180,
                "sin(2 * beta)": ROOT.TMath.Sin(2 * beta_fitted),
                "sin(2 * beta) uncertainty": 2
                * ROOT.TMath.Cos(2 * beta_fitted)
                * beta_uncertainty,
                "cos(2 * beta)": ROOT.TMath.Cos(2 * beta_fitted),
                "cos(2 * beta) uncertainty": 2
                * ROOT.TMath.Sin(2 * beta_fitted)
                * beta_uncertainty,
            }
        else:
            result = {}

        # -------- retrieve c s C S results --------

        if snakemake.wildcards.c_s_status != "csfixed":
            for i in range(M):
                if f"c{i}" in fitresult.params:
                    result[f"c{i}_fitted"] = fitresult.params[f"c{i}"]["value"]
                    result[f"c{i}_uncertainty"] = float(
                        fitresult.params[f"c{i}"]["hesse"]["error"]
                    )
                else:
                    result[f"c{i}_fitted"] = 1
                    result[f"c{i}_uncertainty"] = 1
                if f"s{i}" in fitresult.params:
                    result[f"s{i}_fitted"] = fitresult.params[f"s{i}"]["value"]
                    result[f"s{i}_uncertainty"] = float(
                        fitresult.params[f"s{i}"]["hesse"]["error"]
                    )
                else:
                    result[f"s{i}_fitted"] = 1
                    result[f"s{i}_uncertainty"] = 1
        if snakemake.wildcards.C_S_status != "CSfixed":
            for i in range(N):
                if f"C{i}" in fitresult.params:
                    result[f"C{i}_fitted"] = fitresult.params[f"C{i}"]["value"]
                    result[f"C{i}_uncertainty"] = float(
                        fitresult.params[f"C{i}"]["hesse"]["error"]
                    )
                else:
                    result[f"C{i}_fitted"] = 1
                    result[f"C{i}_uncertainty"] = 1
                if f"S{i}" in fitresult.params:
                    result[f"S{i}_fitted"] = fitresult.params[f"S{i}"]["value"]
                    result[f"S{i}_uncertainty"] = float(
                        fitresult.params[f"S{i}"]["hesse"]["error"]
                    )
                else:
                    result[f"S{i}_fitted"] = 1
                    result[f"S{i}_uncertainty"] = 1

        # -------- retrieve other results --------

        if fitresult.valid:
            result["status"] = 0
        else:
            result["status"] = 1
        result["message"] = fitresult.message
        result["edm"] = fitresult.edm
        result["minNll"] = fitresult.fminfull
        # result['info'] = fitresult.info

        results.append(result)

    with open(snakemake.output.fitresult_yamlfilepath, "w") as f:
        yaml.dump(results, f, default_flow_style=False)

elif snakemake.wildcards.fitting_lib == "roofit":

    fitresult_file = ROOT.TFile(snakemake.input.fitresult_filepath)
    fitresult = fitresult_file.Get("fitresult")

    # -------- retrieve beta results --------

    if (
        (snakemake.wildcards.model_name == "model_beta")
        or (snakemake.wildcards.model_name == "model_combined")
    ) and (snakemake.wildcards.beta_status != "betafixed"):
        beta_fitted = fitresult.floatParsFinal().find("beta").getVal()
        beta_uncertainty = fitresult.floatParsFinal().find("beta").getError()
        beta_uncertainty_low = fitresult.floatParsFinal().find("beta").getErrorLo()
        beta_uncertainty_high = fitresult.floatParsFinal().find("beta").getErrorHi()
        result = {
            "beta_fitted": beta_fitted,
            "beta_uncertainty": beta_uncertainty,
            "beta_fitted (degree)": beta_fitted / ROOT.TMath.Pi() * 180,
            "beta_uncertainty (degree)": beta_uncertainty / ROOT.TMath.Pi() * 180,
            "beta_uncertainty_low (degree)": beta_uncertainty_low
            / ROOT.TMath.Pi()
            * 180,
            "beta_uncertainty_high (degree)": beta_uncertainty_high
            / ROOT.TMath.Pi()
            * 180,
            "sin(2 * beta)": ROOT.TMath.Sin(2 * beta_fitted),
            "sin(2 * beta) uncertainty": 2
            * ROOT.TMath.Cos(2 * beta_fitted)
            * beta_uncertainty,
            "cos(2 * beta)": ROOT.TMath.Cos(2 * beta_fitted),
            "cos(2 * beta) uncertainty": 2
            * ROOT.TMath.Sin(2 * beta_fitted)
            * beta_uncertainty,
        }
    elif (snakemake.wildcards.model_name == "model_cos2betasin2beta") and (
        snakemake.wildcards.beta_status != "betafixed"
    ):
        cos2beta_fitted = fitresult.floatParsFinal().find("cos2beta").getVal()
        cos2beta_uncertainty = fitresult.floatParsFinal().find("cos2beta").getError()
        cos2beta_uncertainty_low = (
            fitresult.floatParsFinal().find("cos2beta").getErrorLo()
        )
        cos2beta_uncertainty_high = (
            fitresult.floatParsFinal().find("cos2beta").getErrorHi()
        )
        sin2beta_fitted = fitresult.floatParsFinal().find("sin2beta").getVal()
        sin2beta_uncertainty = fitresult.floatParsFinal().find("sin2beta").getError()
        sin2beta_uncertainty_low = (
            fitresult.floatParsFinal().find("sin2beta").getErrorLo()
        )
        sin2beta_uncertainty_high = (
            fitresult.floatParsFinal().find("sin2beta").getErrorHi()
        )
        result = {
            "sin(2 * beta)": sin2beta_fitted,
            "sin(2 * beta) uncertainty": sin2beta_uncertainty,
            "sin(2 * beta) uncertainty low": sin2beta_uncertainty_low,
            "sin(2 * beta) uncertainty high": sin2beta_uncertainty_high,
            "cos(2 * beta)": cos2beta_fitted,
            "cos(2 * beta) uncertainty": cos2beta_uncertainty,
            "cos(2 * beta) uncertainty low": cos2beta_uncertainty_low,
            "cos(2 * beta) uncertainty high": cos2beta_uncertainty_high,
        }
    else:
        result = {}

    # -------- retrieve c s C S results --------

    if snakemake.wildcards.c_s_status != "csfixed":
        for i in range(M):
            result[f"c{i}_fitted"] = fitresult.floatParsFinal().find(f"c{i}").getVal()
            result[f"c{i}_uncertainty"] = (
                fitresult.floatParsFinal().find(f"c{i}").getError()
            )
            result[f"s{i}_fitted"] = fitresult.floatParsFinal().find(f"s{i}").getVal()
            result[f"s{i}_uncertainty"] = (
                fitresult.floatParsFinal().find(f"s{i}").getError()
            )
    if snakemake.wildcards.C_S_status != "CSfixed":
        for i in range(N):
            result[f"C{i}_fitted"] = fitresult.floatParsFinal().find(f"C{i}").getVal()
            result[f"C{i}_uncertainty"] = (
                fitresult.floatParsFinal().find(f"C{i}").getError()
            )
            result[f"S{i}_fitted"] = fitresult.floatParsFinal().find(f"S{i}").getVal()
            result[f"S{i}_uncertainty"] = (
                fitresult.floatParsFinal().find(f"S{i}").getError()
            )

    # -------- retrieve other results --------

    result["status"] = fitresult.status()
    result["edm"] = fitresult.edm()
    result["minNll"] = fitresult.minNll()
    result["numInvalidNLL"] = fitresult.numInvalidNLL()

    with open(snakemake.output.fitresult_yamlfilepath, "w") as f:
        yaml.dump(result, f, default_flow_style=False)

tee.stop()
