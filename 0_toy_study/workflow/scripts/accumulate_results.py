import os
from typing import Any

import matplotlib.patches
import matplotlib.pyplot as plt
import numpy as np
import ROOT
import yaml
from utils import Tee, get_cp_parameters

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)


def draw_1Dhists(hists, output_filepaths):
    for hist in hists:
        canvas = ROOT.TCanvas()
        # hist.SetStats(False)
        hist.SetMarkerStyle(24)
        hist.SetMarkerColor(ROOT.kBlue)
        hist.SetLineColor(ROOT.kBlue)
        hist.SetMinimum(0.0)
        hist.SetXTitle(hist.GetTitle())
        hist.Draw("e")
        canvas.SaveAs(output_filepaths[hist.GetName()])


def draw_2Dhists(hists, output_filepaths):
    for hist in hists:
        canvas = ROOT.TCanvas()
        ROOT.gPad.SetLeftMargin(0.16)
        ROOT.gPad.SetRightMargin(0.16)
        hist.SetStats(False)
        hist.GetYaxis().SetTitleOffset(0.9)
        hist.Draw("colz")
        canvas.SaveAs(output_filepaths[hist.GetName()])


ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
ROOT.gROOT.SetBatch(True)
ROOT.EnableImplicitMT(snakemake.threads)
ROOT.gStyle.SetMarkerSize(0.5)

C, S, K, c, s, k, beta = get_cp_parameters(
    snakemake.input.b2dpipi_parameters_filepath, M, N
)


# -------- initialization --------

#     -------- initialize hists --------

if int(snakemake.wildcards.event_num) < 10000:
    lowerlimit_beta_fitted_degree = 0
    upperlimit_beta_fitted_degree = 90
    lowerlimit_beta_uncertainty_degree = 4
    upperlimit_beta_uncertainty_degree = 20
elif 10000 <= int(snakemake.wildcards.event_num) < 100000:
    lowerlimit_beta_fitted_degree = 0
    upperlimit_beta_fitted_degree = 90
    lowerlimit_beta_uncertainty_degree = 3
    upperlimit_beta_uncertainty_degree = 13
else:
    lowerlimit_beta_fitted_degree = 15
    upperlimit_beta_fitted_degree = 30
    lowerlimit_beta_uncertainty_degree = 1
    upperlimit_beta_uncertainty_degree = 2.5

hist_beta_fitted_degree = ROOT.TH1D(
    "hist_beta_fitted_degree",
    "#beta_{fitted} (#circ)",
    50,
    lowerlimit_beta_fitted_degree,
    upperlimit_beta_fitted_degree,
)
hist_beta_uncertainty_degree = ROOT.TH1D(
    "hist_beta_uncertainty_degree",
    "#sigma_{#beta_{fitted}} (#circ)",
    50,
    lowerlimit_beta_uncertainty_degree,
    upperlimit_beta_uncertainty_degree,
)
hist_sin2beta_fitted = ROOT.TH1D(
    "hist_sin2beta_fitted", "sin(2#beta)_{fitted}", 50, 0, 1
)
hist_sin2beta_uncertainty = ROOT.TH1D(
    "hist_sin2beta_uncertainty", "#sigma_{sin(2#beta)_{fitted}}", 50, 0, 1
)
hist_cos2beta_fitted = ROOT.TH1D(
    "hist_cos2beta_fitted", "cos(2#beta)_{fitted}", 50, 0, 1
)
hist_cos2beta_uncertainty = ROOT.TH1D(
    "hist_cos2beta_uncertainty", "#sigma_{cos(2#beta)_{fitted}}", 50, 0, 1
)
hist_beta_fitted_degree_beta_uncertainty_degree = ROOT.TH2D(
    "hist_beta_fitted_degree_beta_uncertainty_degree",
    "hist_beta_fitted_degree_beta_uncertainty_degree;#beta_{fitted} (#circ);#sigma_{#beta_{fitted}} (#circ)",
    50,
    lowerlimit_beta_fitted_degree,
    upperlimit_beta_fitted_degree,
    50,
    lowerlimit_beta_uncertainty_degree,
    upperlimit_beta_uncertainty_degree,
)
hist_Q_B_fitted_beta_uncertainty = ROOT.TH2D(
    "hist_Q_B_fitted_beta_uncertainty",
    "hist_Q_B_fitted_beta_uncertainty",
    50,
    0,
    3,
    50,
    lowerlimit_beta_uncertainty_degree,
    upperlimit_beta_uncertainty_degree,
)
hist_Q_D_fitted_beta_uncertainty = ROOT.TH2D(
    "hist_Q_D_fitted_beta_uncertainty",
    "hist_Q_D_fitted_beta_uncertainty",
    50,
    0,
    3,
    50,
    lowerlimit_beta_uncertainty_degree,
    upperlimit_beta_uncertainty_degree,
)
if snakemake.wildcards.c_s_status != "csfixed":
    hists_cs_fitted = {
        f"{variable_name}{i}": ROOT.TH1D(
            f"hist_{variable_name}{i}_fitted", f"{variable_name}_{i}", 50, -5, 5
        )
        for variable_name in ["c", "s"]
        for i in range(M)
    }
    hists_cs_uncertainty = {
        f"{variable_name}{i}": ROOT.TH1D(
            f"hist_{variable_name}{i}_uncertainty",
            f"#sigma_{{{variable_name}_{i}}}",
            50,
            0,
            3,
        )
        for variable_name in ["c", "s"]
        for i in range(M)
    }
else:
    hists_cs_fitted = {}
    hists_cs_uncertainty = {}
if snakemake.wildcards.C_S_status != "CSfixed":
    hists_CS_fitted = {
        f"{variable_name}{i}": ROOT.TH1D(
            f"hist_{variable_name}{i}_fitted", f"{variable_name}_{i}", 50, -5, 5
        )
        for variable_name in ["C", "S"]
        for i in range(N)
    }
    hists_CS_uncertainty = {
        f"{variable_name}{i}": ROOT.TH1D(
            f"hist_{variable_name}{i}_uncertainty",
            f"#sigma_{{{variable_name}_{i}}}",
            50,
            0,
            3,
        )
        for variable_name in ["C", "S"]
        for i in range(N)
    }
else:
    hists_CS_fitted = {}
    hists_CS_uncertainty = {}

#     -------- initialize lists and dicts --------

hist_status = {}
beta_fitted_degree = []
beta_uncertainty_degree = []
beta_pull = []
sin2beta_fitted = []
sin2beta_uncertainty = []
sin2beta_pull = []
cos2beta_fitted = []
cos2beta_uncertainty = []
cos2beta_pull = []
if snakemake.wildcards.c_s_status != "csfixed":
    cs_fitted = {
        f"{variable_name}{i}": [] for variable_name in ["c", "s"] for i in range(M)
    }
    cs_uncertainty = {
        f"{variable_name}{i}": [] for variable_name in ["c", "s"] for i in range(M)
    }
    cs_pull: dict[str, Any] = {
        f"{variable_name}{i}": [] for variable_name in ["c", "s"] for i in range(M)
    }
else:
    cs_fitted = {}
    cs_uncertainty = {}
    cs_pull = {}
if snakemake.wildcards.C_S_status != "CSfixed":
    CS_fitted = {
        f"{variable_name}{i}": [] for variable_name in ["C", "S"] for i in range(N)
    }
    CS_uncertainty = {
        f"{variable_name}{i}": [] for variable_name in ["C", "S"] for i in range(N)
    }
    CS_pull: dict[str, Any] = {
        f"{variable_name}{i}": [] for variable_name in ["C", "S"] for i in range(N)
    }
else:
    CS_fitted = {}
    CS_uncertainty = {}
    CS_pull = {}


# -------- filling --------

for input_filepath in snakemake.input.fitresult_yamlfilepaths:
    with open(input_filepath) as fin:
        results = yaml.load(fin, yaml.FullLoader)

        if snakemake.wildcards.fitting_lib == "roofit":
            results = [results]

        for result in results:

            hist_status[result["status"]] = hist_status.get(result["status"], 0) + 1

            # -------- do beta pull cut --------

            if snakemake.wildcards.beta_status != "betafixed":
                if snakemake.wildcards.model_name != "model_cos2betasin2beta":
                    if result["beta_uncertainty_low (degree)"] != 0:
                        if result["beta_fitted (degree)"] <= beta:
                            this_beta_pull = (
                                beta / ROOT.TMath.Pi() * 180
                                - result["beta_fitted (degree)"]
                            ) / result["beta_uncertainty_high (degree)"]
                        else:
                            this_beta_pull = (
                                result["beta_fitted (degree)"]
                                - beta / ROOT.TMath.Pi() * 180
                            ) / result["beta_uncertainty_low (degree)"]
                        if (this_beta_pull < -8) or (this_beta_pull > 8):
                            continue
                    else:
                        continue

            # -------- fill hists, lists and dicts --------

            if (
                result["status"] == 0
                or result["status"] == 70
                or result["status"] == 140
            ):

                # -------- fill beta-related hists, lists and dicts --------

                if snakemake.wildcards.beta_status != "betafixed":

                    # -------- fill sin2beta and cos2beta --------

                    hist_sin2beta_fitted.Fill(result["sin(2 * beta)"])
                    hist_sin2beta_uncertainty.Fill(result["sin(2 * beta) uncertainty"])
                    hist_cos2beta_fitted.Fill(result["cos(2 * beta)"])
                    hist_cos2beta_uncertainty.Fill(result["cos(2 * beta) uncertainty"])
                    sin2beta_fitted.append(result["sin(2 * beta)"])
                    sin2beta_uncertainty.append(result["sin(2 * beta) uncertainty"])
                    cos2beta_fitted.append(result["cos(2 * beta)"])
                    cos2beta_uncertainty.append(result["cos(2 * beta) uncertainty"])

                    # -------- fill sin2beta_pull and cos2beta_pull --------

                    if snakemake.wildcards.model_name != "model_cos2betasin2beta":
                        sin2beta_pull.append(
                            (result["sin(2 * beta)"] - ROOT.TMath.Sin(2 * beta))
                            / result["sin(2 * beta) uncertainty"]
                        )
                        cos2beta_pull.append(
                            (result["cos(2 * beta)"] - ROOT.TMath.Cos(2 * beta))
                            / result["cos(2 * beta) uncertainty"]
                        )
                    else:
                        if (
                            result["sin(2 * beta) uncertainty low"] != 0
                            and result["sin(2 * beta) uncertainty high"]
                        ):
                            if result["sin(2 * beta)"] <= ROOT.TMath.Sin(2 * beta):
                                sin2beta_pull.append(
                                    (ROOT.TMath.Sin(2 * beta) - result["sin(2 * beta)"])
                                    / result["sin(2 * beta) uncertainty high"]
                                )
                            else:
                                sin2beta_pull.append(
                                    (result["sin(2 * beta)"] - ROOT.TMath.Sin(2 * beta))
                                    / result["sin(2 * beta) uncertainty low"]
                                )
                        else:
                            sin2beta_pull.append(
                                (result["sin(2 * beta)"] - ROOT.TMath.Sin(2 * beta))
                                / result["sin(2 * beta) uncertainty"]
                            )
                        if (
                            result["cos(2 * beta) uncertainty low"] != 0
                            and result["cos(2 * beta) uncertainty high"] != 0
                        ):
                            if result["cos(2 * beta)"] <= ROOT.TMath.Cos(2 * beta):
                                cos2beta_pull.append(
                                    (ROOT.TMath.Cos(2 * beta) - result["cos(2 * beta)"])
                                    / result["cos(2 * beta) uncertainty high"]
                                )
                            else:
                                cos2beta_pull.append(
                                    (result["cos(2 * beta)"] - ROOT.TMath.Cos(2 * beta))
                                    / result["cos(2 * beta) uncertainty low"]
                                )
                        else:
                            cos2beta_pull.append(
                                (result["cos(2 * beta)"] - ROOT.TMath.Cos(2 * beta))
                                / result["cos(2 * beta) uncertainty"]
                            )

                    if snakemake.wildcards.model_name != "model_cos2betasin2beta":

                        # -------- fill beta --------

                        hist_beta_fitted_degree.Fill(result["beta_fitted (degree)"])
                        hist_beta_uncertainty_degree.Fill(
                            result["beta_uncertainty (degree)"]
                        )

                        hist_beta_fitted_degree_beta_uncertainty_degree.Fill(
                            result["beta_fitted (degree)"],
                            result["beta_uncertainty (degree)"],
                        )
                        beta_fitted_degree.append(result["beta_fitted (degree)"])
                        beta_uncertainty_degree.append(
                            result["beta_uncertainty (degree)"]
                        )

                        # -------- fill beta_pull --------

                        if result["beta_uncertainty_low (degree)"] != 0:
                            if result["beta_fitted (degree)"] <= beta:
                                beta_pull.append(
                                    (
                                        beta / ROOT.TMath.Pi() * 180
                                        - result["beta_fitted (degree)"]
                                    )
                                    / result["beta_uncertainty_high (degree)"]
                                )
                            else:
                                beta_pull.append(
                                    (
                                        result["beta_fitted (degree)"]
                                        - beta / ROOT.TMath.Pi() * 180
                                    )
                                    / result["beta_uncertainty_low (degree)"]
                                )
                        # beta_pull.append((result['beta_fitted (degree)'] - beta / ROOT.TMath.Pi() * 180) / result['beta_uncertainty (degree)'])

                        # -------- calculate Q_B and Q_D --------

                        if snakemake.wildcards.c_s_status == "csfixed":
                            c_forQ = c
                            s_forQ = s
                        else:
                            c_forQ = [result[f"c{i}_fitted"] for i in range(M)]
                            s_forQ = [result[f"s{i}_fitted"] for i in range(M)]
                        if snakemake.wildcards.C_S_status == "CSfixed":
                            C_forQ = C
                            S_forQ = S
                        else:
                            C_forQ = [result[f"C{i}_fitted"] for i in range(N)]
                            S_forQ = [result[f"S{i}_fitted"] for i in range(N)]
                        c_forQ = [c_forQ[-i - 1] for i in range(-M, 0)] + c_forQ
                        s_forQ = [-s_forQ[-i - 1] for i in range(-M, 0)] + s_forQ
                        C_forQ = [C_forQ[-i - 1] for i in range(-N, 0)] + C_forQ
                        S_forQ = [-S_forQ[-i - 1] for i in range(-N, 0)] + S_forQ
                        Q_B = ROOT.TMath.Sqrt(
                            sum(
                                [
                                    k[i] * (c_forQ[i] ** 2 + s_forQ[i] ** 2)
                                    for i in range(len(k))
                                ]
                            )
                            / sum(k)
                        )
                        Q_D = ROOT.TMath.Sqrt(
                            sum(
                                [
                                    K[i] * (C_forQ[i] ** 2 + S_forQ[i] ** 2)
                                    for i in range(len(K))
                                ]
                            )
                            / sum(K)
                        )
                        hist_Q_B_fitted_beta_uncertainty.Fill(
                            Q_B, result["beta_uncertainty (degree)"]
                        )
                        hist_Q_D_fitted_beta_uncertainty.Fill(
                            Q_D, result["beta_uncertainty (degree)"]
                        )

                # -------- fill c s hists, lists and dicts --------

                if snakemake.wildcards.c_s_status != "csfixed":
                    for variable_name in ["c", "s"]:
                        for i in range(M):
                            hists_cs_fitted[f"{variable_name}{i}"].Fill(
                                result[f"{variable_name}{i}_fitted"]
                            )
                            hists_cs_uncertainty[f"{variable_name}{i}"].Fill(
                                result[f"{variable_name}{i}_uncertainty"]
                            )
                            cs_fitted[f"{variable_name}{i}"].append(
                                result[f"{variable_name}{i}_fitted"]
                            )
                            cs_uncertainty[f"{variable_name}{i}"].append(
                                result[f"{variable_name}{i}_uncertainty"]
                            )
                            cs_pull[f"{variable_name}{i}"].append(
                                (
                                    result[f"{variable_name}{i}_fitted"]
                                    - globals()[variable_name][i]
                                )
                                / result[f"{variable_name}{i}_uncertainty"]
                            )

                # -------- fill C S hists, lists and dicts --------

                if snakemake.wildcards.C_S_status != "CSfixed":
                    for variable_name in ["C", "S"]:
                        for i in range(N):
                            hists_CS_fitted[f"{variable_name}{i}"].Fill(
                                result[f"{variable_name}{i}_fitted"]
                            )
                            hists_CS_uncertainty[f"{variable_name}{i}"].Fill(
                                result[f"{variable_name}{i}_uncertainty"]
                            )
                            CS_fitted[f"{variable_name}{i}"].append(
                                result[f"{variable_name}{i}_fitted"]
                            )
                            CS_uncertainty[f"{variable_name}{i}"].append(
                                result[f"{variable_name}{i}_uncertainty"]
                            )
                            CS_pull[f"{variable_name}{i}"].append(
                                (
                                    result[f"{variable_name}{i}_fitted"]
                                    - globals()[variable_name][i]
                                )
                                / result[f"{variable_name}{i}_uncertainty"]
                            )


# -------- convert lists to numpy arrays --------

beta_fitted_degree = np.array(beta_fitted_degree)
beta_uncertainty_degree = np.array(beta_uncertainty_degree)
beta_pull = np.array(beta_pull)
sin2beta_fitted = np.array(sin2beta_fitted)
sin2beta_uncertainty = np.array(sin2beta_uncertainty)
sin2beta_pull = np.array(sin2beta_pull)
cos2beta_fitted = np.array(cos2beta_fitted)
cos2beta_uncertainty = np.array(cos2beta_uncertainty)
cos2beta_pull = np.array(cos2beta_pull)
if snakemake.wildcards.c_s_status != "csfixed":
    for variable_name in ["c", "s"]:
        for i in range(M):
            cs_pull[f"{variable_name}{i}"] = np.array(cs_pull[f"{variable_name}{i}"])
if snakemake.wildcards.C_S_status != "CSfixed":
    for variable_name in ["C", "S"]:
        for i in range(N):
            CS_pull[f"{variable_name}{i}"] = np.array(CS_pull[f"{variable_name}{i}"])


# -------- histogram output --------

#     -------- histogram output of beta, sin2beta and cos2beta --------

draw_1Dhists(
    [
        hist_beta_fitted_degree,
        hist_beta_uncertainty_degree,
        hist_sin2beta_fitted,
        hist_sin2beta_uncertainty,
        hist_cos2beta_fitted,
        hist_cos2beta_uncertainty,
    ],
    snakemake.output,
)
draw_2Dhists(
    [
        hist_beta_fitted_degree_beta_uncertainty_degree,
        hist_Q_B_fitted_beta_uncertainty,
        hist_Q_D_fitted_beta_uncertainty,
    ],
    snakemake.output,
)

#     -------- histogram output of c s C S  --------

outputdir_hist_cs_fitted = f"{snakemake.params.figure_filepath_prefix}/hist_cs_fitted/{snakemake.params.figure_filepath_suffix}"
os.makedirs(outputdir_hist_cs_fitted)
outputdir_hist_cs_uncertainty = f"{snakemake.params.figure_filepath_prefix}/hist_cs_uncertainty/{snakemake.params.figure_filepath_suffix}"
os.makedirs(outputdir_hist_cs_uncertainty)
outputdir_hist_CS_fitted = f"{snakemake.params.figure_filepath_prefix}/hist_CS_fitted/{snakemake.params.figure_filepath_suffix}"
os.makedirs(outputdir_hist_CS_fitted)
outputdir_hist_CS_uncertainty = f"{snakemake.params.figure_filepath_prefix}/hist_CS_uncertainty/{snakemake.params.figure_filepath_suffix}"
os.makedirs(outputdir_hist_CS_uncertainty)
if snakemake.wildcards.c_s_status != "csfixed":
    draw_1Dhists(
        hists_cs_fitted.values(),
        {
            hist_cs_fitted.GetName(): f"{outputdir_hist_cs_fitted}/{hist_cs_fitted.GetName()}.pdf"
            for hist_cs_fitted in hists_cs_fitted.values()
        },
    )
    draw_1Dhists(
        hists_cs_uncertainty.values(),
        {
            hist_cs_uncertainty.GetName(): f"{outputdir_hist_cs_uncertainty}/{hist_cs_uncertainty.GetName()}.pdf"
            for hist_cs_uncertainty in hists_cs_uncertainty.values()
        },
    )
if snakemake.wildcards.C_S_status != "CSfixed":
    draw_1Dhists(
        hists_CS_fitted.values(),
        {
            hist_CS_fitted.GetName(): f"{outputdir_hist_CS_fitted}/{hist_CS_fitted.GetName()}.pdf"
            for hist_CS_fitted in hists_CS_fitted.values()
        },
    )
    draw_1Dhists(
        hists_CS_uncertainty.values(),
        {
            hist_CS_uncertainty.GetName(): f"{outputdir_hist_CS_uncertainty}/{hist_CS_uncertainty.GetName()}.pdf"
            for hist_CS_uncertainty in hists_CS_uncertainty.values()
        },
    )

#     -------- histogram output of beta_pull --------

beta_pull_data = ROOT.RooDataSet.from_numpy(
    {"beta_pull": beta_pull}, [ROOT.RooRealVar("beta_pull", "beta_pull", -8, 8)]
)
workspace = ROOT.RooWorkspace("w", "workspace")
workspace.factory(
    "Gaussian::gauss(beta_pull[-8, 8], mean[0, -1, 1], sigma[1, 0.1, 10])"
)
model = workspace.pdf("gauss")
pull_fitresult = model.fitTo(
    beta_pull_data, NumCPU=snakemake.threads, Save=True, Verbose=True
)
pull_fitresult.SetName("fitresult")
obs = workspace.var("beta_pull")
frame = obs.frame(Title="beta pull")
beta_pull_data.plotOn(frame)
model.plotOn(frame)
model.paramOn(frame)
canvas = ROOT.TCanvas("", "", 1200, 800)
frame.Draw()
canvas.SaveAs(snakemake.output.hist_beta_pull)

#     -------- histogram output of cos2beta_pull --------

cos2beta_pull_data = ROOT.RooDataSet.from_numpy(
    {"cos2beta_pull": cos2beta_pull},
    [ROOT.RooRealVar("cos2beta_pull", "cos2beta_pull", -8, 8)],
)
workspace = ROOT.RooWorkspace("w", "workspace")
workspace.factory(
    "Gaussian::gauss(cos2beta_pull[-8, 8], mean[0, -1, 1], sigma[1, 0.1, 10])"
)
model = workspace.pdf("gauss")
pull_fitresult = model.fitTo(
    cos2beta_pull_data, NumCPU=snakemake.threads, Save=True, Verbose=True
)
pull_fitresult.SetName("fitresult")
obs = workspace.var("cos2beta_pull")
frame = obs.frame(Title="#cos(2 * #beta) pull")
cos2beta_pull_data.plotOn(frame)
model.plotOn(frame)
model.paramOn(frame)
canvas = ROOT.TCanvas("", "", 1200, 800)
frame.Draw()
canvas.SaveAs(snakemake.output.hist_cos2beta_pull)

#     -------- histogram output of sin2beta_pull --------

sin2beta_pull_data = ROOT.RooDataSet.from_numpy(
    {"sin2beta_pull": sin2beta_pull},
    [ROOT.RooRealVar("sin2beta_pull", "sin2beta_pull", -8, 8)],
)
workspace = ROOT.RooWorkspace("w", "workspace")
workspace.factory(
    "Gaussian::gauss(sin2beta_pull[-8, 8], mean[0, -1, 1], sigma[1, 0.1, 10])"
)
model = workspace.pdf("gauss")
pull_fitresult = model.fitTo(
    sin2beta_pull_data, NumCPU=snakemake.threads, Save=True, Verbose=True
)
pull_fitresult.SetName("fitresult")
obs = workspace.var("sin2beta_pull")
frame = obs.frame(Title="#sin(2 * #beta) pull")
sin2beta_pull_data.plotOn(frame)
model.plotOn(frame)
model.paramOn(frame)
canvas = ROOT.TCanvas("", "", 1200, 800)
frame.Draw()
canvas.SaveAs(snakemake.output.hist_sin2beta_pull)

#     -------- histogram output of c s pulls --------

outputdir_hist_cs_pull = f"{snakemake.params.figure_filepath_prefix}/hist_cs_pull/{snakemake.params.figure_filepath_suffix}"
os.makedirs(outputdir_hist_cs_pull)
if snakemake.wildcards.c_s_status != "csfixed":
    for variable_name in ["c", "s"]:
        for i in range(M):
            cs_pull_data = ROOT.RooDataSet.from_numpy(
                {f"{variable_name}{i}_pull": cs_pull[f"{variable_name}{i}"]},
                [
                    ROOT.RooRealVar(
                        f"{variable_name}{i}_pull", f"{variable_name}{i}_pull", -8, 8
                    )
                ],
            )
            workspace = ROOT.RooWorkspace(f"w_{variable_name}{i}", "workspace")
            workspace.factory(
                f"Gaussian::gauss({variable_name}{i}_pull[-8, 8], mean[0, -1, 1], sigma[1, 0.1, 10])"
            )
            model = workspace.pdf("gauss")
            pull_fitresult = model.fitTo(
                cs_pull_data, NumCPU=snakemake.threads, Save=True, Verbose=True
            )
            pull_fitresult.SetName("fitresult")
            obs = workspace.var(f"{variable_name}{i}_pull")
            frame = obs.frame(Title=f"{variable_name}{i} pull")
            cs_pull_data.plotOn(frame)
            model.plotOn(frame)
            model.paramOn(frame)
            canvas = ROOT.TCanvas("", "", 1200, 800)
            frame.Draw()
            canvas.SaveAs(f"{outputdir_hist_cs_pull}/{variable_name}{i}.pdf")

#     -------- histogram output of C S pulls --------

outputdir_hist_CS_pull = f"{snakemake.params.figure_filepath_prefix}/hist_CS_pull/{snakemake.params.figure_filepath_suffix}"
os.makedirs(outputdir_hist_CS_pull)
if snakemake.wildcards.C_S_status != "CSfixed":
    for variable_name in ["C", "S"]:
        for i in range(N):
            CS_pull_data = ROOT.RooDataSet.from_numpy(
                {f"{variable_name}{i}_pull": CS_pull[f"{variable_name}{i}"]},
                [
                    ROOT.RooRealVar(
                        f"{variable_name}{i}_pull", f"{variable_name}{i}_pull", -8, 8
                    )
                ],
            )
            workspace = ROOT.RooWorkspace(f"w_{variable_name}{i}", "workspace")
            workspace.factory(
                f"Gaussian::gauss({variable_name}{i}_pull[-8, 8], mean[0, -1, 1], sigma[1, 0.1, 10])"
            )
            model = workspace.pdf("gauss")
            pull_fitresult = model.fitTo(
                CS_pull_data, NumCPU=snakemake.threads, Save=True, Verbose=True
            )
            pull_fitresult.SetName("fitresult")
            obs = workspace.var(f"{variable_name}{i}_pull")
            frame = obs.frame(Title=f"{variable_name}{i} pull")
            CS_pull_data.plotOn(frame)
            model.plotOn(frame)
            model.paramOn(frame)
            canvas = ROOT.TCanvas("", "", 1200, 800)
            frame.Draw()
            canvas.SaveAs(f"{outputdir_hist_CS_pull}/{variable_name}{i}.pdf")


# -------- yaml output --------

accumulated_result = {}
accumulated_result["hist_status"] = hist_status

#     -------- yaml output of beta, sin2beta, cos2beta, beta_pull, sin2beta_pull and cos2beta_pull --------

if snakemake.wildcards.beta_status != "betafixed":

    print(f"beta_pull: {beta_pull}")
    print(f"min of beta_pull: {np.min(beta_pull)}")
    print(f"max of beta_pull: {np.max(beta_pull)}")
    print(f"stddev of beta_pull: {np.std(beta_pull)}")

    accumulated_result["mean of beta (degree)"] = float(np.mean(beta_fitted_degree))
    accumulated_result["stddev of beta (degree)"] = float(np.std(beta_fitted_degree))
    accumulated_result["mean of beta uncertainty (degree)"] = float(
        np.mean(beta_uncertainty_degree)
    )
    accumulated_result["stddev of beta uncertainty (degree)"] = float(
        np.std(beta_uncertainty_degree)
    )
    accumulated_result["mean of beta pull"] = float(np.mean(beta_pull))
    accumulated_result["stddev of beta pull"] = float(np.std(beta_pull))
    accumulated_result["mean of sin(2 * beta)"] = float(np.mean(sin2beta_fitted))
    accumulated_result["stddev of sin(2 * beta)"] = float(np.std(sin2beta_fitted))
    accumulated_result["mean of sin(2 * beta) uncertainty"] = float(
        np.mean(sin2beta_uncertainty)
    )
    accumulated_result["stddev of sin(2 * beta) uncertainty"] = float(
        np.std(sin2beta_uncertainty)
    )
    accumulated_result["mean of cos(2 * beta)"] = float(np.mean(cos2beta_fitted))
    accumulated_result["stddev of cos(2 * beta)"] = float(np.std(cos2beta_fitted))
    accumulated_result["mean of cos(2 * beta) uncertainty"] = float(
        np.mean(cos2beta_uncertainty)
    )
    accumulated_result["stddev of cos(2 * beta) uncertainty"] = float(
        np.std(cos2beta_uncertainty)
    )

#     -------- yaml output of c s C S and their pulls  --------

if snakemake.wildcards.c_s_status != "csfixed":
    for variable_name in ["c", "s"]:
        for i in range(M):
            accumulated_result[f"mean of {variable_name}{i}"] = float(
                np.mean(cs_fitted[f"{variable_name}{i}"])
            )
            accumulated_result[f"stddev of {variable_name}{i}"] = float(
                np.std(cs_fitted[f"{variable_name}{i}"])
            )
            accumulated_result[f"mean of {variable_name}{i} uncertainty"] = float(
                np.mean(cs_uncertainty[f"{variable_name}{i}"])
            )
            accumulated_result[f"stddev of {variable_name}{i} uncertainty"] = float(
                np.std(cs_uncertainty[f"{variable_name}{i}"])
            )
            accumulated_result[f"mean of {variable_name}{i} pull"] = float(
                np.mean(cs_pull[f"{variable_name}{i}"])
            )
            accumulated_result[f"stddev of {variable_name}{i} pull"] = float(
                np.std(cs_pull[f"{variable_name}{i}"])
            )
if snakemake.wildcards.C_S_status != "CSfixed":
    for variable_name in ["C", "S"]:
        for i in range(N):
            accumulated_result[f"mean of {variable_name}{i}"] = float(
                np.mean(CS_fitted[f"{variable_name}{i}"])
            )
            accumulated_result[f"stddev of {variable_name}{i}"] = float(
                np.std(CS_fitted[f"{variable_name}{i}"])
            )
            accumulated_result[f"mean of {variable_name}{i} uncertainty"] = float(
                np.mean(CS_uncertainty[f"{variable_name}{i}"])
            )
            accumulated_result[f"stddev of {variable_name}{i} uncertainty"] = float(
                np.std(CS_uncertainty[f"{variable_name}{i}"])
            )
            accumulated_result[f"mean of {variable_name}{i} pull"] = float(
                np.mean(CS_pull[f"{variable_name}{i}"])
            )
            accumulated_result[f"stddev of {variable_name}{i} pull"] = float(
                np.std(CS_pull[f"{variable_name}{i}"])
            )

with open(snakemake.output.accumulated_result, "w") as f:
    yaml.dump(accumulated_result, f, default_flow_style=False)


# -------- c s C S circle figure output --------


def plot_cs_parameters(
    c,
    s,
    c_uncertainties,
    s_uncertainties,
    output_filepath,
    *,
    xlabel="c",
    ylabel="s",
    c_true=None,
    s_true=None,
):
    plt.figure()
    figure, axes = plt.subplots()
    plt.errorbar(c, s, xerr=c_uncertainties, yerr=s_uncertainties, fmt="bo")
    if (c_true is not None) and (s_true is not None):
        plt.scatter(c_true, s_true, marker="p", facecolors="none", edgecolors="k")
    axes.add_artist(
        matplotlib.patches.Circle((0, 0), 1, fill=False, linestyle="--", color="r")
    )
    plt.grid(which="both", linestyle="--")
    for i in range(len(c)):
        plt.annotate(
            f"{i + 1}", (c[i], s[i]), xytext=(0.5, 0.5), textcoords="offset fontsize"
        )
    plt.xlim(-1.8, 1.8)
    plt.ylim(-1.8, 1.8)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.minorticks_on()
    # plt.tick_params(axis='both', which='major', width=0.5)
    # plt.tick_params(axis='both', which='minor', width=0.1)
    # plt.xticks(np.arange(-1.2, 1.2, 0.1))
    # plt.yticks(np.arange(-1.2, 1.2, 0.1))
    axes.set_aspect("equal")
    plt.savefig(output_filepath)


if snakemake.wildcards.c_s_status != "csfixed":
    c_fitted = [accumulated_result[f"mean of c{i}"] for i in range(M)]
    s_fitted = [accumulated_result[f"mean of s{i}"] for i in range(M)]
    c_fitted_stddev = [accumulated_result[f"stddev of c{i}"] for i in range(M)]
    s_fitted_stddev = [accumulated_result[f"stddev of s{i}"] for i in range(M)]
    plot_cs_parameters(
        c_fitted,
        s_fitted,
        c_fitted_stddev,
        s_fitted_stddev,
        snakemake.output.fig_cs,
        xlabel="c",
        ylabel="s",
        c_true=c,
        s_true=s,
    )
else:
    os.system(f"touch {snakemake.output.fig_cs}")

if snakemake.wildcards.C_S_status != "CSfixed":
    C_fitted = [accumulated_result[f"mean of C{i}"] for i in range(N)]
    S_fitted = [accumulated_result[f"mean of S{i}"] for i in range(N)]
    C_fitted_stddev = [accumulated_result[f"stddev of C{i}"] for i in range(N)]
    S_fitted_stddev = [accumulated_result[f"stddev of S{i}"] for i in range(N)]
    plot_cs_parameters(
        C_fitted,
        S_fitted,
        C_fitted_stddev,
        S_fitted_stddev,
        snakemake.output.fig_CS,
        xlabel="C",
        ylabel="S",
        c_true=C,
        s_true=S,
    )
else:
    os.system(f"touch {snakemake.output.fig_CS}")

tee.stop()
