import os
import pickle

import numpy as np
import tensorflow as tf
import zfit
from generate_and_fit_toy_by_zfit import get_model_beta, get_model_beta_cpspecific
from utils import Tee

tee = Tee(output_filepath=snakemake.log[0])
tee.start()


# ---------- configure runtime configurations ----------

if "CUDA_VISIBLE_DEVICES" in os.environ:
    print("CUDA_VISIBLE_DEVICES is set")
    print(f'CUDA_VISIBLE_DEVICES: {os.environ["CUDA_VISIBLE_DEVICES"]}')
else:
    print("CUDA_VISIBLE_DEVICES is not set")
    os.environ["CUDA_VISIBLE_DEVICES"] = (
        f"{int(snakemake.wildcards.profnll_run_index) % 4}"
    )
    print(f'setting CUDA_VISIBLE_DEVICES to {os.environ["CUDA_VISIBLE_DEVICES"]}')
os.environ["TF_FORCE_GPU_ALLOW_GROWTH"] = "true"
zfit.run.set_n_cpu(strict=True)

print(f"visible devices: {tf.config.list_physical_devices()}")

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)


# ---------- prepare toy and fitresult ----------

with open(snakemake.input.toy_picklefilepath, "rb") as toy_file:
    toy = zfit.Data.from_pandas(
        pickle.load(toy_file)[int(snakemake.wildcards.toy_index_in_the_run)]
    )
with open(snakemake.input.fitresult_picklefilepath, "rb") as fitresult_file:
    fitresult = pickle.load(fitresult_file)[
        int(snakemake.wildcards.toy_index_in_the_run)
    ]


# ---------- prepare model ----------

if snakemake.wildcards.model_name == "model_beta":
    mybdecay = get_model_beta(M, N)()
    mybdecay.configure_parameters(snakemake)
elif snakemake.wildcards.model_name == "model_beta_cpspecific":
    mybdecay = get_model_beta_cpspecific(M)()
    mybdecay.configure_parameters(snakemake)
# elif snakemake.wildcards.model_name == 'model_cos2betasin2beta':
#     mybdecay = get_model_cos2betasin2beta(M, N)
else:
    print("model undefined")
    exit(1)

# configure_parameters(mybdecay, snakemake, M, N)

zfit.param.set_values(mybdecay.get_params(), fitresult)
print(mybdecay.params)


# ---------- calculate profnll ----------

nll = zfit.loss.UnbinnedNLL(model=mybdecay, data=toy)
minimizer = zfit.minimize.Minuit()

scanned_parameter = mybdecay.params[snakemake.wildcards.variable_name]
profnll_run_index = int(snakemake.wildcards.profnll_run_index)
points_per_profnll_run = int(snakemake.params.points_per_profnll_run)
x = np.linspace(
    scanned_parameter.lower,
    scanned_parameter.upper,
    num=snakemake.params.number_of_points,
)
x = x[
    (profnll_run_index * points_per_profnll_run) : (
        (profnll_run_index + 1) * points_per_profnll_run
    )
]
nll_values = []
profnll_values = []
scanned_parameter.floating = False
for i, val in enumerate(x):
    print(f"\nprocessing point {i}\n")

    scanned_parameter.set_value(val)
    nll_values.append(nll.value())

    temp_fitresult = minimizer.minimize(nll)
    zfit.param.set_values(mybdecay.get_params(), temp_fitresult)
    profnll_values.append(nll.value())
    zfit.param.set_values(mybdecay.get_params(), fitresult)

mybdecay.params["beta"].floating = True
zfit.param.set_values(mybdecay.get_params(), fitresult)


# ---------- save ----------

with open(snakemake.output.profnll_picklefilepath, "wb") as f:
    pickle.dump({"x": x, "nll": nll_values, "profnll": profnll_values}, f)

tee.stop()
