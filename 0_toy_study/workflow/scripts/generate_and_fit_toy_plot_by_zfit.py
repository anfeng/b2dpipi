import os
import pickle
import sys

import matplotlib.pyplot as plt
import tensorflow as tf
import zfit

sys.path.append(os.path.dirname(snakemake.input.model_filepath))

from generate_and_fit_toy_by_zfit import get_model_beta
from utils import get_cp_parameters

# ---------- configure runtime configurations ----------

if "CUDA_VISIBLE_DEVICES" in os.environ:
    print("CUDA_VISIBLE_DEVICES is set")
    print(f'CUDA_VISIBLE_DEVICES: {os.environ["CUDA_VISIBLE_DEVICES"]}')
else:
    print("CUDA_VISIBLE_DEVICES is not set")
    os.environ["CUDA_VISIBLE_DEVICES"] = f"{int(snakemake.wildcards.run_index) % 4}"
    print(f'setting CUDA_VISIBLE_DEVICES to {os.environ["CUDA_VISIBLE_DEVICES"]}')
os.environ["TF_FORCE_GPU_ALLOW_GROWTH"] = "true"
zfit.run.set_n_cpu(strict=True)

print(f"visible devices: {tf.config.list_physical_devices()}")


# ---------- prepare toy and fitresult ----------

with open(snakemake.input.toy_picklefilepath, "rb") as toy_file:
    toys = pickle.load(toy_file)
with open(snakemake.input.fitresult_picklefilepath, "rb") as fitresult_file:
    fitresults = pickle.load(fitresult_file)

toy = toys[0]
print(toy)
fitresult = fitresults[0]
fitresult.params


# ---------- read parameters ----------

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)
C, S, K, c, s, k, beta = get_cp_parameters(
    snakemake.input.b2dpipi_parameters_filepath, M, N
)


# ---------- prepare model ----------

mybdecay = get_model_beta(M, N)()
mybdecay.configure_parameters(snakemake)
mybdecay.get_params()
zfit.param.set_values(mybdecay.get_params(), fitresult)
print(mybdecay.params)


# ---------- plot ----------

results = {}
for input_filepath in snakemake.input.profnll_beta_picklefilepath:
    with open(input_filepath, "rb") as fin:
        result = pickle.load(fin)
        for key, value in result.items():
            results[key] = results.get(key, []) + list(value)
fig, ax = plt.subplots()
ax.plot(results["x"], results["nll"], color="b", marker=".", label="nll")
ax.plot(results["x"], results["profnll"], color="r", marker=".", label="profnll")
ax.set_xlabel("$\\beta$")
ax.set_ylabel("NLL")
ax.legend()
fig.savefig(snakemake.output.nll_beta)

results = {}
for input_filepath in snakemake.input.profnll_c0_picklefilepath:
    with open(input_filepath, "rb") as fin:
        result = pickle.load(fin)
        for key, value in result.items():
            results[key] = results.get(key, []) + list(value)
fig, ax = plt.subplots()
ax.plot(results["x"], results["nll"], color="b", marker=".", label="nll")
ax.plot(results["x"], results["profnll"], color="r", marker=".", label="profnll")
ax.set_xlabel("$c_0$")
ax.set_ylabel("NLL")
ax.legend()
fig.savefig(snakemake.output.nll_c0)

results = {}
for input_filepath in snakemake.input.profnll_s0_picklefilepath:
    with open(input_filepath, "rb") as fin:
        result = pickle.load(fin)
        for key, value in result.items():
            results[key] = results.get(key, []) + list(value)
fig, ax = plt.subplots()
ax.plot(results["x"], results["nll"], color="b", marker=".", label="nll")
ax.plot(results["x"], results["profnll"], color="r", marker=".", label="profnll")
ax.set_xlabel("$s_0$")
ax.set_ylabel("NLL")
ax.legend()
fig.savefig(snakemake.output.nll_s0)
