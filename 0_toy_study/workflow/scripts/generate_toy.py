import ROOT
from utils import Tee, get_cp_parameters

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)

model_file = ROOT.TFile(snakemake.input.model_filepath)
workspace = model_file.Get("w")

C, S, K, c, s, k, beta = get_cp_parameters(
    snakemake.input.b2dpipi_parameters_filepath, M, N
)

for i in range(N):
    workspace.var(f"C{i}").setVal(C[i])
    workspace.var(f"C{i}").setConstant(True)
    workspace.var(f"S{i}").setVal(S[i])
    workspace.var(f"S{i}").setConstant(True)
for i in range(M):
    workspace.var(f"c{i}").setVal(c[i])
    workspace.var(f"c{i}").setConstant(True)
    workspace.var(f"s{i}").setVal(s[i])
    workspace.var(f"s{i}").setConstant(True)
if snakemake.wildcards.model_name != "model_cos2betasin2beta":
    workspace.var("beta").setVal(beta)
else:
    workspace.var("cos2beta").setVal(ROOT.TMath.Cos(2 * beta))
    workspace.var("sin2beta").setVal(ROOT.TMath.Sin(2 * beta))

output_file = ROOT.TFile(snakemake.output.toy_filepath, "recreate")

dt = workspace.var("dt")
tagFlav = workspace.cat("tagFlav")
index_Dbin = workspace.cat("i")
index_Bbin = workspace.cat("j")

if snakemake.wildcards.model_name == "model_combined":
    bcpg_KSpipi = workspace.pdf(f"bcpg0_KSpipi")
    bcpg_hh = workspace.pdf(f"bcpg0_hh")

    ROOT.RooRandom.randomGenerator().SetSeed(int(snakemake.wildcards.subtoy_index))

    data_KSpipi = bcpg_KSpipi.generate(
        {dt, tagFlav, index_Dbin, index_Bbin},
        NumEvents=int(snakemake.wildcards.subtoy_size),
        Verbose=True,
    )
    data_KSpipi.SetName(f"bcpgData_KSpipi")
    data_KSpipi.Write()

    data_hh = bcpg_hh.generate(
        {dt, tagFlav, index_Bbin},
        NumEvents=int(1.6 * int(snakemake.wildcards.subtoy_size)),
        Verbose=True,
    )
    data_hh.SetName(f"bcpgData_hh")
    data_hh.Write()
else:
    bcpg = workspace.pdf(f"bcpg0")

    # Generate some data
    ROOT.RooRandom.randomGenerator().SetSeed(int(snakemake.wildcards.subtoy_index))
    data = bcpg.generate(
        {dt, tagFlav, index_Dbin, index_Bbin},
        NumEvents=int(snakemake.wildcards.subtoy_size),
        Verbose=True,
    )
    data.SetName(f"bcpgData")

    data.Write()

output_file.Close()

tee.stop()
