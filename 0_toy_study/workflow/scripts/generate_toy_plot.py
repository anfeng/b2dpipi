import os

import pandas as pd
import ROOT
from utils import Tee, get_cp_parameters, plot_workspace

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

model_file = ROOT.TFile(snakemake.input.model_filepath)
workspace = model_file.Get("w")

toy_data = ROOT.TFile(snakemake.input.toy_filepath[0]).Get("bcpgData")
for toy_filepath in snakemake.input.toy_filepath[1:]:
    toy_data.append(ROOT.TFile(toy_filepath).Get("bcpgData"))
toy_data = ROOT.RooDataSet.from_pandas(
    pd.DataFrame(toy_data.to_numpy())[: int(snakemake.wildcards.event_num)],
    toy_data.get(),
)
print(toy_data.numEntries())

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)

C, S, K, c, s, k, beta = get_cp_parameters(
    snakemake.input.b2dpipi_parameters_filepath, M, N
)
for i in range(N):
    workspace.var(f"C{i}").setVal(C[i])
    workspace.var(f"C{i}").setConstant(True)
    workspace.var(f"S{i}").setVal(S[i])
    workspace.var(f"S{i}").setConstant(True)
for i in range(M):
    workspace.var(f"c{i}").setVal(c[i])
    workspace.var(f"c{i}").setConstant(True)
    workspace.var(f"s{i}").setVal(s[i])
    workspace.var(f"s{i}").setConstant(True)
workspace.var("beta").setVal(beta)

os.makedirs(
    os.path.dirname(
        f"{snakemake.params.figure_filepath_prefix}_{snakemake.params.figure_filepath_suffix}"
    ),
    exist_ok=True,
)
plot_workspace(
    workspace,
    toy_data,
    M,
    N,
    snakemake.params.figure_filepath_prefix,
    snakemake.params.figure_filepath_suffix,
)

tee.stop()
