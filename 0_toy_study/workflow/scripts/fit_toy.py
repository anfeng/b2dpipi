import pandas as pd
import ROOT
from utils import Tee, get_cp_parameters

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)


# -------- read model and toy data --------

model_file = ROOT.TFile(snakemake.input.model_filepath)
workspace = model_file.Get("w")

sample_category = ROOT.RooCategory("sample_category", "sample_category")
sample_category.defineType("KSpipi")
sample_category.defineType("hh")

if snakemake.wildcards.model_name == "model_combined":

    toy_data_KSpipi = ROOT.TFile(snakemake.input.toy_filepath[0]).Get("bcpgData_KSpipi")
    for toy_filepath in snakemake.input.toy_filepath[1:]:
        toy_data_KSpipi.append(ROOT.TFile(toy_filepath).Get("bcpgData_KSpipi"))
    data_KSpipi = ROOT.RooDataSet.from_pandas(
        pd.DataFrame(toy_data_KSpipi.to_numpy())[: int(snakemake.wildcards.event_num)],
        toy_data_KSpipi.get(),
    )
    print(data_KSpipi.numEntries())

    toy_data_hh = ROOT.TFile(snakemake.input.toy_filepath[0]).Get("bcpgData_hh")
    for toy_filepath in snakemake.input.toy_filepath[1:]:
        toy_data_hh.append(ROOT.TFile(toy_filepath).Get("bcpgData_hh"))
    data_hh = ROOT.RooDataSet.from_pandas(
        pd.DataFrame(toy_data_hh.to_numpy())[
            : int(1.6 * int(snakemake.wildcards.event_num))
        ],
        toy_data_hh.get(),
    )
    print(data_hh.numEntries())

    data = ROOT.RooDataSet(
        "data",
        "data",
        data_KSpipi.get(),  # It seems that for data_hh, the "i" variable will be added and set to a fixed default value
        Index=sample_category,
        Import={
            "KSpipi": data_KSpipi,
            "hh": data_hh,
        },
    )
    data.Print("V")

else:

    toy_data = ROOT.TFile(snakemake.input.toy_filepath[0]).Get("bcpgData")
    for toy_filepath in snakemake.input.toy_filepath[1:]:
        toy_data.append(ROOT.TFile(toy_filepath).Get("bcpgData"))
    data = ROOT.RooDataSet.from_pandas(
        pd.DataFrame(toy_data.to_numpy())[: int(snakemake.wildcards.event_num)],
        toy_data.get(),
    )
    print(data.numEntries())

C, S, K, c, s, k, beta = get_cp_parameters(
    snakemake.input.b2dpipi_parameters_filepath, M, N
)


# -------- set initial values --------

for i in range(M):
    workspace.var(f"c{i}").setVal(c[i])
    workspace.var(f"s{i}").setVal(s[i])
    workspace.var(f"c{i}").setRange(-5, 5)
    workspace.var(f"s{i}").setRange(-5, 5)
for i in range(N):
    workspace.var(f"C{i}").setVal(C[i])
    workspace.var(f"S{i}").setVal(S[i])
    workspace.var(f"C{i}").setRange(-5, 5)
    workspace.var(f"S{i}").setRange(-5, 5)
if snakemake.wildcards.model_name != "model_cos2betasin2beta":
    workspace.var("beta").setVal(beta)
else:
    workspace.var("cos2beta").setVal(ROOT.TMath.Cos(2 * beta))
    workspace.var("sin2beta").setVal(ROOT.TMath.Sin(2 * beta))


# -------- fix some parameters --------

if snakemake.wildcards.c_s_status == "csfixed":
    for i in range(M):
        workspace.var(f"c{i}").setConstant(True)
        workspace.var(f"s{i}").setConstant(True)
if snakemake.wildcards.C_S_status == "CSfixed":
    for i in range(N):
        workspace.var(f"C{i}").setConstant(True)
        workspace.var(f"S{i}").setConstant(True)
if snakemake.wildcards.beta_status == "betafixed":
    workspace.var("beta").setConstant(True)


# -------- fit --------


class RepeatedFit:
    def __init__(
        self,
        model: ROOT.RooAbsPdf,
        parameter_list,
        times: int,
        random_seed: int | None = None,
    ):
        self.model: ROOT.RooAbsPdf = model
        self.parameter_list: ROOT.RooArgSet = ROOT.RooArgSet(
            [
                model.getVariables().find(parameter)
                for parameter in parameter_list
                if not model.getVariables().find(parameter).isConstant()
            ]
        )
        self.times: int = times

        if random_seed:
            ROOT.RooRandom.randomGenerator().SetSeed(random_seed)
        else:
            ROOT.RooRandom.randomGenerator().SetSeed()
        if isinstance(parameter_list, ROOT.RooArgSet):
            self.parameter_samples: ROOT.RooDataSet = ROOT.RooUniform(
                "uniform", "uniform", parameter_list
            ).generate(parameter_list, times)
        else:
            self.parameter_samples: ROOT.RooDataSet = ROOT.RooUniform(
                "uniform", "uniform", self.parameter_list
            ).generate(self.parameter_list, times)

    def do_repeated_fit(self, data: ROOT.RooDataSet, **fit_options) -> None:
        fit_options["Save"] = True
        self.fitresults = []
        for index in range(self.times):
            for parameter in self.parameter_samples.get(index):
                if self.parameter_list.find(parameter):
                    self.parameter_list.find(parameter).setVal(parameter.getValV())
            self.fitresults.append(self.model.fitTo(data, **fit_options))

    def print_all_results(self) -> None:
        for i, fitresult in enumerate(
            sorted(self.fitresults, key=lambda x: x.minNll())
        ):
            print(f"\n********** printing fit result {i + 1} **********\n")
            print(f"NLL: {fitresult.minNll()}")
            print(f"edm: {fitresult.edm()}")
            print()
            fitresult.Print("V")
            print(f"\n********** finished printing fit result {i + 1} **********\n")

    def get_best_result(self) -> ROOT.RooFitResult:
        return sorted(self.fitresults, key=lambda x: x.minNll())[0]


if snakemake.wildcards.model_name == "model_beta":

    beta_var = workspace.var("beta")
    bcpg = workspace.pdf("bcpg0")
    _parameter_list = [ROOT.RooRealVar("beta", "beta", 0, ROOT.TMath.Pi() / 2)]
    _parameter_list += [
        ROOT.RooRealVar(f"{variable}{index}", f"{variable}{index}", -2, 2)
        for variable in ["c", "s"]
        for index in range(M)
    ]
    _parameter_list += [
        ROOT.RooRealVar(f"{variable}{index}", f"{variable}{index}", -2, 2)
        for variable in ["C", "S"]
        for index in range(N)
    ]
    parameter_list = ROOT.RooArgSet(_parameter_list)
    repeated_fit = RepeatedFit(
        bcpg, parameter_list, 10, random_seed=int(snakemake.wildcards.run_index)
    )
    repeated_fit.do_repeated_fit(
        data,
        Extended=True,
        NumCPU=snakemake.threads,
        Save=True,
        Verbose=True,
        Minos=[beta_var],
    )
    repeated_fit.print_all_results()
    fitresult = repeated_fit.get_best_result()
    fitresult.SetName("fitresult")

elif snakemake.wildcards.model_name == "model_combined":

    beta_var = workspace.var("beta")

    bcpg_KSpipi = workspace.pdf("bcpg0_KSpipi")
    bcpg_hh = workspace.pdf("bcpg0_hh")

    _parameter_list = [ROOT.RooRealVar("beta", "beta", 0, ROOT.TMath.Pi() / 2)]
    _parameter_list += [
        ROOT.RooRealVar(f"{variable}{index}", f"{variable}{index}", -2, 2)
        for variable in ["c", "s"]
        for index in range(M)
    ]
    _parameter_list += [
        ROOT.RooRealVar(f"{variable}{index}", f"{variable}{index}", -2, 2)
        for variable in ["C", "S"]
        for index in range(N)
    ]
    parameter_list = ROOT.RooArgSet(_parameter_list)

    pdf_total = ROOT.RooSimultaneous(
        "pdf_total",
        "pdf_total",
        {
            "KSpipi": bcpg_KSpipi,
            "hh": bcpg_hh,
        },
        sample_category,
    )

    repeated_fit = RepeatedFit(
        pdf_total, parameter_list, 10, random_seed=int(snakemake.wildcards.run_index)
    )
    repeated_fit.do_repeated_fit(
        data,
        Extended=True,
        NumCPU=snakemake.threads,
        Save=True,
        Verbose=True,
        Minos=[beta_var],
    )
    repeated_fit.print_all_results()
    fitresult = repeated_fit.get_best_result()
    fitresult.SetName("fitresult")

elif snakemake.wildcards.model_name == "model_cos2betasin2beta":

    workspace.var(f"c0").setConstant(True)
    workspace.var(f"C0").setConstant(True)
    cos2beta_var = workspace.var("cos2beta")
    sin2beta_var = workspace.var("sin2beta")
    bcpg = workspace.pdf("bcpg0")
    _parameter_list = [ROOT.RooRealVar("cos2beta", "cos2beta", 0, 1)]
    _parameter_list += [ROOT.RooRealVar("sin2beta", "sin2beta", 0, 1)]
    _parameter_list += [
        ROOT.RooRealVar(f"{variable}{index}", f"{variable}{index}", -2, 2)
        for variable in ["c", "s"]
        for index in range(M)
    ]
    _parameter_list += [
        ROOT.RooRealVar(f"{variable}{index}", f"{variable}{index}", -2, 2)
        for variable in ["C", "S"]
        for index in range(N)
    ]
    parameter_list = ROOT.RooArgSet(_parameter_list)
    repeated_fit = RepeatedFit(
        bcpg, parameter_list, 10, random_seed=int(snakemake.wildcards.run_index)
    )
    repeated_fit.do_repeated_fit(
        data,
        Extended=True,
        NumCPU=snakemake.threads,
        Save=True,
        Verbose=True,
        Minos=[cos2beta_var, sin2beta_var],
    )
    repeated_fit.print_all_results()
    fitresult = repeated_fit.get_best_result()
    fitresult.SetName("fitresult")
else:
    print("model undefined")
    exit(1)

# -------- output fit result --------

output_file = ROOT.TFile(snakemake.output.fitresult_rootfilepath, "recreate")
output_file.cd()
fitresult.Write()

model_file.Close()
output_file.Close()

tee.stop()
