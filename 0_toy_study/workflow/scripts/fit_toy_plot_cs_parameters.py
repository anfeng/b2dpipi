import matplotlib.patches
import matplotlib.pyplot as plt
import yaml


def plot_cs_parameters(
    c, s, c_uncertainties, s_uncertainties, output_filepath, xlabel="c", ylabel="s"
):
    plt.figure()
    figure, axes = plt.subplots()
    plt.errorbar(c, s, xerr=c_uncertainties, yerr=s_uncertainties, fmt="bo")
    axes.add_artist(
        matplotlib.patches.Circle((0, 0), 1, fill=False, linestyle="--", color="r")
    )
    plt.grid(which="both")
    for i in range(len(c)):
        plt.annotate(
            f"{i + 1}", (c[i], s[i]), xytext=(0.5, 0.5), textcoords="offset fontsize"
        )
    plt.xlim(-1.8, 1.8)
    plt.ylim(-1.8, 1.8)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.minorticks_on()
    # plt.tick_params(axis='both', which='major', width=0.5)
    # plt.tick_params(axis='both', which='minor', width=0.1)
    # plt.xticks(np.arange(-1.2, 1.2, 0.1))
    # plt.yticks(np.arange(-1.2, 1.2, 0.1))
    axes.set_aspect("equal")
    plt.savefig(output_filepath)


if __name__ == "__main__":
    with open(snakemake.input[0]) as fin:
        fitresult = yaml.load(fin, yaml.FullLoader)[0]
    if snakemake.wildcards.c_s_status != "csfixed":
        c = [fitresult[f"c{i}_fitted"] for i in range(int(snakemake.wildcards.M))]
        s = [fitresult[f"s{i}_fitted"] for i in range(int(snakemake.wildcards.M))]
        c_uncertainties = [
            fitresult[f"c{i}_uncertainty"] for i in range(int(snakemake.wildcards.M))
        ]
        s_uncertainties = [
            fitresult[f"s{i}_uncertainty"] for i in range(int(snakemake.wildcards.M))
        ]
        plot_cs_parameters(c, s, c_uncertainties, s_uncertainties, snakemake.output[0])
    elif snakemake.wildcards.C_S_status != "CSfixed":
        C = [fitresult[f"C{i}_fitted"] for i in range(int(snakemake.wildcards.N))]
        S = [fitresult[f"S{i}_fitted"] for i in range(int(snakemake.wildcards.N))]
        C_uncertainties = [
            fitresult[f"C{i}_uncertainty"] for i in range(int(snakemake.wildcards.N))
        ]
        S_uncertainties = [
            fitresult[f"S{i}_uncertainty"] for i in range(int(snakemake.wildcards.N))
        ]
        plot_cs_parameters(
            C,
            S,
            C_uncertainties,
            S_uncertainties,
            snakemake.output[0],
            xlabel="C",
            ylabel="S",
        )
