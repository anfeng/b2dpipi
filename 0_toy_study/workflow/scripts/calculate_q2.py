import yaml
from utils import Tee, get_cp_parameters

tee = Tee(output_filepath=snakemake.log[0])
tee.start()

M = int(snakemake.wildcards.M)
N = int(snakemake.wildcards.N)

C, S, K, c, s, k, beta = get_cp_parameters(snakemake.input.b2dpipi_parameters_filepath, M, N)

c = [c[-i - 1] for i in range(-M, 0)] + c
s = [-s[-i - 1] for i in range(-M, 0)] + s
C = [C[-i - 1] for i in range(-N, 0)] + C
S = [-S[-i - 1] for i in range(-N, 0)] + S

print(f'c: {c}')
print(f's: {s}')
print(f'C: {C}')
print(f'S: {S}')

result = {}
result['Q^2_B'] = sum([k[i] * (c[i] ** 2 + s[i] ** 2) for i in range(len(k))]) / sum(k)
result['Q^2_D'] = sum([K[i] * (C[i] ** 2 + S[i] ** 2) for i in range(len(K))]) / sum(K)

with open(snakemake.output[0], 'w') as f:
    yaml.dump(result, f, default_flow_style=False)

tee.stop()
