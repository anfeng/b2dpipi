import multiprocessing
import os
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
import yaml


class Tee(object):
    """
    Class used to grab standard output or another stream.
    """

    escape_char = "\b"

    def __init__(self, streams=None, output_filepath=None, threaded=True):
        if streams is None:
            self.original_streams = [sys.stdout, sys.stderr]
        elif isinstance(streams, list):
            self.original_streams = streams
        else:
            self.original_streams = [streams]

        if output_filepath is None:
            self.output_file = None
        else:
            self.output_file = open(output_filepath, "w")

        self.threaded = threaded

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, type, value, traceback):
        self.stop()

    def start(self):
        """
        Start capturing the stream data.
        """
        self.capturedtexts = []
        self.capturedtext = ""
        self.original_stream_fds = [
            original_stream.fileno() for original_stream in self.original_streams
        ]
        # Save a copy of the stream:
        self.duplicated_stream_fds = [
            os.dup(original_stream_fd)
            for original_stream_fd in self.original_stream_fds
        ]
        self.duplicated_stdout = open(os.dup(sys.stdout.fileno()), "w")
        # Create a pipe so the stream can be captured:
        self.pipe_out, self.pipe_in = os.pipe()
        # Replace the original stream with our write pipe:
        for original_stream_fd in self.original_stream_fds:
            os.dup2(self.pipe_in, original_stream_fd)
        if self.threaded:
            # Start thread that will read the stream:
            self.worker_thread = multiprocessing.Process(target=self.read_output)
            self.worker_thread.start()
            # Make sure that the thread is running and os.read() has executed:
            time.sleep(0.01)

    def stop(self):
        """
        Stop capturing the stream data and save the text in `capturedtext`.
        """
        # Flush the stream to make sure all our data goes in before
        # the escape character:
        for original_stream in self.original_streams:
            original_stream.flush()
        # Print the escape character to make the read_output method stop:
        os.write(
            self.pipe_in, self.escape_char.encode(self.original_streams[0].encoding)
        )
        if self.threaded:
            # wait until the thread finishes so we are sure that
            # we have until the last character:
            self.worker_thread.join()
        else:
            self.read_output()
        # Close the pipe:
        os.close(self.pipe_in)
        os.close(self.pipe_out)
        for i in range(len(self.original_stream_fds)):
            # Restore the original stream:
            os.dup2(self.duplicated_stream_fds[i], self.original_stream_fds[i])
            # Close the duplicate stream:
            os.close(self.duplicated_stream_fds[i])
        self.duplicated_stdout.close()
        self.output_file.close()

    def read_output(self):
        """
        Read the stream data (one byte at a time)
        and save the text in `capturedtext`.
        """
        while True:
            char = os.read(self.pipe_out, 1).decode(self.original_streams[0].encoding)
            if not char or self.escape_char in char:
                break
            self.duplicated_stdout.write(char)
            if self.output_file is not None:
                self.output_file.write(char)
            self.capturedtext += char
            if len(self.capturedtext) >= 1000:
                self.capturedtexts.append(self.capturedtext)
                self.capturedtext = ""
        self.capturedtexts.append(self.capturedtext)
        self.capturedtext = ""
        self.output_file.flush()

    def get_capturedtext(self):
        return "".join(self.capturedtexts)


def get_cp_parameters(b2dpipi_parameters_filepath, M, N):
    with open(b2dpipi_parameters_filepath) as f:
        parameters = yaml.load(f, yaml.FullLoader)
        ci = parameters["ci"]
        si = parameters["si"]
        ki = parameters["ki"]
        kmi = parameters["kmi"]
    Ci = [
        -0.0154534380,
        0.8478508543,
        0.1892644422,
        -0.9137806340,
        -0.1550303715,
        0.3648258385,
        0.8644270022,
        0.8565834993,
    ]
    Si = [
        -0.8104234346,
        -0.1331743422,
        -0.8703767427,
        -0.0799645449,
        0.8545587019,
        0.7922451754,
        0.2067869318,
        -0.3323938070,
    ]
    Ki = [
        0.0203365780,
        0.0040000361,
        0.0032598069,
        0.0637987973,
        0.0312136149,
        0.0038962435,
        0.0493697765,
        0.0634571919,
    ]
    Kmi = [
        0.0960459496,
        0.1463195996,
        0.1433025377,
        0.1085394555,
        0.0540369696,
        0.0767518358,
        0.1145536063,
        0.0211180007,
    ]
    beta = 22 / 180 * np.pi

    ki = [kmi[-i - 1] for i in range(-M, 0)] + ki
    Ki = [Kmi[-i - 1] for i in range(-N, 0)] + Ki

    return Ci, Si, Ki, ci, si, ki, beta


def plot_workspace(
    workspace, data, M, N, output_filename_prefix, output_filename_suffix
):
    import ROOT

    ROOT.gROOT.SetBatch(True)

    dt = workspace.var("dt")
    frame = dt.frame(Title="B decay distribution (B0/B0bar)")

    tagFlav = workspace.cat("tagFlav")
    index_Dbin = workspace.cat("i")
    index_Bbin = workspace.cat("j")

    bcpg = workspace.pdf(f"bcpg0")

    df = data.to_pandas()

    for i in list(range(-N, 0)) + list(range(1, N + 1)):
        for j in list(range(-M, 0)) + list(range(1, M + 1)):
            # Plot B0 and B0bar tagged data separately
            frame = dt.frame(
                Title=f"B decay distribution (B0/B0bar) in Dbin {i} and Bbin {j}"
            )

            data.plotOn(frame, Cut=f"tagFlav==tagFlav::B0 && i=={i} && j=={j}")
            bcpg.plotOn(
                frame, Slice={tagFlav: "B0", index_Dbin: str(i), index_Bbin: str(j)}
            )

            data.plotOn(
                frame,
                Cut=f"tagFlav==tagFlav::B0bar && i=={i} && j=={j}",
                MarkerColor="c",
            )
            bcpg.plotOn(
                frame,
                Slice={tagFlav: "B0bar", index_Dbin: str(i), index_Bbin: str(j)},
                LineColor="c",
            )

            c = ROOT.TCanvas("", "", 1200, 800)
            ROOT.gPad.SetLeftMargin(0.15)
            frame.GetYaxis().SetTitleOffset(1.6)
            frame.Draw()
            c.SaveAs(
                output_filename_prefix + f"_Dbin{i}_Bbin{j}" + output_filename_suffix
            )

            hist_B0, bin_edges = np.histogram(
                df.query(f"(dt < 5) & (tagFlav == 1) & (i == {i}) & (j == {j})")["dt"],
                bins=50,
            )
            hist_B0bar, _ = np.histogram(
                df.query(f"(dt < 5) & (tagFlav == -1) & (i == {i}) & (j == {j})")["dt"],
                bins=bin_edges,
            )
            hist = (hist_B0 - hist_B0bar) / (hist_B0 + hist_B0bar)
            hist_err = 2 * np.sqrt(hist_B0 * hist_B0bar / (hist_B0 + hist_B0bar) ** 3)
            bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

            hist_B0_true = []
            hist_B0bar_true = []
            for bin_index in range(len(bin_edges) - 1):
                # workaround for a bug
                dt.setRange("integral1", 0, bin_edges[bin_index])
                dt.setRange("integral2", 0, bin_edges[bin_index + 1])
                index_Dbin.setLabel(str(i))
                index_Bbin.setLabel(str(j))
                tagFlav.setLabel("B0")
                hist_B0_true.append(
                    bcpg.createIntegral(
                        [dt], [dt, tagFlav, index_Dbin, index_Bbin], "integral2"
                    ).getVal()
                    - bcpg.createIntegral(
                        [dt], [dt, tagFlav, index_Dbin, index_Bbin], "integral1"
                    ).getVal()
                )
                tagFlav.setLabel("B0bar")
                hist_B0bar_true.append(
                    bcpg.createIntegral(
                        [dt], [dt, tagFlav, index_Dbin, index_Bbin], "integral2"
                    ).getVal()
                    - bcpg.createIntegral(
                        [dt], [dt, tagFlav, index_Dbin, index_Bbin], "integral1"
                    ).getVal()
                )
            hist_B0_true = np.array(hist_B0_true)
            hist_B0bar_true = np.array(hist_B0bar_true)
            print(hist_B0_true)
            print(hist_B0bar_true)
            hist_true = (hist_B0_true - hist_B0bar_true) / (
                hist_B0_true + hist_B0bar_true
            )

            plt.figure()
            plt.errorbar(
                bin_centers, hist, yerr=hist_err, marker="o", markersize=3, linestyle=""
            )
            plt.plot(bin_centers, hist_true)
            plt.axhline(y=0, color="k")
            plt.savefig(
                output_filename_prefix
                + f"_acp_Dbin{i}_Bbin{j}"
                + output_filename_suffix
            )

    for i in list(range(-N, 0)) + list(range(1, N + 1)):
        hist_B0, bin_edges = np.histogram(
            df.query(f"(dt < 5) & (tagFlav == 1) & (i == {i})")["dt"], bins=50
        )
        hist_B0bar, _ = np.histogram(
            df.query(f"(dt < 5) & (tagFlav == -1) & (i == {i})")["dt"], bins=bin_edges
        )
        hist = (hist_B0 - hist_B0bar) / (hist_B0 + hist_B0bar)
        hist_err = 2 * np.sqrt(hist_B0 * hist_B0bar / (hist_B0 + hist_B0bar) ** 3)
        bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
        plt.figure()
        plt.errorbar(
            bin_centers, hist, yerr=hist_err, marker="o", markersize=3, linestyle=""
        )
        plt.axhline(y=0, color="k")
        plt.savefig(output_filename_prefix + f"_acp_Dbin{i}" + output_filename_suffix)

    for j in list(range(-M, 0)) + list(range(1, M + 1)):
        hist_B0, bin_edges = np.histogram(
            df.query(f"(dt < 5) & (tagFlav == 1) & (j == {j})")["dt"], bins=50
        )
        hist_B0bar, _ = np.histogram(
            df.query(f"(dt < 5) & (tagFlav == -1) & (j == {j})")["dt"], bins=bin_edges
        )
        hist = (hist_B0 - hist_B0bar) / (hist_B0 + hist_B0bar)
        hist_err = 2 * np.sqrt(hist_B0 * hist_B0bar / (hist_B0 + hist_B0bar) ** 3)
        bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
        plt.figure()
        plt.errorbar(
            bin_centers, hist, yerr=hist_err, marker="o", markersize=3, linestyle=""
        )
        plt.axhline(y=0, color="k")
        plt.savefig(output_filename_prefix + f"_acp_Bbin{j}" + output_filename_suffix)

    # Plot B0 and B0bar tagged data separately
    frame = dt.frame(Title="B decay distribution (B0/B0bar)")

    data.plotOn(frame, Cut=f"tagFlav==tagFlav::B0")
    bcpg.plotOn(frame, Slice={tagFlav: "B0"})

    data.plotOn(frame, Cut=f"tagFlav==tagFlav::B0bar", MarkerColor="c")
    bcpg.plotOn(frame, Slice={tagFlav: "B0bar"}, LineColor="c")

    c = ROOT.TCanvas("", "", 1200, 800)
    ROOT.gPad.SetLeftMargin(0.15)
    frame.GetYaxis().SetTitleOffset(1.6)
    frame.Draw()
    c.SaveAs(output_filename_prefix + f"_total" + output_filename_suffix)

    hist_B0, bin_edges = np.histogram(
        df.query(f"(dt < 5) & (tagFlav == 1)")["dt"], bins=50
    )
    hist_B0bar, _ = np.histogram(
        df.query(f"(dt < 5) & (tagFlav == -1)")["dt"], bins=bin_edges
    )
    hist = (hist_B0 - hist_B0bar) / (hist_B0 + hist_B0bar)
    hist_err = 2 * np.sqrt(hist_B0 * hist_B0bar / (hist_B0 + hist_B0bar) ** 3)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    hist_B0_true = []
    hist_B0bar_true = []
    for bin_index in range(len(bin_edges) - 1):
        # workaround for a bug
        dt.setRange("integral1", 0, bin_edges[bin_index])
        dt.setRange("integral2", 0, bin_edges[bin_index + 1])
        tagFlav.setLabel("B0")
        hist_B0_true.append(
            bcpg.createIntegral(
                [dt, index_Dbin, index_Bbin],
                [dt, tagFlav, index_Dbin, index_Bbin],
                "integral2",
            ).getVal()
            - bcpg.createIntegral(
                [dt, index_Dbin, index_Bbin],
                [dt, tagFlav, index_Dbin, index_Bbin],
                "integral1",
            ).getVal()
        )
        tagFlav.setLabel("B0bar")
        hist_B0bar_true.append(
            bcpg.createIntegral(
                [dt, index_Dbin, index_Bbin],
                [dt, tagFlav, index_Dbin, index_Bbin],
                "integral2",
            ).getVal()
            - bcpg.createIntegral(
                [dt, index_Dbin, index_Bbin],
                [dt, tagFlav, index_Dbin, index_Bbin],
                "integral1",
            ).getVal()
        )
    hist_B0_true = np.array(hist_B0_true)
    hist_B0bar_true = np.array(hist_B0bar_true)
    print(hist_B0_true)
    print(hist_B0bar_true)
    hist_true = (hist_B0_true - hist_B0bar_true) / (hist_B0_true + hist_B0bar_true)

    plt.figure()
    plt.errorbar(
        bin_centers, hist, yerr=hist_err, marker="o", markersize=3, linestyle=""
    )
    plt.plot(bin_centers, hist_true)
    plt.axhline(y=0, color="k")
    plt.axhline(
        y=-0.016018 / 0.998667 * (1 - 2 * 0.36), ls="--", linewidth=0.5, color="red"
    )
    plt.axvline(x=np.pi / 2 / 0.472, ls="--", linewidth=0.5, color="red")
    plt.savefig(output_filename_prefix + "_acp_total" + output_filename_suffix)
