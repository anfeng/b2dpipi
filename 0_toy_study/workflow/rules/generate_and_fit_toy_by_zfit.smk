rule generate_and_fit_toy_by_zfit:
    input:
        "workflow/scripts/utils.py",
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
    output:
        toy_picklefilepath=f"data/output/toy/{paramspace.wildcard_pattern}.pkl",
        fitresult_picklefilepath=f"data/output/fitresult/{paramspace.wildcard_pattern}.pkl",
    log:
        f"logs/generate_and_fit_toy_by_zfit/{paramspace.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/generate_and_fit_toy_by_zfit/{paramspace.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        fitting_lib="zfit",
    params:
        toys_per_run=config["toys_per_run"],
    threads: 16
    resources:
        slurm_extra="--gres=gpu:1 --qos=unlimited",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/generate_and_fit_toy_by_zfit.py"
