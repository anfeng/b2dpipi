rule generate_and_fit_toy_plot_by_zfit:
    input:
        utils_filepath="workflow/scripts/utils.py",
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
        model_filepath="workflow/scripts/generate_and_fit_toy_by_zfit.py",
        toy_picklefilepath=f"data/output/toy/{paramspace.wildcard_pattern}.pkl",
        fitresult_picklefilepath=f"data/output/fitresult/{paramspace.wildcard_pattern}.pkl",
        profnll_beta_picklefilepath=[
            f"data/output/profnll/{paramspace_calprofnllbyzfit.wildcard_pattern}.pkl".replace(
                "{variable_name}", "beta"
            )
            .replace("{toy_index_in_the_run}", "0")
            .replace("{profnll_run_index}", str(profnll_run_index))
            for profnll_run_index in range(
                config["number_of_profnll_points"] // config["points_per_profnll_run"]
            )
        ],
        profnll_c0_picklefilepath=[
            f"data/output/profnll/{paramspace_calprofnllbyzfit.wildcard_pattern}.pkl".replace(
                "{variable_name}", "c0"
            )
            .replace("{toy_index_in_the_run}", "0")
            .replace("{profnll_run_index}", str(profnll_run_index))
            for profnll_run_index in range(
                config["number_of_profnll_points"] // config["points_per_profnll_run"]
            )
        ],
        profnll_s0_picklefilepath=[
            f"data/output/profnll/{paramspace_calprofnllbyzfit.wildcard_pattern}.pkl".replace(
                "{variable_name}", "s0"
            )
            .replace("{toy_index_in_the_run}", "0")
            .replace("{profnll_run_index}", str(profnll_run_index))
            for profnll_run_index in range(
                config["number_of_profnll_points"] // config["points_per_profnll_run"]
            )
        ],
    output:
        nll_beta=f"figures/generate_and_fit_toy_plot_by_zfit/nll_beta/{paramspace.wildcard_pattern}.png",
        nll_c0=f"figures/generate_and_fit_toy_plot_by_zfit/nll_c0/{paramspace.wildcard_pattern}.png",
        nll_s0=f"figures/generate_and_fit_toy_plot_by_zfit/nll_s0/{paramspace.wildcard_pattern}.png",
    log:
        f"logs/generate_and_fit_toy_plot_by_zfit/{paramspace.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/generate_and_fit_toy_plot_by_zfit/{paramspace.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        fitting_lib="zfit",
        c_s_status="csfloat",
        beta_status="betafloat",
    threads: 16
    resources:
        slurm_extra="--gres=gpu:1 --qos=unlimited",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/generate_and_fit_toy_plot_by_zfit.py"
