rule calculate_parameters:
    input:
        "data/b2dpipi_model/amplitudes.root",
    output:
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
        hist_binning_rootfilepath=f"data/output/b2dpipi_parameters/hist_binning/{paramspace_calparams.wildcard_pattern}.root",
        hist_a_sq_filepath=f"figures/b2dpipi_parameters/hist_a_sq/{paramspace_calparams.wildcard_pattern}.png",
        hist_a_sq_Dpip_pippim_filepath=f"figures/b2dpipi_parameters/hist_a_sq_Dpip_pippim/{paramspace_calparams.wildcard_pattern}.png",
        hist_a_sq_Dpip_pippim_Dpipprojection_filepath=f"figures/b2dpipi_parameters/hist_a_sq_Dpip_pippim_Dpipprojection/{paramspace_calparams.wildcard_pattern}.png",
        hist_a_sq_Dpip_pippim_pippimprojection_filepath=f"figures/b2dpipi_parameters/hist_a_sq_Dpip_pippim_pippimprojection/{paramspace_calparams.wildcard_pattern}.png",
        hist_a_sq_Dpim_pippim_filepath=f"figures/b2dpipi_parameters/hist_a_sq_Dpim_pippim/{paramspace_calparams.wildcard_pattern}.png",
        hist_a_sq_Dpim_pippim_Dpimprojection_filepath=f"figures/b2dpipi_parameters/hist_a_sq_Dpim_pippim_Dpimprojection/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_filepath=f"figures/b2dpipi_parameters/hist_abar_sq/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_Dpip_pippim_filepath=f"figures/b2dpipi_parameters/hist_abar_sq_Dpip_pippim/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_Dpip_pippim_Dpipprojection_filepath=f"figures/b2dpipi_parameters/hist_abar_sq_Dpip_pippim_Dpipprojection/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_Dpip_pippim_pippimprojection_filepath=f"figures/b2dpipi_parameters/hist_abar_sq_Dpip_pippim_pippimprojection/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_Dpip_pippim_withpippimcut_filepath=f"figures/b2dpipi_parameters/hist_abar_sq_Dpip_pippim_withpippimcut/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection_filepath=f"figures/b2dpipi_parameters/hist_abar_sq_Dpip_pippim_withpippimcut_Dpipprojection/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_Dpip_pippim_withDpipcut_filepath=f"figures/b2dpipi_parameters/hist_abar_sq_Dpip_pippim_withDpipcut/{paramspace_calparams.wildcard_pattern}.png",
        hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection_filepath=f"figures/b2dpipi_parameters/hist_abar_sq_Dpip_pippim_withDpipcut_pippimprojection/{paramspace_calparams.wildcard_pattern}.png",
        hist_arg_diff_filepath=f"figures/b2dpipi_parameters/hist_arg_diff/{paramspace_calparams.wildcard_pattern}.png",
        hist_abs_arg_diff_filepath=f"figures/b2dpipi_parameters/hist_abs_arg_diff/{paramspace_calparams.wildcard_pattern}.png",
        hist_abs_arg_diff_diff_filepath=f"figures/b2dpipi_parameters/hist_abs_arg_diff_diff/{paramspace_calparams.wildcard_pattern}.png",
        hist_binning_filepath=f"figures/b2dpipi_parameters/hist_binning/{paramspace_calparams.wildcard_pattern}.png",
        hist_binning_Dpip_pippim_filepath=f"figures/b2dpipi_parameters/hist_binning_Dpip_pippim/{paramspace_calparams.wildcard_pattern}.png",
        fig_cs_filepath=f"figures/b2dpipi_parameters/fig_cs/{paramspace_calparams.wildcard_pattern}.png",
        fig_k_filepath=f"figures/b2dpipi_parameters/fig_k/{paramspace_calparams.wildcard_pattern}.png",
        fig_CS_filepath=f"figures/b2dpipi_parameters/fig_CS/{paramspace_calparams.wildcard_pattern}.png",
        fig_K_filepath=f"figures/b2dpipi_parameters/fig_K/{paramspace_calparams.wildcard_pattern}.png",
    log:
        f"logs/calculate_parameters/{paramspace_calparams.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/calculate_parameters/{paramspace_calparams.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        param_source="our_params",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/calculate_parameters.py"


rule get_bondar_parameters:
    input:
        "data/b2dpipi_model/bondar_parameters.yml",
    output:
        f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
    benchmark:
        f"benchmarks/get_bondar_parameters/{paramspace_calparams.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        M="8",
        N="8",
        param_source="bondar_params",
    shell:
        "cp {input[0]} {output[0]}"


rule calculate_q2:
    input:
        "workflow/scripts/utils.py",
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
    output:
        f"data/output/Q2/{paramspace_calparams.wildcard_pattern}.yml",
    log:
        f"logs/calculate_q2/{paramspace_calparams.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/calculate_q2/{paramspace_calparams.wildcard_pattern}.benchmark.txt"
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/calculate_q2.py"
