rule generate_model:
    input:
        "workflow/scripts/utils.py",
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
    output:
        model_filepath=f"data/output/model/{paramspace_genmodel.wildcard_pattern}.root",
    log:
        f"logs/generate_model/{paramspace_genmodel.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/generate_model/{paramspace_genmodel.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        fitting_lib="roofit",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/generate_model.py"
