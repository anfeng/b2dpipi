rule calculate_profilelikelihood_by_zfit:
    input:
        utils_filepath="workflow/scripts/utils.py",
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
        toy_picklefilepath=f"data/output/toy/{paramspace.wildcard_pattern}.pkl",
        fitresult_picklefilepath=f"data/output/fitresult/{paramspace.wildcard_pattern}.pkl",
    output:
        profnll_picklefilepath=f"data/output/profnll/{paramspace_calprofnllbyzfit.wildcard_pattern}.pkl",
    log:
        f"logs/calculate_profilelikelihood_by_zfit/{paramspace_calprofnllbyzfit.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/calculate_profilelikelihood_by_zfit/{paramspace_calprofnllbyzfit.wildcard_pattern}.benchmark.txt"
    params:
        number_of_points=config["number_of_profnll_points"],
        points_per_profnll_run=config["points_per_profnll_run"],
    wildcard_constraints:
        fitting_lib="zfit",
    threads: 16
    resources:
        slurm_extra="--gres=gpu:1 --qos=unlimited",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/calculate_profilelikelihood_by_zfit.py"
