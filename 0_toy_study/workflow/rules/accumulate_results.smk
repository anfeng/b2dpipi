import math


rule accumulate_results:
    input:
        utils_filepath="workflow/scripts/utils.py",
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
        fitresult_yamlfilepaths=lambda wildcards: [
            f"data/output/fitresult/{paramspace.wildcard_pattern}.yml".replace(
                "{run_index}", str(i)
            )
            for i in range(
                math.ceil(config["toy_number"] / config["toys_per_run"])
                if wildcards.fitting_lib == "zfit"
                else config["toy_number"]
            )
        ],
    output:
        directory(
            f"figures/accumulated_result/hist_cs_fitted/{paramspace_accumulated.wildcard_pattern}/"
        ),
        directory(
            f"figures/accumulated_result/hist_cs_uncertainty/{paramspace_accumulated.wildcard_pattern}/"
        ),
        directory(
            f"figures/accumulated_result/hist_cs_pull/{paramspace_accumulated.wildcard_pattern}/"
        ),
        directory(
            f"figures/accumulated_result/hist_CS_fitted/{paramspace_accumulated.wildcard_pattern}/"
        ),
        directory(
            f"figures/accumulated_result/hist_CS_uncertainty/{paramspace_accumulated.wildcard_pattern}/"
        ),
        directory(
            f"figures/accumulated_result/hist_CS_pull/{paramspace_accumulated.wildcard_pattern}/"
        ),
        hist_beta_fitted_degree=f"figures/accumulated_result/hist_beta_fitted_degree/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_beta_uncertainty_degree=f"figures/accumulated_result/hist_beta_uncertainty_degree/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_beta_pull=f"figures/accumulated_result/hist_beta_pull/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_beta_fitted_degree_beta_uncertainty_degree=f"figures/accumulated_result/hist_beta_fitted_degree_beta_uncertainty_degree/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_Q_B_fitted_beta_uncertainty=f"figures/accumulated_result/hist_Q_B_fitted_beta_uncertainty/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_Q_D_fitted_beta_uncertainty=f"figures/accumulated_result/hist_Q_D_fitted_beta_uncertainty/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_sin2beta_fitted=f"figures/accumulated_result/hist_sin2beta_fitted/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_sin2beta_uncertainty=f"figures/accumulated_result/hist_sin2beta_uncertainty/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_sin2beta_pull=f"figures/accumulated_result/hist_sin2beta_pull/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_cos2beta_fitted=f"figures/accumulated_result/hist_cos2beta_fitted/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_cos2beta_uncertainty=f"figures/accumulated_result/hist_cos2beta_uncertainty/{paramspace_accumulated.wildcard_pattern}.pdf",
        hist_cos2beta_pull=f"figures/accumulated_result/hist_cos2beta_pull/{paramspace_accumulated.wildcard_pattern}.pdf",
        fig_cs=f"figures/accumulated_result/fig_cs/{paramspace_accumulated.wildcard_pattern}.pdf",
        fig_CS=f"figures/accumulated_result/fig_CS/{paramspace_accumulated.wildcard_pattern}.pdf",
        accumulated_result=f"data/output/accumulated_result/{paramspace_accumulated.wildcard_pattern}.yml",
    log:
        f"logs/accumulate_results/{paramspace_accumulated.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/accumulate_results/{paramspace_accumulated.wildcard_pattern}.benchmark.txt"
    params:
        figure_filepath_prefix="figures/accumulated_result",
        figure_filepath_suffix=paramspace_accumulated.wildcard_pattern,
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/accumulate_results.py"
