rule fit_toy_get_results:
    input:
        utils_filepath="workflow/scripts/utils.py",
        fitresult_filepath=lambda wildcards: (
            f"data/output/fitresult/{paramspace.wildcard_pattern}.pkl"
            if wildcards.fitting_lib == "zfit"
            else f"data/output/fitresult/{paramspace.wildcard_pattern}.root"
        ),
    output:
        fitresult_yamlfilepath=f"data/output/fitresult/{paramspace.wildcard_pattern}.yml",
    log:
        f"logs/fit_toy_get_results/{paramspace.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/fit_toy_get_results/{paramspace.wildcard_pattern}.benchmark.txt"
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/fit_toy_get_results.py"


rule fit_toy_plot_cs_parameters:
    input:
        f"data/output/fitresult/{paramspace.wildcard_pattern}.yml",
    output:
        f"figures/fig_cs/{paramspace.wildcard_pattern}.png",
    log:
        f"logs/fit_toy_plot_cs_parameters/{paramspace.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/fit_toy_plot_cs_parameters/{paramspace.wildcard_pattern}.benchmark.txt"
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/fit_toy_plot_cs_parameters.py"
