rule generate_toy:
    input:
        "workflow/scripts/utils.py",
        b2dpipi_parameters_filepath=f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml",
        model_filepath=f"data/output/model/{paramspace_genmodel.wildcard_pattern}.root",
    output:
        toy_filepath=f"data/output/toy/{paramspace_gentoy.wildcard_pattern}.root",
    log:
        f"logs/generate_toy/{paramspace_gentoy.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/generate_toy/{paramspace_gentoy.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        fitting_lib="roofit",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/generate_toy.py"
