def get_fit_toy_input(wildcards):
    import math

    subtoy_size = get_subtoy_size(int(wildcards.M), int(wildcards.N))
    subtoy_num = math.ceil(int(wildcards.event_num) / subtoy_size)

    inputs = {
        "utils_filepath": "workflow/scripts/utils.py",
        "b2dpipi_parameters_filepath": f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml".format(
            **wildcards
        ),
        "model_filepath": f"data/output/model/{paramspace_genmodel.wildcard_pattern}.root".format(
            **wildcards
        ),
    }
    inputs["toy_filepath"] = [
        f"data/output/toy/{paramspace_gentoy.wildcard_pattern}.root".format(
            subtoy_size=subtoy_size, subtoy_index=i, **wildcards
        )
        for i in range(
            int(wildcards.run_index) * subtoy_num,
            (int(wildcards.run_index) + 1) * subtoy_num,
        )
    ]
    return inputs


rule fit_toy:
    input:
        unpack(get_fit_toy_input),
    output:
        fitresult_rootfilepath=f"data/output/fitresult/{paramspace.wildcard_pattern}.root",
    log:
        f"logs/fit_toy/{paramspace.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/fit_toy/{paramspace.wildcard_pattern}.benchmark.txt"
    wildcard_constraints:
        fitting_lib="roofit",
    threads: 4
    resources:
        runtime="5h",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/fit_toy.py"
