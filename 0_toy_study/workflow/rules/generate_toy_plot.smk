def get_generate_toy_plot_input(wildcards):
    import math

    subtoy_size = get_subtoy_size(int(wildcards.M), int(wildcards.N))
    subtoy_num = math.ceil(int(wildcards.event_num) / subtoy_size)

    inputs = {
        "utils_filepath": "workflow/scripts/utils.py",
        "b2dpipi_parameters_filepath": f"data/output/b2dpipi_parameters/{paramspace_calparams.wildcard_pattern}.yml".format(
            **wildcards
        ),
        "model_filepath": f"data/output/model/{paramspace_genmodel.wildcard_pattern}.root".format(
            **wildcards
        ),
    }
    inputs["toy_filepath"] = [
        f"data/output/toy/{paramspace_gentoy.wildcard_pattern}.root".format(
            subtoy_size=subtoy_size, subtoy_index=i, **wildcards
        )
        for i in range(
            int(wildcards.run_index) * subtoy_num,
            (int(wildcards.run_index) + 1) * subtoy_num,
        )
    ]
    return inputs


rule generate_toy_plot:
    input:
        unpack(get_generate_toy_plot_input),
    output:
        directory(
            f"figures/generate_toy_plot/{paramspace_gentoyplot.wildcard_pattern}/"
        ),
    log:
        f"logs/generate_toy_plot/{paramspace_gentoyplot.wildcard_pattern}.log",
    benchmark:
        f"benchmarks/generate_toy_plot/{paramspace_gentoyplot.wildcard_pattern}.benchmark.txt"
    params:
        figure_filepath_prefix=f"figures/generate_toy_plot/{paramspace_gentoyplot.wildcard_pattern}/toy",
        figure_filepath_suffix=".pdf",
    wildcard_constraints:
        fitting_lib="roofit",
    conda:
        "../envs/analysis.yml"
    script:
        "../scripts/generate_toy_plot.py"
