# $B^0 \to \bar{D}^{0} \pi^+ \pi^-, \  \bar{D}^{0} \to K_S \pi^+ \pi^-$ toy study

## Input files of the workflow

To run the workflow and produce results, the following input files are required:

| file name                                | description                                                     |
| :--------------------------------------- | :-------------------------------------------------------------- |
| data/b2dpipi_model/amplitudes.root       | Amplitudes given in 100x100 bins on $D \pi^+ \pi^-$ Dalitz plot |
| data/b2dpipi_model/bondar_parameters.yml | CP parameters from A.Bondar's paper                             |

`data/b2dpipi_model/bondar_parameters.yml` is included in this repository, while `data/b2dpipi_model/amplitudes.root` needs to be downloaded from EOS due to its large size. 

## Setting up environments

To run this analysis, only a snakemake environment is required, which can be created and activated like

```
mamba create -c conda-forge -c bioconda -n snakemake snakemake
mamba activate snakemake
```

## Running

The following commands need to be executed in an environment with snakemake. 

To dry-run the main workflow, issue in a snakemake conda environment

```shell
snakemake -np
```

To get the dot language description of the DAG of the workflow, issue in a snakemake conda environment

```shell
snakemake --dag
snakemake --filegraph
snakemake --rulegraph
```

To run the main workflow, issue in a snakemake conda environment

```shell
snakemake -c64 --use-conda
```
